#pragma once
class cObjectShader
{
public:
	cObjectShader();
	~cObjectShader();

private:
	//Vertex Shader
	ID3D11VertexShader* m_VS_Shader = nullptr;
	//Pixel Shader
	ID3D11PixelShader* m_PS_Shader = nullptr;
	//InputLayout
	ID3D11InputLayout* m_InputLayout = nullptr;

	//월드 뷰 프로젝션
	ID3D11Buffer* m_matrixBuffer = nullptr;
	//카메라 위치 버퍼
	ID3D11Buffer* m_cameraBuffer = nullptr;
	//빛 정보 버퍼
	ID3D11Buffer* m_lightBuffer = nullptr;
public:
	cTexture* m_Texture = nullptr;

	void _Initialize();
	void _Update(D3DXMATRIX m, D3DXMATRIX v, D3DXMATRIX p);
	void _Render(UINT renderCount);


};

