#include "stdafx.h"
#include "cColliderBox.h"

cColliderBox::cColliderBox()
	:
	m_Center(0,0,0),
	m_extent(0.5f, 0.5f, 0.5f)
{
	m_Axis[0] = D3DXVECTOR3(1, 0, 0);
	m_Axis[1] = D3DXVECTOR3(0, 1, 0);
	m_Axis[2] = D3DXVECTOR3(0, 0, 1);
}


cColliderBox::~cColliderBox()
{
}

bool cColliderBox::_Collider(cColliderBox * other)
{
	D3DXVECTOR3 T = other->m_Center - m_Center;

	float C[3][3];    //matrix C=A^T B,c_{ij}=Dot(A_i,B_j)
	float absC[3][3]; //|c_{ij}| //절대값
	float AD[3];      //Dot(A_i,D)
	float R0, R1, R;    //interval radii and distance between centers //간격 반경 및 중심 간 거리
	float R01;        //=R0+R1

	//A0
	{
		C[0][0] = D3DXVec3Dot(&m_Axis[0], &other->m_Axis[0]);
		C[0][1] = D3DXVec3Dot(&m_Axis[0], &other->m_Axis[1]);
		C[0][2] = D3DXVec3Dot(&m_Axis[0], &other->m_Axis[2]);

		AD[0] = D3DXVec3Dot(&m_Axis[0], &T);

		absC[0][0] = fabsf(C[0][0]);
		absC[0][1] = fabsf(C[0][1]);
		absC[0][2] = fabsf(C[0][2]);

		R = fabsf(AD[0]);
		R1 = 
			other->m_extent[0] * absC[0][0] +
			other->m_extent[1] * absC[0][1] +
			other->m_extent[2] * absC[0][2];
		R01 = m_extent[0] + R1;
		if (R > R01)return 0;
	}

	//A1
	{
		C[1][0] = D3DXVec3Dot(&m_Axis[1], &other->m_Axis[0]);
		C[1][1] = D3DXVec3Dot(&m_Axis[1], &other->m_Axis[1]);
		C[1][2] = D3DXVec3Dot(&m_Axis[1], &other->m_Axis[2]);

		AD[1] = D3DXVec3Dot(&m_Axis[1], &T);

		absC[1][0] = fabsf(C[1][0]);
		absC[1][1] = fabsf(C[1][1]);
		absC[1][2] = fabsf(C[1][2]);

		R = fabsf(AD[1]);
		R1 =
			other->m_extent[0] * absC[1][0] +
			other->m_extent[1] * absC[1][1] +
			other->m_extent[2] * absC[1][2];
		R01 = m_extent[1] + R1;
		if (R > R01)return 0;
	}

	//A2
	{
		C[2][0] = D3DXVec3Dot(&m_Axis[2], &other->m_Axis[0]);
		C[2][1] = D3DXVec3Dot(&m_Axis[2], &other->m_Axis[1]);
		C[2][2] = D3DXVec3Dot(&m_Axis[2], &other->m_Axis[2]);

		AD[2] = D3DXVec3Dot(&m_Axis[2], &T);

		absC[2][0] = fabsf(C[2][0]);
		absC[2][1] = fabsf(C[2][1]);
		absC[2][2] = fabsf(C[2][2]);

		R = fabsf(AD[2]);
		R1 =
			other->m_extent[0] * absC[2][0] +
			other->m_extent[1] * absC[2][1] +
			other->m_extent[2] * absC[2][2];
		R01 = m_extent[2] + R1;
		if (R > R01)return 0;
	}

	//B0
	{
		float dot = D3DXVec3Dot(&other->m_Axis[0], &T);
		R = fabsf(dot);
		R0 =
			m_extent[0] * absC[0][0] +
			m_extent[1] * absC[1][0] +
			m_extent[2] * absC[2][0];
		R01 = R0 + other->m_extent[0];
		if (R > R01)return 0;
	}

	//B1
	{
		float dot = D3DXVec3Dot(&other->m_Axis[1], &T);
		R = fabsf(dot);
		R0 =
			m_extent[0] * absC[0][1] +
			m_extent[1] * absC[1][1] +
			m_extent[2] * absC[2][1];
		R01 = R0 + other->m_extent[1];
		if (R > R01)return 0;
	}

	//B2
	{
		float dot = D3DXVec3Dot(&other->m_Axis[2], &T);
		R = fabsf(dot);
		R0 =
			m_extent[0] * absC[0][2] +
			m_extent[1] * absC[1][2] +
			m_extent[2] * absC[2][2];
		R01 = R0 + other->m_extent[2];
		if (R > R01)return 0;
	}

	
	//A0xB0
	{
		R = fabsf(AD[2] * C[1][0] - AD[1] * C[2][0]);
		R0 = m_extent[1] * absC[2][0] + m_extent[2] * absC[1][0];
		R1 = other->m_extent[1] * absC[0][2] + other->m_extent[2] * absC[0][1];
		R01 = R0 + R1;
		if (R > R01)return 0;
	}

	//A0xB1
	{
		R = fabsf(AD[2] * C[1][1] - AD[1] * C[2][1]);
		R0 = m_extent[1] * absC[2][1] + m_extent[2] * absC[1][1];
		R1 = other->m_extent[0] * absC[0][2] + other->m_extent[2] * absC[0][0];
		R01 = R0 + R1;
		if (R > R01)return 0;
	}
	//A0xB2
	{
		R = fabsf(AD[2] * C[1][2] - AD[1] * C[2][2]);
		R0 = m_extent[1] * absC[2][2] + m_extent[2] * absC[1][2];
		R1 = other->m_extent[0] * absC[0][1] + other->m_extent[1] * absC[0][0];
		R01 = R0 + R1;
		if (R > R01)return 0;
	}

	//A1xB0
	{
		R = fabsf(AD[0] * C[2][0] - AD[2] * C[0][0]);
		R0 = m_extent[0] * absC[2][0] + m_extent[2] * absC[0][0];
		R1 = other->m_extent[1] * absC[1][2] + other->m_extent[2] * absC[1][1];
		R01 = R0 + R1;
		if (R > R01)return 0;
	}

	//A1xB1
	{
		R = fabsf(AD[0] * C[2][1] - AD[2] * C[0][1]);
		R0 = m_extent[0] * absC[2][1] + m_extent[2] * absC[0][1];
		R1 = other->m_extent[0] * absC[1][2] + other->m_extent[2] * absC[1][0];
		R01 = R0 + R1;
		if (R > R01)return 0;
	}
	//A1xB2
	{
		R = fabsf(AD[0] * C[2][2] - AD[2] * C[0][2]);
		R0 = m_extent[0] * absC[2][2] + m_extent[2] * absC[0][2];
		R1 = other->m_extent[0] * absC[1][1] + other->m_extent[1] * absC[1][0];
		R01 = R0 + R1;
		if (R > R01)return 0;
	}

	//A2xB0
	{
		R = fabsf(AD[1] * C[0][0] - AD[0] * C[1][0]);
		R0 = m_extent[0] * absC[1][0] + m_extent[1] * absC[0][0];
		R1 = other->m_extent[1] * absC[2][2] + other->m_extent[2] * absC[2][1];
		R01 = R0 + R1;
		if (R > R01)return 0;
	}

	//A2xB1
	{
		R = fabsf(AD[1] * C[0][1] - AD[0] * C[1][1]);
		R0 = m_extent[0] * absC[1][1] + m_extent[1] * absC[0][1];
		R1 = other->m_extent[0] * absC[2][2] + other->m_extent[2] * absC[2][0];
		R01 = R0 + R1;
		if (R > R01)return 0;
	}

	//A2xB2
	{
		R = fabsf(AD[1] * C[0][2] - AD[0] * C[1][2]);
		R0 = m_extent[0] * absC[1][2] + m_extent[1] * absC[0][2];
		R1 = other->m_extent[0] * absC[2][1] + other->m_extent[1] * absC[2][0];
		R01 = R0 + R1;
		if (R > R01)return 0;
	}

	return true;
}
