#pragma once

#include "IPointDown.h"

class cUIPointDown : public cIPointDown
{
public:
	cUIPointDown();
	virtual ~cUIPointDown();

public:
	class cImage* m_Target = nullptr;

protected:
	// cIPointDown을(를) 통해 상속됨
	virtual void _IPointDown() override;
	// cIPointDown을(를) 통해 상속됨
	virtual const RECT _IGetRect() override;
};

