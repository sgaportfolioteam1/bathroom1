#pragma once
class cObjFileLoad
{
public:
	cObjFileLoad();
	~cObjFileLoad();

	static void _Load(
		IN const std::string & file, 
		class cGameObject * Rootobject);

	//머터리얼 여러개일 시 .. 자료구조 사용
	static void _MaterialLoad(IN const std::string& file);

};

