#pragma once
/*
위치와 관련된 클래스이다.
*/
#include "cComponent.h"

class cTransform : public cComponent
{
public:
	cTransform();
	virtual ~cTransform();
public:
	//로컬 위치에 대한 변수값들
	D3DXVECTOR3		m_localPosition;
	//D3DXQUATERNION	m_localRotation;
	D3DXVECTOR3		m_localRotation;
	D3DXVECTOR3		m_scale;
	D3DXMATRIX		m_matWorld;
public:
	cTransform* m_Parent = nullptr;

	//오버로딩 매개변수 다르다, 함수 이름은 같다
	void _Translation(float x, float y, float z);
	void _Translation(D3DXVECTOR3 position);

	//대입 이동 // 로컬 포지션에 덮어씌우기
	void OverwriteTranslation(float x, float y, float z);

	//회전 함수
	void _Rotate(float x, float y, float z);

	void OverwriteRotate(float x, float y, float z);
	//회전은 추후 수정
	void _Scaling(float x, float y, float z);
	void _Scaling(D3DXVECTOR3 scale);

	inline void _UpdateSRT();

	D3DXMATRIX _UpdateWorld();

	D3DXMATRIX* _GetWorld();
	void _SetWorld(D3DXMATRIX mat);
};

