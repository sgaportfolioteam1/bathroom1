#include "stdafx.h"
#include "cMesh.h"

cMesh::cMesh()
{
}

cMesh::~cMesh()
{
//	SAFE_RELEASE(m_IndicesBuffer);
//	SAFE_RELEASE(m_VertexBuffer);
}

void cMesh::_InitializeIndices(std::vector<UINT> &indices)
{
	m_indices = indices.size();

	D3D11_BUFFER_DESC BufferDesc;
	//정적인가, 동적인가, 선택
	BufferDesc.Usage = D3D11_USAGE_DEFAULT;
	//사이즈 정하기
	BufferDesc.ByteWidth = sizeof(UINT) * indices.size();
	//무슨 버퍼인지 정하기.
	BufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	BufferDesc.CPUAccessFlags = 0;
	BufferDesc.MiscFlags = 0;
	BufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA Date;
	Date.pSysMem = indices.data();
	Date.SysMemPitch = 0;
	Date.SysMemSlicePitch = 0;

	//해당 하는 버퍼 만들어주기
	if (FAILED(cGraphic::GetInstance().GetDevice()->CreateBuffer(
		&BufferDesc, &Date, &m_IndicesBuffer)))
		assert("IndicesBuffer 에러");
}

void cMesh::_Render()
{
	const UINT stride = m_stride;
	const UINT offset = 0;

	//입력 어셈블러에서(IA) 정점버퍼 넣기
	if (m_VertexBuffer)
		cGraphic::GetInstance().GetDeviceContext()->
		IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

	if (m_IndicesBuffer)
	cGraphic::GetInstance().GetDeviceContext()->
		IASetIndexBuffer(m_IndicesBuffer, DXGI_FORMAT_R32_UINT, 0);
}
