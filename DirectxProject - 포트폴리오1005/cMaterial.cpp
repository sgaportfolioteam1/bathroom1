#include "stdafx.h"
#include "cMaterial.h"

//#include "cLightShader.h"
#include "cLightViewShader.h"

cMaterial::cMaterial()
{
}

cMaterial::~cMaterial()
{
}

void cMaterial::_Update(D3DXMATRIX *m, D3DXMATRIX v, D3DXMATRIX p)
{
	m_Shader->_Update(*m, v, p);
}

void cMaterial::_Render(UINT indexCount)
{
	m_Shader->_Render(indexCount);
}
