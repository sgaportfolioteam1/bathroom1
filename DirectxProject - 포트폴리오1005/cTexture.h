#pragma once

#include "cRefObject.h"

class cTexture : public cRefObject
{
public:
	cTexture();
	virtual ~cTexture();

private:
	ID3D11ShaderResourceView* m_ShaderResource = nullptr;
	ID3D11SamplerState*	m_SampleState = nullptr;
public:
	std::string m_name;

	void _Initialize(const std::string& image);
	void _Render();
};

