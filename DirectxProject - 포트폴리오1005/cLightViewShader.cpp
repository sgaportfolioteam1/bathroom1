#include "stdafx.h"
#include "cLightViewShader.h"

#include "cTexture.h"

cLightViewShader::cLightViewShader()
{
}


cLightViewShader::~cLightViewShader()
{
	SAFE_RELEASE(m_lightBuffer);
	SAFE_RELEASE(m_cameraBuffer);
	SAFE_RELEASE(m_matrixBuffer);

	SAFE_RELEASE(m_InputLayout);
	SAFE_RELEASE(m_PS_Shader);
	SAFE_RELEASE(m_VS_Shader);

	SAFE_RELEASE(m_Texture);
}

void cLightViewShader::_Initialize()
{
	//VS. PS, InputLayout
	{
		HRESULT result;

		ID3DBlob* vertexShaderBuffer = nullptr;
		ID3DBlob* pixelShaderBuffer = nullptr;

		result = D3DX11CompileFromFileA(
			"LightViewShader.hlsl", NULL, NULL, "VS", "vs_5_0",
			0, 0, NULL,
			&vertexShaderBuffer, NULL, NULL);

		assert(SUCCEEDED(result));

		/////PS///////
		result = D3DX11CompileFromFileA(
			"LightViewShader.hlsl", NULL, NULL, "PS", "ps_5_0",
			0, 0, NULL,
			&pixelShaderBuffer, NULL, NULL);
		assert(SUCCEEDED(result));

		//버퍼로부터 정점 셰이더를 생성한다.
		result = cGraphic::GetInstance().GetDevice()->
			CreateVertexShader(vertexShaderBuffer->GetBufferPointer(),
				vertexShaderBuffer->GetBufferSize(),
				NULL, &m_VS_Shader);
		assert(SUCCEEDED(result));

		//버퍼로부터 픽셀 셰이더를 생성한다.
		result = cGraphic::GetInstance().GetDevice()->
			CreatePixelShader(pixelShaderBuffer->GetBufferPointer(),
				pixelShaderBuffer->GetBufferSize(),
				NULL, &m_PS_Shader);
		assert(SUCCEEDED(result));

		//정점 입력 레이아웃 구조체를 설정한다.
		//이 설정은 내가 보낼 구조체와, 쉐이더의 구조체와 일치해야된다.
		D3D11_INPUT_ELEMENT_DESC input_desc[]
		{
			{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,
			0, 0, D3D11_INPUT_PER_VERTEX_DATA,0},
			{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,
			0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA,0},
			{"NORMALIZE", 0, DXGI_FORMAT_R32G32B32_FLOAT,
			0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA,0}
		};

		auto hr = cGraphic::GetInstance().GetDevice()->
			CreateInputLayout(
				input_desc,	//desc 정보
				3,			//원소 갯수
				vertexShaderBuffer->GetBufferPointer(),
				vertexShaderBuffer->GetBufferSize(),
				&m_InputLayout);	//초기화 할 정보

		assert(SUCCEEDED(hr));

		//더이상 사용안하니 해제
		SAFE_RELEASE(vertexShaderBuffer);
		SAFE_RELEASE(pixelShaderBuffer);

		//World View Projection Buffer
		{
			//matrix 만들기.
			D3D11_BUFFER_DESC matrixBufferDesc;
			//정적인가, 동적인가, 선택
			matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
			//사이즈 정하기
			matrixBufferDesc.ByteWidth = sizeof(ST__WVP);
			//무슨 버퍼인지 정하기.
			matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			matrixBufferDesc.MiscFlags = 0;
			matrixBufferDesc.StructureByteStride = 0;

			hr = cGraphic::GetInstance().GetDevice()->CreateBuffer(
				&matrixBufferDesc, NULL, &m_matrixBuffer);

			assert(SUCCEEDED(hr));
		}

		//Camera Buffer
		{
			D3D11_BUFFER_DESC cameraBufferDesc;
			//정적인가, 동적인가, 선택
			cameraBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
			//사이즈 정하기
			cameraBufferDesc.ByteWidth = sizeof(ST__CameraBuffer);
			//무슨 버퍼인지 정하기.
			cameraBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			cameraBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			cameraBufferDesc.MiscFlags = 0;
			cameraBufferDesc.StructureByteStride = 0;

			hr = cGraphic::GetInstance().GetDevice()->CreateBuffer(
				&cameraBufferDesc, NULL, &m_cameraBuffer);

			assert(SUCCEEDED(hr));
		}
		//LightView Buffer
		{
			D3D11_BUFFER_DESC lightBufferDesc;
			//정적인가, 동적인가, 선택
			lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
			//사이즈 정하기
			lightBufferDesc.ByteWidth = sizeof(ST__LightViewBuffer);
			//무슨 버퍼인지 정하기.
			lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			lightBufferDesc.MiscFlags = 0;
			lightBufferDesc.StructureByteStride = 0;

			hr = cGraphic::GetInstance().GetDevice()->CreateBuffer(
				&lightBufferDesc, NULL, &m_lightBuffer);

			assert(SUCCEEDED(hr));
		}
	}
}

void cLightViewShader::_Update(D3DXMATRIX m, D3DXMATRIX v, D3DXMATRIX p)
{
	//셰이더에서와 처리방식이 다름. 셰이더에선 열로 행렬 처리를 함.
	{
		D3DXMatrixTranspose(&m, &m);
		D3DXMatrixTranspose(&v, &v);
		D3DXMatrixTranspose(&p, &p);
	}

	//matrixBuffer 
	{
		//상수 버퍼 내용을 쓸 수 있도록 잠금
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		if (FAILED(cGraphic::GetInstance().GetDeviceContext()->Map(
			m_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource
		)))
		{
			return;
		};

		//상수 버퍼의 데이터에 대한 포인터를 가져옴.
		ST__WVP* dataPtr = (ST__WVP*)mappedResource.pData;
		//상수 버퍼에 행렬을 복사
		dataPtr->m_worldMatrix = m;
		dataPtr->m_viewMatrix = v;
		dataPtr->m_projectionMatrix = p;

		//상수 버퍼의 잠금을 푼다.
		cGraphic::GetInstance().GetDeviceContext()->Unmap(
			m_matrixBuffer, 0
		);
	}

	//Camera Buffer 
	{
		//상수 버퍼 내용을 쓸 수 있도록 잠금
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		if (FAILED(cGraphic::GetInstance().GetDeviceContext()->Map(
			m_cameraBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource
		)))
		{
			return;
		};

		//상수 버퍼의 데이터에 대한 포인터를 가져옴.
		ST__CameraBuffer* dataPtr = (ST__CameraBuffer*)mappedResource.pData;
		dataPtr->m_cameraPosition = g_CameraPosition;
		dataPtr->m_padding = 0;

		//상수 버퍼의 잠금을 푼다.
		cGraphic::GetInstance().GetDeviceContext()->Unmap(
			m_cameraBuffer, 0
		);
	}

	//LightView Buffer 
	{
		//상수 버퍼 내용을 쓸 수 있도록 잠금
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		if (FAILED(cGraphic::GetInstance().GetDeviceContext()->Map(
			m_lightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource
		)))
		{
			return;
		};

		//상수 버퍼의 데이터에 대한 포인터를 가져옴.
		ST__LightViewBuffer* dataPtr = (ST__LightViewBuffer*)mappedResource.pData;
		//상수 버퍼에 행렬을 복사
		//이것 관련된 것은 태양광, 전역으로 만들어서 처리를 하자.
		dataPtr->ambientColor = D3DXCOLOR(0.15f, 0.15f, 0.15f, 1.0f);// m_materialRef->m_DiffuseColor;//D3DXCOLOR(1,1,1,1);
		dataPtr->diffuseColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		dataPtr->lightDirection = D3DXVECTOR3(0, 0, 1);
		dataPtr->specularColor = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		dataPtr->specularPower = 32.f;

		//상수 버퍼의 잠금을 푼다.
		cGraphic::GetInstance().GetDeviceContext()->Unmap(
			m_lightBuffer, 0
		);
	}
}

void cLightViewShader::_Render(UINT renderCount)
{
	//삼각형으로 그리겠다.
	cGraphic::GetInstance().GetDeviceContext()->
		IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	cGraphic::GetInstance().GetDeviceContext()->
		IASetInputLayout(m_InputLayout);

	////////////////////VS/////////////////////////
	cGraphic::GetInstance().GetDeviceContext()->
		VSSetShader(m_VS_Shader, NULL, 0);
	cGraphic::GetInstance().GetDeviceContext()->
		VSSetConstantBuffers(0, 1, &m_matrixBuffer);
	cGraphic::GetInstance().GetDeviceContext()->
		VSSetConstantBuffers(1, 1, &m_cameraBuffer);

	////////////////////PS///////////////////////////////
	cGraphic::GetInstance().GetDeviceContext()->
		PSSetShader(m_PS_Shader, NULL, 0);

	if (m_Texture)
		m_Texture->_Render();

	cGraphic::GetInstance().GetDeviceContext()->
		PSSetConstantBuffers(0, 1, &m_lightBuffer);

	/////////////////////Draw///////////////////
	//뭘로 기준으로 랜더를 하나.
	//랜더 타입도 정해야 된다.
	cGraphic::GetInstance().GetDeviceContext()->
		DrawIndexed(renderCount,0, 0);
}
