#pragma once

#include <assert.h>

class cRefObject
{
public:
	cRefObject()
		: m_refCount(1) //생성하면 참조카운트는 1로 초기화 된다.
	{
	}
	virtual ~cRefObject()
	{
		//false 일시 true 프로그램 멈춤
		//Release를 호출해서 지워야 된다.
		//1의 값이 있는 상황에서 delete되었다.
		assert(m_refCount >= 0 && "어디서 Delete 호출되었습니다.");
	}

private:
	//참조 횟수를 알려줄 카운트
	int m_refCount;

public:
	void AddRef()
	{
		//참조 카운트 횟수 올려주기
		m_refCount++;
	}
	void Release()
	{
		m_refCount--;

		if (m_refCount <= 0)
			delete this;
	}
};

