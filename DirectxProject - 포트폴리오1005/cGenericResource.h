#pragma once

#include <list>
#define SAFE_RELEASE(p)		{ if(p) { (p)->Release();	(p) = nullptr; } }

template<typename cResource>
class cGenericResource
{
public:
	cGenericResource();
	~cGenericResource();

private:
	std::list<cResource*> m_list;

public:
	cResource* _FindResource(const std::string& name);
	cResource* _AddResource(cResource* resource);
};

template<typename cResource>
inline cGenericResource<cResource>::cGenericResource()
{
}

template<typename cResource>
inline cGenericResource<cResource>::~cGenericResource()
{
	typename std::list<cResource*>::iterator iter = m_list.begin();

	for (iter; iter != m_list.end(); )
	{
		cResource* ref = (*iter);
		iter = m_list.erase(iter);
		SAFE_RELEASE(ref);
	}
}

template<typename cResource>
inline cResource * cGenericResource<cResource>::_FindResource(const std::string & name)
{
	typename std::list<cResource*>::iterator iter = m_list.begin();

	for (iter; iter != m_list.end(); ++iter)
	{
		//이름과 같은게 있을 시 반환하도록 한다.
		if ((*iter)->m_name._Equal(name))
		{
			return *iter;
		}
	}

	return NULL;
}

template<typename cResource>
inline cResource * cGenericResource<cResource>::_AddResource(cResource * resource)
{
	//혹시나 같은 리소스를 넣지 않도록 
	//count감소 혹은 예외처리를 할것.
	//Addreff 올려 줄 것.
	//딱히.. 이름중복 있을까?
	m_list.push_back(resource);

	return resource;
}