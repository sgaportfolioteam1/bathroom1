#include "stdafx.h"
#include "cObjFileLoad.h"

#include "cGameObject.h"
#include "cMesh.h"
#include "cTexture.h"
#include "cMaterial.h"
#include "cLightShader.h"
#include "cLightViewShader.h"
//#include "cTextureShader.h"

cObjFileLoad::cObjFileLoad()
{
}


cObjFileLoad::~cObjFileLoad()
{
}

void cObjFileLoad::_Load(IN const std::string & file,
	cGameObject * Rootobject)
{
	//파일 열기
	FILE *fp;
	fopen_s(&fp, file.c_str(), "r");

	//예외처리
	//assert(false)실행된다. //프로그램을 멈추게 한다
	assert(fp != nullptr);

	// P T N 정보 담기
	std::vector<D3DXVECTOR3> vP;
	std::vector<D3DXVECTOR2> vT;
	std::vector<D3DXVECTOR3> vN;
	std::vector<ST__Vertex_PTN> vPTN;
	std::vector<UINT> vIdices;
	UINT idx = 0;

	std::string objectName = "";
	std::string mtlName = "";
	//읽기 시작

	//그룹 추가할 것이다.
	while (!feof(fp))
	{
		char buffer[1024];
		fgets(buffer, 1024, fp);

		//주석은 건너뛰기
		if (buffer[0] == '#')
			continue;

		else if (buffer[0] == 'm')
		{
			char mtlfileName[1024];
			sscanf_s(buffer, "%*s %s", mtlfileName, 1024);
			_MaterialLoad(mtlfileName);
		}
		else if (buffer[0] == 'g')
		{
			//버텍스의 정보가 없을 시, 메쉬 구성이 안되었을 시
			if (vPTN.empty())
			{
				//g         정보가 없기 때문에 건너뛴다.
				//g Box01이 와도 정보가 없기에 건너뛴다.
				char szMtlFileName[1024];
				sscanf_s(buffer, "%*s %s", szMtlFileName, 1024);

				//이름이 있다는 걸로 간주하고.
				if (buffer[1] != '\n')
				{
					//이름 갱신 
					objectName = szMtlFileName;
				}
				continue;
			}

			else //데이터를 채워주기
			{
				//자식 오브젝트 생성
				cGameObject* object = new cGameObject();
				object->m_name = objectName;

				//메쉬 연결
				cMesh* mesh = new cMesh();
				mesh->m_name = objectName;
				mesh->_InitializeVertex(vPTN);
				mesh->_InitializeIndices(vIdices);
				object->m_Mesh = mesh;
				//MANAGERRESOURCE->_SetMesh(mesh);
				MANAGERRESOURCE->m_Meshs->_AddResource(mesh);
				mesh->AddRef(); //누가 사용중.

				//머터리얼 사용
				object->m_Material = 
					//MANAGERRESOURCE->_GetMaterial(mtlName);
				MANAGERRESOURCE->m_Materials->_FindResource(mtlName);

				Rootobject->m_child.push_back(object);

				//vP.clear();
				//vT.clear();
				//vN.clear();
				vPTN.clear();
				idx = 0;
			}
		}
		//오브젝트가 사용하는 머터리얼
		else if (buffer[0] == 'u')
		{
			char mtlfileName[1024];
			sscanf_s(buffer, "%*s %s", mtlfileName, 1024);
			mtlName = mtlfileName;
		}

		//버택스 정보 일때
		else if (buffer[0] == 'v')
		{
			//p 정보
			if (buffer[1] == ' ')
			{
				float x, y, z;
				sscanf_s(buffer, 
					"%*s %f %f %f",
					&x, &y, &z);
				vP.push_back(D3DXVECTOR3(x, y, z));
				//vP.push_back(D3DXVECTOR3(x, z, y));
			}
			//T 정보
			else if (buffer[1] == 't')
			{
				float u, v;
				sscanf_s(buffer,
					"%*s %f %f",
					&u, &v);
				vT.push_back(D3DXVECTOR2(u, v));
			}
			//N 정보
			else if (buffer[1] == 'n')
			{
				float x, y, z;
				sscanf_s(buffer,
					"%*s %f %f %f",
					&x, &y, &z);
				vN.push_back(D3DXVECTOR3(x, y,z));
			}
		}

		//면 정보
		else if (buffer[0] == 'f')
		{
			int Index[3][3];
			sscanf_s(buffer,
				"%*s %d/%d/%d %d/%d/%d %d/%d/%d",
				&Index[0][0], &Index[0][1], &Index[0][2],
				&Index[1][0], &Index[1][1], &Index[1][2],
				&Index[2][0], &Index[2][1], &Index[2][2]);

			for (int i = 0; i < 3; ++i)
			{
				//페이스 정보 0부터아닌 1부터 시작해서 -1해줌
				ST__Vertex_PTN v;
				v.m_position =  vP[Index[i][0] - 1];
				v.m_texcoord =  vT[Index[i][1] - 1];
				v.m_normalize = vN[Index[i][2] - 1];

				//최종 정점 데이터
				vPTN.push_back(v);

				//순서대로 인덱스 셋팅하기
				vIdices.push_back(idx);
				idx++;
			}
		}
	}
	//파일 닫기.
	fclose(fp);
}

void cObjFileLoad::_MaterialLoad(
	IN const std::string & file)
{
	//파일 열기
	FILE *fp;
	fopen_s(&fp, file.c_str(), "r");

	//예외처리
	//assert(false)실행된다. //프로그램을 멈추게 한다
	assert(fp != nullptr);

	cMaterial* mtl = nullptr;

	//파일 읽기
	while (!feof(fp))
	{
		char buffer[1024];
		fgets(buffer, 1024, fp);

		//주석은 건너뛰기
		if (buffer[0] == '#')
			continue;

		else if (buffer[0] == 'n')
		{
			char mtlName[1024];
			sscanf_s(buffer, "%*s %s", mtlName, 1024);
			
			//새로운 머터리얼 생성 후
			mtl = new cMaterial();
			mtl->m_name = mtlName;
			//쉐이더 연결시켜주기, Ref 적용안시켰음.
			mtl->m_Shader = MANAGERRESOURCE->m_LightViewShader;
			//리스트에 저장
			//MANAGERRESOURCE->_SetMaterial(mtl);
			MANAGERRESOURCE->m_Materials->_AddResource(mtl);
		}
		//a, d, s
		else if (buffer[0] == 'K')
		{
			//a 정보
			if (buffer[1] == 'a')
			{
				float r, g, b;
				sscanf_s(buffer,
					"%*s %f %f %f",
					&r, &g, &b);
				
				mtl->m_AmbientColor =
					D3DXCOLOR(r, g, b, 1);
			}
			//d 정보
			else if (buffer[1] == 'd')
			{
				float r, g, b;
				sscanf_s(buffer,
					"%*s %f %f %f",
					&r, &g, &b);

				mtl->m_DiffuseColor =
					D3DXCOLOR(r, g, b, 1);
			}
			//s 정보
			else if (buffer[1] == 's')
			{
				float r, g, b;
				sscanf_s(buffer,
					"%*s %f %f %f",
					&r, &g, &b);

				mtl->m_SpecularColor =
					D3DXCOLOR(r, g, b, 1);
			}
		}

		//map_Kd ./box.jpg... 파일 경로
		else if (buffer[0] == 'm')
		{
			char imageFileName[1024];
			sscanf_s(buffer, "%*s %s", imageFileName, 1024);

			//cTexture* find = MANAGERRESOURCE->_GetTexture(imageFileName);
			cTexture* find = MANAGERRESOURCE->m_Textures->_FindResource(imageFileName);
			if (find == nullptr)
			{
				//새로운 텍스쳐 생성
				cTexture* texture = new cTexture();
				texture->_Initialize(imageFileName);
				texture->m_name = imageFileName;
				//리소스 매니져에 저장
				//MANAGERRESOURCE->_SetTexture(texture);
				MANAGERRESOURCE->m_Textures->_AddResource(texture);

				//머터리얼에 쉐이더 텍스쳐 연결
				mtl->m_Shader->m_Texture = texture;
				texture->AddRef();
			}
		}
	}

	fclose(fp);
}
