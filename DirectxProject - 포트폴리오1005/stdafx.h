#pragma once

#define NOMINMAX

#if _DEBUG
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console")
#endif

#include <Windows.h>
#include <assert.h>
#include <iostream>

//다이렉트 추가.
#include <d3dcompiler.h>
#include <d3d11.h>
#include <D3DX11async.h>
#include <D3DX10math.h>

//라이브러리 추가
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")

#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib, "d3dx10.lib")

#pragma comment(lib, "d3dcompiler.lib")

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
//
#pragma comment(lib, "assimp.lib")
#pragma comment(lib, "fmodex_vc.lib")


#include <assimp/Importer.hpp>  // C++ importer interface
#include <assimp/scene.h>       // Output data structure
#include <assimp/postprocess.h> // Post processing flags

//
#include <string>
#include <vector>
#include <list>
#include <map>

using namespace std;
//
#include "Setting.h"
#include "cGraphic.h"
#include "cManagerResource.h"
#include "cManagerInput.h"

#define MANAGERRESOURCE cManagerResource::GetSingleton()
#define MANAGERINPUT cManagerInput::GetSingleton()

//
#define SAFE_DELETE(p)		{ if(p) { delete (p);		(p) = nullptr; } }
#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);		(p) = nullptr; } }
#define SAFE_RELEASE(p)		{ if(p) { (p)->Release();	(p) = nullptr; } }
#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))

extern D3DXVECTOR3 g_CameraPosition;
extern bool g_Click;
extern bool g_Pass;
extern POINT Winpos;


struct ST__Vertex_P
{
	D3DXVECTOR3 m_position;
	ST__Vertex_P()
	{ }
	ST__Vertex_P(D3DXVECTOR3& p)
		: m_position(p)
	{ }
};

struct ST__Vertex_PC
{
	D3DXVECTOR3 m_position;
	D3DXCOLOR	m_color;

	ST__Vertex_PC()
	{ }
	ST__Vertex_PC(D3DXVECTOR3& p, D3DXCOLOR &color)
		: m_position(p), m_color(color)
	{ }
};

struct ST__Vertex_PN
{
	D3DXVECTOR3 m_position;
	D3DXVECTOR3	m_normalize;
};

struct ST__Vertex_PT
{
	D3DXVECTOR3 m_position;
	D3DXVECTOR2	m_texcoord;
};

struct ST__Vertex_PTN
{
	D3DXVECTOR3 m_position;
	D3DXVECTOR2	m_texcoord;
	D3DXVECTOR3	m_normalize;
};

struct ST__Vertex_Obj
{
	//aiVector3D m_position;
	//aiVector3D	m_texcoord;
	//aiVector3D m_normalize;

	D3DXVECTOR3 m_position;
	D3DXVECTOR2	m_texcoord;
	D3DXVECTOR3 m_normalize;
};



#define NUM_BONES_PER_VEREX 4

struct ST__Vertex_SKIN
{
	D3DXVECTOR3 m_position;
	D3DXVECTOR2 m_texcoord; //8
	D3DXVECTOR3 m_normal;	//12
	UINT m_boneId[NUM_BONES_PER_VEREX]; //16
	float m_weights[NUM_BONES_PER_VEREX]; //16

	ST__Vertex_SKIN()
	{
		ZeroMemory(m_boneId, sizeof(m_boneId));
		ZeroMemory(m_weights, sizeof(m_weights));
	}

	void AddBoneData(UINT BoneID, float Weight)
	{
		for (UINT i = 0; i < NUM_BONES_PER_VEREX; i++)
		{
			if (m_weights[i] == 0.0) {
				m_boneId[i] = BoneID;
				m_weights[i] = Weight;
				return;
			}
		}
	}
};

struct ST__BoneWeight
{
	UINT BondIndex[NUM_BONES_PER_VEREX];
	float Weights[NUM_BONES_PER_VEREX];

	ST__BoneWeight()
	{
		ZeroMemory(BondIndex, sizeof(BondIndex));
		ZeroMemory(Weights, sizeof(Weights));
	}

	void AddBoneData(UINT BoneID, float Weight)
	{
		for (UINT i = 0; i < NUM_BONES_PER_VEREX; i++)
		{
			if (Weights[i] == 0.0) {
				BondIndex[i] = BoneID;
				Weights[i] = Weight;
				return;
			}
		}
	}
};

struct ST__BindPose
{
	aiMatrix4x4 BoneOffset;
	aiMatrix4x4 FinalTransformation;
};

struct ST__WVP
{
	D3DXMATRIX m_worldMatrix;		//월드 공간
	D3DXMATRIX m_viewMatrix;		//카메라 공간
	D3DXMATRIX m_projectionMatrix;	//투영
};

//픽셀 단계 전용 버퍼
struct ST__Light
{
	D3DXCOLOR diffuseColor;
	D3DXVECTOR3 lightDirection;
	float padding;
};

//픽셀 단계 전용 버퍼
struct ST__LightAmbient
{
	D3DXCOLOR ambientColor;
	D3DXCOLOR diffuseColor;
	D3DXVECTOR3 lightDirection;
	float padding;
};

struct ST__CameraBuffer
{
	D3DXVECTOR3 m_cameraPosition;
	float m_padding;
};

//픽셀에 쓸 라이트 버퍼
struct ST__LightViewBuffer
{
	D3DXCOLOR ambientColor;
	D3DXCOLOR diffuseColor;
	D3DXVECTOR3 lightDirection;
	float specularPower;
	D3DXCOLOR specularColor;
};

struct ST__BoneBuffer
{
	D3DXMATRIX bons[100];
};

struct ST__ColorBuffer
{
	D3DXCOLOR color;
};

class cGetRect
{
public:
	virtual const RECT _IGetRect() = 0;
};
