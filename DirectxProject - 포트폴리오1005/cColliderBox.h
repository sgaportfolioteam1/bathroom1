#pragma once
class cColliderBox
{
public:
	cColliderBox();
	~cColliderBox();

public:
	D3DXVECTOR3 m_Center;
	D3DXVECTOR3 m_Axis[3];
	D3DXVECTOR3 m_extent;

public:
	bool _Collider(cColliderBox* other);
};

