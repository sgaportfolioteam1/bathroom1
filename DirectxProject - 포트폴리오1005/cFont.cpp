#include "stdafx.h"
#include "cFont.h"

#include <fstream>

cFont::cFont()
{
}


cFont::~cFont()
{
}

bool cFont::_LoadFontData(const std::string & file)
{
	std::ifstream fin;
	int i;
	char temp;

	// Create the font spacing buffer.
	m_fontDate.resize(95);

	// Read in the font size and spacing between chars.
	fin.open(file);
	if (fin.fail())
	{
		return false;
	}

	// Read in the 95 used ascii characters for text.
	for (i = 0; i < 95; i++)
	{
		fin.get(temp);
		while (temp != ' ')
		{
			fin.get(temp);
		}
		fin.get(temp);
		while (temp != ' ')
		{
			fin.get(temp);
		}

		fin >> m_fontDate[i].left;
		fin >> m_fontDate[i].right;
		fin >> m_fontDate[i].size;
	}

	// Close the file.
	fin.close();

	return true;
}

UINT cFont::BuildVertexArray(void * vertices, 
	const char * sentence, float drawX, float drawY)
{
	ST__Vertex_PT* vertexPtr;
	int numLetters, index, i, letter;

	// Coerce the input vertices into a VertexType structure.
	vertexPtr = (ST__Vertex_PT*)vertices;

	// Get the number of letters in the sentence.
	numLetters = (int)strlen(sentence);

	// Initialize the index to the vertex array.
	index = 0;

	//TODO
	drawX = 0;
	drawY = 0;

	// Draw each letter onto a quad.
	for (i = 0; i < numLetters; i++)
	{
		letter = ((int)sentence[i]) - 32;

		// If the letter is a space then just move over three pixels.
		if (letter == 0)
		{
			drawX = drawX + 3.0f;
		}
		else
		{
			// First triangle in quad.
			vertexPtr[index].m_position = D3DXVECTOR3(drawX, drawY, 0.0f);  // Top left.
			vertexPtr[index].m_texcoord = D3DXVECTOR2(m_fontDate[letter].left, 0.0f);
			index++;

			vertexPtr[index].m_position = D3DXVECTOR3((drawX + m_fontDate[letter].size), (drawY - 16), 0.0f);  // Bottom right.
			vertexPtr[index].m_texcoord = D3DXVECTOR2(m_fontDate[letter].right, 1.0f);
			index++;

			vertexPtr[index].m_position = D3DXVECTOR3(drawX, (drawY - 16), 0.0f);  // Bottom left.
			vertexPtr[index].m_texcoord = D3DXVECTOR2(m_fontDate[letter].left, 1.0f);
			index++;

			// Second triangle in quad.
			vertexPtr[index].m_position = D3DXVECTOR3(drawX, drawY, 0.0f);  // Top left.
			vertexPtr[index].m_texcoord = D3DXVECTOR2(m_fontDate[letter].left, 0.0f);
			index++;

			vertexPtr[index].m_position = D3DXVECTOR3(drawX + m_fontDate[letter].size, drawY, 0.0f);  // Top right.
			vertexPtr[index].m_texcoord = D3DXVECTOR2(m_fontDate[letter].right, 0.0f);
			index++;

			vertexPtr[index].m_position = D3DXVECTOR3((drawX + m_fontDate[letter].size), (drawY - 16), 0.0f);  // Bottom right.
			vertexPtr[index].m_texcoord = D3DXVECTOR2(m_fontDate[letter].right, 1.0f);
			index++;

			// Update the x location for drawing by the size of the letter and one pixel.
			drawX = drawX + m_fontDate[letter].size + 1.0f;
			//drawX = drawX + m_fontDate[letter].size;
		}
	}

	return index;
}
