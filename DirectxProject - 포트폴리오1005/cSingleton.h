#pragma once

/*
�̱��� : �޸𸮿� �ϳ� �� �÷��� ȣ���ؼ� �����
*/

//���ø� ���
template <class T>
class cSingleton
{
protected:
	static T* singleton;

	//�����ڸ� public�� ������ �ʴ´�.
	cSingleton() { }
	~cSingleton() { }

public:
	//�̱��� ȣ��
	static T* GetSingleton();
	//�̱��� �޸𸮿��� �����ϱ�.
	void ReleaseSingleton();
};

//�̱��� �ʱ�ȭ
template<typename T>
T* cSingleton<T>::singleton = 0;

//�̱��� ��������.
template<class T>
inline T * cSingleton<T>::GetSingleton()
{
	//�̱����� ���ٸ� ���� ����
	if (!singleton)
		singleton = new T;

	//�̱����� ������ ������ ȣ���ϱ�.
	return singleton;
}

//�̱��� �����ϱ�.
template<class T>
inline void cSingleton<T>::ReleaseSingleton()
{
	if (singleton)
	{
		delete singleton;
		singleton = 0;
	}
}