#pragma once

class cMesh;
class cFont;
class cFontShader;

class cText
{
public:
	cText();
	~cText();
public:
	//폰트 정보
	cFont* m_Font = nullptr;
	cFontShader * m_FontShader = nullptr;

	//텍스트 매쉬
	cMesh* m_Mesh = nullptr;

	int m_screenWidth;
	int m_screenHeight;

	float m_x;
	float m_y;

public:
	void _Initialize();
	void _Update(float x, float y, D3DXMATRIX* v, D3DXMATRIX *p);
	void _Render();

	void _SetText(const std::string &text);
};

