#pragma once

class cMesh;
class cTexture;
class cTextureShader;

//Person : public Student
//Student : public Person

class cImage : private cGetRect
{
public:
	cImage();
	~cImage();

private:
	cMesh* m_mesh = nullptr;

	float m_screenWidth;
	float m_screenHeight;

	float m_x;
	float m_y;

	float m_Width;
	float m_Height;

public:
	cTextureShader* m_TextureShader = nullptr;
	RECT m_Rect;

	cTexture* m_Texture = nullptr;

public:
						//파일 경로, 이름, x크기, y크기
	void _Initialize(string file, string name, float width, float height);
	void _Update(float x, float y, D3DXMATRIX *v, D3DXMATRIX *p);
	void _Render();

	// cGetRect을(를) 통해 상속됨
	virtual const RECT _IGetRect() override;

	RECT RectMakeCenter(int x, int y, int width, int height);
	
	void SetMeshTextureUVUpdate(float x, float y, D3DXMATRIX* v, D3DXMATRIX* p, D3DXVECTOR4 setUV);
};