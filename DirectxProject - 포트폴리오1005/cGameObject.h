#pragma once
/*
게임 오브젝트 각종 기능을 넣어
표현하는 물체
*/
#include <typeinfo>

class cComponent;

class cTransform;

class cMesh;
class cMaterial;

class cGameObject
{
public:
	cGameObject();
	~cGameObject();

	std::vector<cGameObject*> m_child;
	std::string m_name = "GameObject";

	//지금은 때려박는 중.
	cMesh* m_Mesh = nullptr;
	cMaterial* m_Material = nullptr;
	cTransform* m_Transform = nullptr;
	//cTextureShader* m_TextureShader = nullptr;
	//cLightShader* m_LightShader = nullptr;

	void _Initialize();
	void _Update(D3DXMATRIX *m, D3DXMATRIX v, D3DXMATRIX p);
	void _Render();

	void _ComponentUpdate();

	std::vector<cComponent*> msg;
	template <typename T>
	T* GetComponent();
};

template<typename T>
inline T*  cGameObject::GetComponent()
{
	for (UINT i = 0; i < msg.size(); ++i)
	{
		//dynamic_cast 별로... 안좋다...
		if ((typeid(*msg[i]) == typeid(T)) != 0)
		{
			return dynamic_cast<T*>(msg[i]);
		}
	}
	return nullptr;
}
