#include "stdafx.h"
#include "cTerrain.h"

#include "cMesh.h"
#include "cLightShader.h"
#include "cTexture.h"

cTerrain::cTerrain()
{
	m_mesh = new cMesh();

	cTexture* Texture = new cTexture();
	Texture->_Initialize("../Resources/dirt01.dds");

	m_LightShader = new cLightShader();
	m_LightShader->_Initialize();
	m_LightShader->m_Texture = Texture;
}

cTerrain::~cTerrain()
{
	if (m_heightMap)
	{
		delete[] m_heightMap;
		m_heightMap = 0;
	}

	SAFE_RELEASE(m_mesh);
	SAFE_DELETE(m_LightShader);
}

void cTerrain::LoadHeightMap(const std::string & file)
{
	FILE* filePtr = nullptr;
	if (fopen_s(&filePtr, file.c_str(), "rb") != 0)
	{
		assert(false);
	}

	BITMAPFILEHEADER bitmapFileHeader;
	if (fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr) != 1)
	{
		assert(false);
	}

	BITMAPINFOHEADER bitmapInfoHeader;
	if (fread(&bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr) != 1)
	{
		assert(false);
	}

	m_terrainWidth = bitmapInfoHeader.biWidth;
	m_terrainHeight = bitmapInfoHeader.biHeight;

	int imageSize = m_terrainWidth * m_terrainHeight * 3;

	unsigned char* bitmapImage = new unsigned char[imageSize];
	if (!bitmapImage)
	{
		assert(false);
	}

	fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

	if (fread(bitmapImage, 1, imageSize, filePtr) != imageSize)
	{
		assert(false);
	}

	if (fclose(filePtr) != 0)
	{
		assert(false);
	}

	m_heightMap = new HeightMapType[m_terrainWidth * m_terrainHeight];
	if (!m_heightMap)
	{
		assert(false);
	}

	int k = 0;

	for (int j = 0; j < m_terrainHeight; j++)
	{
		for (int i = 0; i < m_terrainWidth; i++)
		{
			unsigned char height = bitmapImage[k];

			int index = (m_terrainHeight * j) + i;

			m_heightMap[index].x = (float)i;
			m_heightMap[index].y = (float)height;
			m_heightMap[index].z = (float)j;

			k += 3;
		}
	}

	delete[] bitmapImage;
	bitmapImage = 0;
}

void cTerrain::NormalizeHeightMap()
{
	for (int j = 0; j < m_terrainHeight; j++)
	{
		for (int i = 0; i < m_terrainWidth; i++)
		{
			m_heightMap[(m_terrainHeight * j) + i].y /= 15.0f;
		}
	}
}

bool cTerrain::CalculateNormals()
{
	int index1 = 0;
	int index2 = 0;
	int index3 = 0;
	int index = 0;
	int count = 0;
	float vertex1[3] = { 0.f, 0.f, 0.f };
	float vertex2[3] = { 0.f, 0.f, 0.f };
	float vertex3[3] = { 0.f, 0.f, 0.f };
	float vector1[3] = { 0.f, 0.f, 0.f };
	float vector2[3] = { 0.f, 0.f, 0.f };
	float sum[3] = { 0.f, 0.f, 0.f };
	float length = 0.0f;

	// 정규화되지 않은 법선 벡터를 저장할 임시 배열을 만듭니다.
	std::vector<D3DXVECTOR3> normals;
	normals.resize((m_terrainHeight - 1) * (m_terrainWidth - 1));

	// 메쉬의 모든면을 살펴보고 법선을 계산합니다.
	for (int j = 0; j < (m_terrainHeight - 1); j++)
	{
		for (int i = 0; i < (m_terrainWidth - 1); i++)
		{
			index1 = (j * m_terrainHeight) + i;
			index2 = (j * m_terrainHeight) + (i + 1);
			index3 = ((j + 1) * m_terrainHeight) + i;

			// 표면에서 세 개의 꼭지점을 가져옵니다.
			vertex1[0] = m_heightMap[index1].x;
			vertex1[1] = m_heightMap[index1].y;
			vertex1[2] = m_heightMap[index1].z;

			vertex2[0] = m_heightMap[index2].x;
			vertex2[1] = m_heightMap[index2].y;
			vertex2[2] = m_heightMap[index2].z;

			vertex3[0] = m_heightMap[index3].x;
			vertex3[1] = m_heightMap[index3].y;
			vertex3[2] = m_heightMap[index3].z;

			// 표면의 두 벡터를 계산합니다.
			vector1[0] = vertex1[0] - vertex3[0];
			vector1[1] = vertex1[1] - vertex3[1];
			vector1[2] = vertex1[2] - vertex3[2];
			vector2[0] = vertex3[0] - vertex2[0];
			vector2[1] = vertex3[1] - vertex2[1];
			vector2[2] = vertex3[2] - vertex2[2];

			index = (j * (m_terrainHeight - 1)) + i;

			// 이 두 법선에 대한 정규화되지 않은 값을 얻기 위해 두 벡터의 외적을 계산합니다.
			normals[index].x = (vector1[1] * vector2[2]) - (vector1[2] * vector2[1]);
			normals[index].y = (vector1[2] * vector2[0]) - (vector1[0] * vector2[2]);
			normals[index].z = (vector1[0] * vector2[1]) - (vector1[1] * vector2[0]);
		}
	}

	// 이제 모든 정점을 살펴보고 각면의 평균을 취합니다. 	
	// 정점이 닿아 그 정점에 대한 평균 평균값을 얻는다.
	for (int j = 0; j < m_terrainHeight; j++)
	{
		for (int i = 0; i < m_terrainWidth; i++)
		{
			// 합계를 초기화합니다.
			sum[0] = 0.0f;
			sum[1] = 0.0f;
			sum[2] = 0.0f;

			// 카운트를 초기화합니다.
			count = 0;

			// 왼쪽 아래면.
			if (((i - 1) >= 0) && ((j - 1) >= 0))
			{
				index = ((j - 1) * (m_terrainHeight - 1)) + (i - 1);

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
				count++;
			}

			// 오른쪽 아래 면.
			if ((i < (m_terrainWidth - 1)) && ((j - 1) >= 0))
			{
				index = ((j - 1) * (m_terrainHeight - 1)) + i;

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
				count++;
			}

			// 왼쪽 위 면.
			if (((i - 1) >= 0) && (j < (m_terrainHeight - 1)))
			{
				index = (j * (m_terrainHeight - 1)) + (i - 1);

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
				count++;
			}

			// 오른쪽 위 면.
			if ((i < (m_terrainWidth - 1)) && (j < (m_terrainHeight - 1)))
			{
				index = (j * (m_terrainHeight - 1)) + i;

				sum[0] += normals[index].x;
				sum[1] += normals[index].y;
				sum[2] += normals[index].z;
				count++;
			}

			// 이 정점에 닿는면의 평균을 취합니다.
			sum[0] = (sum[0] / (float)count);
			sum[1] = (sum[1] / (float)count);
			sum[2] = (sum[2] / (float)count);

			// 이 법선의 길이를 계산합니다.
			length = sqrt((sum[0] * sum[0]) + (sum[1] * sum[1]) + (sum[2] * sum[2]));

			// 높이 맵 배열의 정점 위치에 대한 인덱스를 가져옵니다.
			index = (j * m_terrainHeight) + i;

			// 이 정점의 최종 공유 법선을 표준화하여 높이 맵 배열에 저장합니다.
			m_heightMap[index].nx =   (sum[0] / length);
			m_heightMap[index].ny =   (sum[1] / length);
			m_heightMap[index].nz =   (sum[2] / length);   
		}
	}
	return true;
}

void cTerrain::CalculateTextureCoordinates()
{
	// 텍스처 좌표를 얼마나 많이 증가 시킬지 계산합니다.
	float incrementValue = (float)TEXTURE_REPEAT / (float)m_terrainWidth;

	// 텍스처를 반복 할 횟수를 계산합니다.
	int incrementCount = m_terrainWidth / TEXTURE_REPEAT;

	// tu 및 tv 좌표 값을 초기화합니다.
	float tuCoordinate = 0.0f;
	float tvCoordinate = 1.0f;

	//  tu 및 tv 좌표 인덱스를 초기화합니다.
	int tuCount = 0;
	int tvCount = 0;

	// 전체 높이 맵을 반복하고 각 꼭지점의 tu 및 tv 텍스처 좌표를 계산합니다.
	for (int j = 0; j < m_terrainHeight; j++)
	{
		for (int i = 0; i < m_terrainWidth; i++)
		{
			// 높이 맵에 텍스처 좌표를 저장한다.
			m_heightMap[(m_terrainHeight * j) + i].tu = tuCoordinate;
			m_heightMap[(m_terrainHeight * j) + i].tv = tvCoordinate;

			// tu 텍스처 좌표를 증가 값만큼 증가시키고 인덱스를 1 씩 증가시킨다.
			tuCoordinate += incrementValue;
			tuCount++;

			// 텍스처의 오른쪽 끝에 있는지 확인하고, 그렇다면 처음부터 다시 시작하십시오.
			if (tuCount == incrementCount)
			{
				tuCoordinate = 0.0f;
				tuCount = 0;
			}
		}

		// tv 텍스처 좌표를 증가 값만큼 증가시키고 인덱스를 1 씩 증가시킵니다.
		tvCoordinate -= incrementValue;
		tvCount++;

		// 텍스처의 상단에 있는지 확인하고, 그렇다면 하단에서 다시 시작합니다.
		if (tvCount == incrementCount)
		{
			tvCoordinate = 1.0f;
			tvCount = 0;
		}
	}
}

inline float cTerrain::Lerp(float a, float b, float t)
{
	return a - (a * t) + (b * t);
}

void cTerrain::_Initialize()
{
	// 지형의 높이 맵을 로드합니다.
	LoadHeightMap("../Resources/heightmap01.bmp");

	// 높이 맵의 높이를 표준화합니다.
	NormalizeHeightMap();

	// 지형 데이터의 법선을 계산합니다.
	if (!CalculateNormals())
	{ }

	// 텍스처 좌표를 계산합니다.
	CalculateTextureCoordinates();

	// 지형 메쉬의 정점 수를 계산합니다.
	m_vertexCount = (m_terrainWidth - 1) * (m_terrainHeight - 1) * 6;

	// 인덱스 수를 꼭지점 수와 같게 설정합니다.
	m_indexCount = m_vertexCount;

	// 정점 배열을 만듭니다.
	std::vector<ST__Vertex_PTN> vertices;
	vertices.resize(m_vertexCount);

	// 인덱스 배열을 만듭니다.
	std::vector<UINT> indices;
	indices.resize(m_indexCount);

	// 정점 배열에 대한 인덱스를 초기화합니다.
	int index = 0;

	float tu = 0.0f;
	float tv = 0.0f;

	// 지형 데이터로 정점 및 인덱스 배열을 로드합니다.
	for (int j = 0; j < (m_terrainHeight - 1); j++)
	{
		for (int i = 0; i < (m_terrainWidth - 1); i++)
		{
			int index1 = (m_terrainHeight * j) + i;          // 왼쪽 아래.
			int index2 = (m_terrainHeight * j) + (i + 1);      // 오른쪽 아래.
			int index3 = (m_terrainHeight * (j + 1)) + i;      // 왼쪽 위.
			int index4 = (m_terrainHeight * (j + 1)) + (i + 1);  // 오른쪽 위.

			// 왼쪽 위.
			tv = m_heightMap[index3].tv;

			// 상단 가장자리를 덮도록 텍스처 좌표를 수정합니다.
			if (tv == 1.0f) { tv = 0.0f; }

			vertices[index].m_position = D3DXVECTOR3(m_heightMap[index3].x, m_heightMap[index3].y, m_heightMap[index3].z);
			vertices[index].m_texcoord = D3DXVECTOR2(m_heightMap[index3].tu, tv);
			vertices[index].m_normalize = D3DXVECTOR3(m_heightMap[index3].nx, m_heightMap[index3].ny, m_heightMap[index3].nz);
			indices[index] = index;
			index++;

			// 오른쪽 위.
			tu = m_heightMap[index4].tu;
			tv = m_heightMap[index4].tv;

			// 위쪽과 오른쪽 가장자리를 덮도록 텍스처 좌표를 수정합니다.
			if (tu == 0.0f) { tu = 1.0f; }
			if (tv == 1.0f) { tv = 0.0f; }

			vertices[index].m_position = D3DXVECTOR3(m_heightMap[index4].x, m_heightMap[index4].y, m_heightMap[index4].z);
			vertices[index].m_texcoord = D3DXVECTOR2(tu, tv);
			vertices[index].m_normalize = D3DXVECTOR3(m_heightMap[index4].nx, m_heightMap[index4].ny, m_heightMap[index4].nz);
			indices[index] = index;
			index++;

			// 왼쪽 아래.
			vertices[index].m_position = D3DXVECTOR3(m_heightMap[index1].x, m_heightMap[index1].y, m_heightMap[index1].z);
			vertices[index].m_texcoord = D3DXVECTOR2(m_heightMap[index1].tu, m_heightMap[index1].tv);
			vertices[index].m_normalize = D3DXVECTOR3(m_heightMap[index1].nx, m_heightMap[index1].ny, m_heightMap[index1].nz);
			indices[index] = index;
			index++;

			// 왼쪽 아래.
			vertices[index].m_position = D3DXVECTOR3(m_heightMap[index1].x, m_heightMap[index1].y, m_heightMap[index1].z);
			vertices[index].m_texcoord = D3DXVECTOR2(m_heightMap[index1].tu, m_heightMap[index1].tv);
			vertices[index].m_normalize = D3DXVECTOR3(m_heightMap[index1].nx, m_heightMap[index1].ny, m_heightMap[index1].nz);
			indices[index] = index;
			index++;

			// 오른쪽 위.
			tu = m_heightMap[index4].tu;
			tv = m_heightMap[index4].tv;

			// 위쪽과 오른쪽 가장자리를 덮도록 텍스처 좌표를 수정합니다.
			if (tu == 0.0f) { tu = 1.0f; }
			if (tv == 1.0f) { tv = 0.0f; }

			vertices[index].m_position = D3DXVECTOR3(m_heightMap[index4].x, m_heightMap[index4].y, m_heightMap[index4].z);
			vertices[index].m_texcoord = D3DXVECTOR2(tu, tv);
			vertices[index].m_normalize = D3DXVECTOR3(m_heightMap[index4].nx, m_heightMap[index4].ny, m_heightMap[index4].nz);
			indices[index] = index;
			index++;

			// 오른쪽 아래.
			tu = m_heightMap[index2].tu;

			// 오른쪽 가장자리를 덮도록 텍스처 좌표를 수정합니다.
			if (tu == 0.0f) { tu = 1.0f; }

			vertices[index].m_position = D3DXVECTOR3(m_heightMap[index2].x, m_heightMap[index2].y, m_heightMap[index2].z);
			vertices[index].m_texcoord = D3DXVECTOR2(tu, m_heightMap[index2].tv);
			vertices[index].m_normalize = D3DXVECTOR3(m_heightMap[index2].nx, m_heightMap[index2].ny, m_heightMap[index2].nz);
			indices[index] = index;
			index++;
		}
	}

	m_mesh->_InitializeVertex<ST__Vertex_PTN>(vertices);
	m_mesh->_InitializeIndices(indices);
}

void cTerrain::_Update(D3DXMATRIX *v, D3DXMATRIX *p)
{
	D3DXMATRIX mat;
	D3DXMatrixIdentity(&mat);

	m_LightShader->_Update(mat, *v, *p);
}

void cTerrain::_Render()
{
	m_mesh->_Render();
	const UINT count = m_mesh->m_indices;
	m_LightShader->_Render(count);
}

const float cTerrain::_GetHight(float x, float y, float z)
{
	D3DXVECTOR3 ray = D3DXVECTOR3(x, 500, z);
	float height = 0;

	// 지형 데이터로 정점 및 인덱스 배열을 로드합니다.
	UINT uX = (UINT)x;
	UINT uZ = (UINT)z;

	// 왼쪽 아래.
	int index1 = (m_terrainHeight * uZ) + uX;
	// 오른쪽 아래.
	int index2 = (m_terrainHeight * uZ) + (uX + 1);
	// 왼쪽 위.
	int index3 = (m_terrainHeight * (uZ + 1)) + uX;
	// 오른쪽 위.
	int index4 = (m_terrainHeight * (uZ + 1)) + (uX + 1);

	//왼쪽 위
	D3DXVECTOR3 A;
	A.x = m_heightMap[index3].x;
	A.y = m_heightMap[index3].y;
	A.z = m_heightMap[index3].z;

	//오른쪽 위
	D3DXVECTOR3 B;
	B.x = m_heightMap[index4].x;
	B.y = m_heightMap[index4].y;
	B.z = m_heightMap[index4].z;

	//왼쪽 아래
	D3DXVECTOR3 C;
	C.x = m_heightMap[index1].x;
	C.y = m_heightMap[index1].y;
	C.z = m_heightMap[index1].z;

	//오른쪽 아래
	D3DXVECTOR3 D;
	D.x = m_heightMap[index2].x;
	D.y = m_heightMap[index2].y;
	D.z = m_heightMap[index2].z;

	float dx = x - uX;
	float dz = z - uZ;

	if (dz < 1.0f - dx)
	{
		float uy = B.y - A.y;
		float vy = C.y - A.y;

		height = A.y + 
			Lerp(0, uy, dx) + Lerp(0, vy, dz);
		return height;
	}
	else
	{
		float uy = C.y - D.y;
		float vy = B.y - D.y;

		height = D.y + 
			Lerp(0, uy, 1.0f - dx) + Lerp(0, vy, 1.0f - dz);

		return height;
	}
	return y;
}

/*
// 지형 데이터로 정점 및 인덱스 배열을 로드합니다.
	for (int j = 0; j < (m_terrainHeight - 1); j++)
	{
		for (int i = 0; i < (m_terrainWidth - 1); i++)
		{
			// 왼쪽 아래.
			int index1 = (m_terrainHeight * j) + i;
			// 오른쪽 아래.
			int index2 = (m_terrainHeight * j) + (i + 1);
			// 왼쪽 위.
			int index3 = (m_terrainHeight * (j + 1)) + i;
			// 오른쪽 위.
			int index4 = (m_terrainHeight * (j + 1)) + (i + 1);

			//왼쪽 위
			D3DXVECTOR3 A;
			A.x = m_heightMap[index3].x;
			A.y = m_heightMap[index3].y;
			A.z = m_heightMap[index3].z;

			//오른쪽 위
			D3DXVECTOR3 B;
			B.x = m_heightMap[index4].x;
			B.y = m_heightMap[index4].y;
			B.z = m_heightMap[index4].z;

			//왼쪽 아래
			D3DXVECTOR3 C;
			C.x = m_heightMap[index1].x;
			C.y = m_heightMap[index1].y;
			C.z = m_heightMap[index1].z;

			//오른쪽 아래
			D3DXVECTOR3 D;
			D.x = m_heightMap[index2].x;
			D.y = m_heightMap[index2].y;
			D.z = m_heightMap[index2].z;

			//원점으로 이동
			float dx = x - i;
			float dz = z - j;

			float dis = 1000;

			//위쪽 삼각형 A B C
			if (dz < 1.0f - dx)
			{
				if (D3DXIntersectTri(&A, &B, &C,
					&ray, &D3DXVECTOR3(0, -1, 0), 0, 0, &dis))
				{
					float uy = B.y - A.y;
					float vy = C.y - A.y;

					height = A.y +
						Lerp(0, uy, dx) +
						Lerp(0, vy, dz);
					return height;
				}
			}
			//아래쪽 삼각형 D C B
			else
			{
				if (D3DXIntersectTri(&C, &B, &D,
					&ray, &D3DXVECTOR3(0, -1, 0), 0, 0, &dis))
				{
					float uy = C.y - D.y;
					float vy = B.y - D.y;

					height = D.y +
						Lerp(0, uy, 1.0f - dx) +
						Lerp(0, vy, 1.0f - dz);

					return height;
				}
			}
		}
	}



*/
