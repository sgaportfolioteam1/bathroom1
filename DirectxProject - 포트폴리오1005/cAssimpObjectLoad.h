#pragma once
class cMesh;
class cObjectShader;
class cTransform;

class cAssimpObjectLoad
{
public:
	cAssimpObjectLoad();
	~cAssimpObjectLoad();
private:
	std::vector<cMesh*> m_vMesh;
	cObjectShader* m_ObjectShader = nullptr;
public:
	cTransform* m_Transform = nullptr;
	void _Initialize(const std::string & fileName);
	void _Update(D3DXMATRIX v,D3DXMATRIX p);
	void _Render();
	void _TextureSet(const std::string &fileName);
};

