#include "stdafx.h"
#include "cGrid.h"

cGrid::cGrid()
{
}

cGrid::~cGrid()
{
	SAFE_RELEASE(m_IndicesBuffer);
	SAFE_RELEASE(m_VertexBuffer);
}

HRESULT cGrid::_Initialize
(const int sizeX, const int sizeY)
{
	if (sizeX != sizeY)
	{
		assert(false && "Grid Size!!!");
	}
	//정점을 구한다
	vertexSize = (sizeX + 1) * (sizeY + 1) + 2;
	ST__Vertex_PC * pVertexs = new ST__Vertex_PC[vertexSize];;
	D3DXVECTOR3 xx = D3DXVECTOR3(-sizeX / (float)2, 0, sizeY / (float)2);

	int count = 0;

	int r = 1;
	int g = 1;
	int b = 1;

	for (int i = 0; i < vertexSize - 1; ++i)
	{
		r = 1;
		g = 1;
		b = 1;

		if (count >= sizeY + 1)
		{
			xx = D3DXVECTOR3(-sizeX / (float)2, 0, xx.z);
			xx.z -= 1.f;
			count = 0;
		}

		//121개 // 120개 //5번째 //115번째
		//y축은 따로 만들기.
		//z축
		if (i == (sizeY / 2) ||
			i == (vertexSize - 3) - (sizeY / 2))
		{
			int a = 0;
			r = 0;
			g = 0;
			b = 1;
		}
		//x축
		else if (i == (sizeY + 1) * (sizeY / 2) ||
			i == (sizeY + 1) * (sizeY / 2) + sizeY)
		{

			r = 1;
			g = 0;
			b = 0;
		}

		pVertexs[i].m_position = xx;
		pVertexs[i].m_color = D3DXCOLOR(r, g, b, 1);

		xx.x += 1.f;
		count++;
	}

	//y축
	pVertexs[vertexSize - 1].m_position = D3DXVECTOR3(0, -sizeY, 0);
	pVertexs[vertexSize - 1].m_color = D3DXCOLOR(0, 1, 0, 1);

	pVertexs[vertexSize - 2].m_position = D3DXVECTOR3(0, sizeY, 0);
	pVertexs[vertexSize - 2].m_color = D3DXCOLOR(0, 1, 0, 1);

	//Vertex Buffer
	{
		D3D11_BUFFER_DESC desc;
		ZeroMemory(&desc, sizeof(D3D11_BUFFER_DESC));
		desc.Usage = D3D11_USAGE_IMMUTABLE;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;//어느 스테이지에서 쓸것이냐.
		desc.ByteWidth = sizeof(ST__Vertex_PC) * vertexSize;//버택스의 버퍼 크기를 얼만큼 할꺼냐.
		//여까지는 버택스 정보를 어떻게 만들고 어떻게 쓸것인지에 대한 껍데기 정보.

		D3D11_SUBRESOURCE_DATA sub_data;
		//CPU데이터를 GPU에 연결 할 수 있다.
		ZeroMemory(&sub_data, sizeof(D3D11_SUBRESOURCE_DATA));

		//버퍼 연결.
		sub_data.pSysMem = pVertexs;

		auto hr = cGraphic::GetInstance().GetDevice()->CreateBuffer
		(
			&desc,	//버퍼에 대한 설명서.
			&sub_data, //초기화 데이터.
			&m_VertexBuffer //버텍스 버퍼.
		);

		assert(SUCCEEDED(hr));
	}
	//1		3
	//
	//0		2
	IdicesSize = ((sizeX + 1) + (sizeY + 1)) * 2 + 2;

	//4 x 4일시
	UINT* pIdices = new UINT[IdicesSize];

	int index = 0;

	//가로
	for (int i = 0; i <= sizeX; i++)
	{
		pIdices[index] = i * (sizeY + 1);
		pIdices[index + 1] = pIdices[index] + sizeY;
		//2
		index += 2;
	}

	//세로
	for (int i = 0; i <= sizeY; i++)
	{
		pIdices[index] = i;
		pIdices[index + 1] = pIdices[index] + (sizeX * (sizeY + 1));
		index += 2;
	}

	//y축용
	pIdices[IdicesSize - 1] = vertexSize - 1;
	pIdices[IdicesSize - 2] = vertexSize - 2;
	//Index Buffer
	{
		D3D11_BUFFER_DESC desc;
		ZeroMemory(&desc, sizeof(D3D11_BUFFER_DESC));
		desc.Usage = D3D11_USAGE_IMMUTABLE;
		desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		desc.ByteWidth = sizeof(UINT) * IdicesSize;
		//
		D3D11_SUBRESOURCE_DATA sub_data;
		ZeroMemory(&sub_data, sizeof(D3D11_SUBRESOURCE_DATA));
		sub_data.pSysMem = pIdices;
		//
		auto hr = cGraphic::GetInstance().GetDevice()->CreateBuffer
		(
			&desc, &sub_data, &m_IndicesBuffer
		);

		assert(SUCCEEDED(hr));
	}

	SAFE_DELETE(pVertexs);
	SAFE_DELETE(pIdices);

	D3DXMatrixIdentity(&m_world);

	return S_OK;
}

void cGrid::_Render()
{
	const UINT stride = sizeof(ST__Vertex_PC);
	const UINT offset = 0;

	//입력 어셈블러에서(IA) 정점버퍼 넣기
	cGraphic::GetInstance().GetDeviceContext()->
		IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

	cGraphic::GetInstance().GetDeviceContext()->
		IASetIndexBuffer(m_IndicesBuffer, DXGI_FORMAT_R32_UINT, 0);

	//삼각형으로 그리겠다.
	cGraphic::GetInstance().GetDeviceContext()->
		IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
}
