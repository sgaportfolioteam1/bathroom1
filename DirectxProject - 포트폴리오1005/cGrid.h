#pragma once
class cGrid
{
public:
	cGrid();
	~cGrid();
public:
	HRESULT _Initialize(
		const int sizeX, const int sizeY);
	//void _Update(D3DXMATRIX v, D3DXMATRIX p);
	void _Render();

	int vertexSize = 0;
	int IdicesSize = 0;
private:
	
	//버텍스 정보 넣을 버퍼
	ID3D11Buffer* m_VertexBuffer = nullptr;
	//인덱스 정보 넣을 버퍼
	ID3D11Buffer* m_IndicesBuffer = nullptr;

	D3DXMATRIX m_world;
};
