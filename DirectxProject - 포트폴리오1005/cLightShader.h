#pragma once
class cLightShader
{
public:
	cLightShader();
	~cLightShader();
private:
	//버텍스 쉐이더, 
	ID3D11VertexShader* m_VSShader = nullptr;
	//화면상 쉐이더,
	ID3D11PixelShader* m_PSShader = nullptr;
	//Byte 맞추기
	ID3D11InputLayout* m_InputLayout = nullptr;
	//월드 버퍼
	ID3D11Buffer* m_WVPBuffer = nullptr;
	//빛의 정보를 넣어줄 상수 버퍼
	ID3D11Buffer* m_DiffuseBuffer = nullptr;
public:
	//텍스쳐 
	cTexture* m_Texture = nullptr;

	void _Initialize();
	void _Update(D3DXMATRIX m, D3DXMATRIX v, D3DXMATRIX p);
	void _Render(UINT renderCount);
};

