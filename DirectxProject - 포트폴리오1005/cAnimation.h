#pragma once
class cAnimation
{
public:
	cAnimation();
	~cAnimation();

	Assimp::Importer importer;
	const aiScene *scene = nullptr;

	const aiAnimation * m_Animation = nullptr;
	//�� ����
	struct ST__Acter* m_BoneActor = nullptr;

	UINT FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);
	UINT FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
	UINT FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);

	void CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);

	const aiNodeAnim* FindNodeAnim(const aiAnimation* pAnimation, const std::string NodeName);

	void ReadNodeHeirarchy(float AnimationTime, const aiNode* pNode, const aiMatrix4x4& ParentTransform);

	void Play(const aiNode* pNode, aiMatrix4x4 mat, struct std::vector<ST__BindPose>& mats);

	void _Initialize(const std::string & file, struct ST__Acter* acter);
};

