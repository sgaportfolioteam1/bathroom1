#include "stdafx.h"
#include "cGameObject.h"

#include "cMesh.h"
#include "cMaterial.h"
#include "cTransform.h"

cGameObject::cGameObject()
{
	m_Transform = new cTransform();
}


cGameObject::~cGameObject()
{
	SAFE_RELEASE(m_Mesh);
	SAFE_RELEASE(m_Material);

	SAFE_DELETE(m_Transform);
	//게임오브젝트 파괴 시킬것.

	std::vector<cComponent*>::iterator iter;

	for (iter = msg.begin(); iter != msg.end(); )
	{
		cComponent* ref = (*iter);
		iter = msg.erase(iter);
		SAFE_DELETE(ref);
	}
}

void cGameObject::_Initialize()
{
	//Empty....?
}

void cGameObject::_Update(D3DXMATRIX * m, D3DXMATRIX v, D3DXMATRIX p)
{
	if (m_Material != nullptr)
		m_Material->_Update(m, v, p);

	//월드값 계산해서 자식에게 넘겨주기

	for (UINT i = 0; i < m_child.size(); ++i)
	{
		m_child[i]->_Update(m_Transform->_GetWorld(), v, p);
	}
}

void cGameObject::_Render()
{
	if (m_Mesh != nullptr && m_Material != nullptr)
	{
		m_Mesh->_Render();
		m_Material->_Render(m_Mesh->m_indices);
	}

	for (UINT i = 0; i < m_child.size(); ++i)
	{
		m_child[i]->_Render();
	}
}

void cGameObject::_ComponentUpdate()
{
	for (UINT i = 0; i < msg.size(); ++i)
	{
		msg[i]->_Update();
	}
}
