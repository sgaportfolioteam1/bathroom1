struct vertexInput
{
	float4 position : POSITION0;
	float2 uv : TEXCOORD0;
	float3 normal : NORMALIZE0;
	uint4 boneids : BONEIDS0;
	float4 weights : WEIGHTS0;
};

struct PixelInput
{
	float4 position : SV_POSITION0;
	float2 uv : TEXCOORD0;
	float3 normal : NORMALIZE0;

};

cbuffer TransformBuffer : register(b0)
{
	matrix world;
	matrix view;
	matrix proj;
};

cbuffer BoneBuffer : register(b1)
{
	matrix bones[100];
};

PixelInput VS(vertexInput Input)
{
	PixelInput pixelout;

	//Dont NULL DATA 
	matrix BoneTransform = bones[Input.boneids[0]] * Input.weights[0];
	BoneTransform += bones[Input.boneids[1]] * Input.weights[1];
	BoneTransform += bones[Input.boneids[2]] * Input.weights[2];
	BoneTransform += bones[Input.boneids[3]] * Input.weights[3];

	Input.position.w = 1.f;

	pixelout.position = mul(Input.position, BoneTransform);

	pixelout.position = mul(pixelout.position, world);
	pixelout.position = mul(pixelout.position, view);
	pixelout.position = mul(pixelout.position, proj);

	pixelout.uv = Input.uv;
	// 월드 행렬에 대해서만 법선 벡터를 계산합니다.
	pixelout.normal = mul(Input.normal, (float3x3) world);
	// 법선 벡터를 정규화합니다.
	pixelout.normal = normalize(pixelout.normal);


	return pixelout;
}

Texture2D source_texture0 : register(t0);
SamplerState samp : register(s0);

float4 PS(PixelInput input) : SV_Target
{
	return float4(0, 0, 0, 1);
//float4 color = source_texture0.Sample(samp, input.uv);
//return color;
}