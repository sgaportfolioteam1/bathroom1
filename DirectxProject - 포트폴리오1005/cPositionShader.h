#pragma once
class cPositionShader
{
public:
	cPositionShader();
	~cPositionShader();
private:
	//버텍스 쉐이더, 
	ID3D11VertexShader* m_VSShader = nullptr;
	//화면상 쉐이더,
	ID3D11PixelShader* m_PSShader = nullptr;
	//Byte 맞추기
	ID3D11InputLayout* m_InputLayout = nullptr;
	//월드 버퍼
	ID3D11Buffer* m_WVPBuffer = nullptr;

	ID3D11RasterizerState * m_RSState = nullptr;
public:
	void _Initialize();
	void _Update(D3DXMATRIX m, D3DXMATRIX v, D3DXMATRIX p);
	void _Render(UINT renderCount);
};

