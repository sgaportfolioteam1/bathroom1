#pragma once
class cCamera
{
public:
	cCamera();
	~cCamera();

private:
	//Up,
	//Eye
	//LookAt
	D3DXVECTOR3 m_Up;
	D3DXVECTOR3 m_Eye;
	D3DXVECTOR3 m_LookAt;

	//카메라 반전용?
	float			m_fCamRotX = 0;
	float			m_fCamRotY = 0;
	//이전 마우스 위치와, 다음 마우스 위치 방향을 찾기 위함.
	POINT m_stPrevMousePosition;

	bool m_Block = false;
	bool m_Sit = false;

public:
	D3DXMATRIX m_viewMatrix;
	D3DXMATRIX m_projectionMatrix;
	D3DXVECTOR3 GetEye() { return m_Eye; }
public:
	void _Initialize();
	void _Update();
};

