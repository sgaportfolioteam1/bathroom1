#include "stdafx.h"
#include "cCamera.h"


cCamera::cCamera()
{
}


cCamera::~cCamera()
{
}

void cCamera::_Initialize()
{
	//세계 y축
	m_Up = D3DXVECTOR3(0, 1, 0);
	//내가 바라볼 위치
	m_LookAt = D3DXVECTOR3(0, 260, 1);
	//눈 위치
	m_Eye = D3DXVECTOR3(0, 200, -340);

	//뷰 행렬 만들어 주기.
	D3DXMatrixLookAtLH(&m_viewMatrix, &m_Eye, &m_LookAt, &m_Up);

	// 투영 행렬을 설정합니다
	float width = Setting::GetInstance().GetWindowWidth();
	float hight = Setting::GetInstance().GetWindowHeight();

	float fieldOfView = 3.141592654f / 4.0f;

	float screenAspect = width / hight;
	D3DXMatrixPerspectiveFovLH(&m_projectionMatrix,
		fieldOfView, screenAspect, 0.3f, 1000);
}

/*

V = 
	Right				Up					LookAt
{	Xx					Yx					Zx		0
	Xy					Yy					Zy		0
	Xz					Yz					Zz		0
	-Dot(Eye, X)	-Dot(Eye, Y)	-Dot(Eye, Z)	1

zaxis = normal(At - Eye)
xaxis = normal(cross(Up, zaxis))
yaxis = cross(zaxis, xaxis)

 xaxis.x           yaxis.x           zaxis.x          0
 xaxis.y           yaxis.y           zaxis.y          0
 xaxis.z           yaxis.z           zaxis.z          0

*/

void cCamera::_Update()
{
	//커서 가리기용
	//ShowCursor(false);
	//top = 400;
	//bottom = 0;
	//left = -200.f;
	//right = 200.f;
	//front = 400.f;
	//back = -400.f;

	if (MANAGERINPUT->isKeyDown(DIK_W))
	{
		m_Block = false;
		//앞문쪽 벽(출구)
		if (m_Eye.z >= 350)
		{
			m_Block = true;
			m_Eye.z = 349;			
		}
		//뒷문쪽 벽(시작점)
		if (m_Eye.z <= -350)
		{
			m_Block = true;
			m_Eye.z = -349;
		}
		if (m_LookAt.x > 0)
		{
			//오른쪽 기둥
			if (m_Eye.z >= -90 && m_Eye.z <= 40)
			{
				if (m_Eye.x >= 50)
				{
					m_Block = true;
					m_Eye.x = 49;
				}
			}
			//오른쪽 벽(상자쪽)
			else
			{
				if (m_Eye.x >= 70)
				{
					m_Block = true;
					m_Eye.x = 69;
				}
			}
		}
		if (m_LookAt.x < 0)
		{
			//왼쪽 기둥
			if (m_Eye.z >= -100 && m_Eye.z <= 0)
			{
				if (m_Eye.x <= -70)
				{
					m_Block = true;
					m_Eye.x = -69;
				}
			}
			//왼쪽 벽(세면대쪽)
			else
			{
				if (m_Eye.x <= -50)
				{
					m_Block = true;
					m_Eye.x = -49;
				}
			}
		}
		
		//방향
		D3DXVECTOR3 Direction;
		//방향 다시 구하기
		D3DXVec3Normalize(&Direction, &(m_LookAt - m_Eye));
		m_Eye.x += Direction.x * 3.5;
		m_Eye.z += Direction.z * 3.5;
		if (!m_Block)
		{
			m_LookAt += Direction * 3.5;
		}
	}
	if (MANAGERINPUT->isKeyDown(DIK_S))
	{
		m_Block = false;
		//앞문쪽 벽(출구)
		if (m_Eye.z >= 350)
		{
			m_Block = true;
			m_Eye.z = 349;
		}
		//뒷문쪽 벽(시작점)
		if (m_Eye.z <= -350)
		{
			m_Block = true;
			m_Eye.z = -349;
		}
		if (m_LookAt.x < 0)
		{
			//오른쪽 벽(상자쪽)
			if (m_Eye.z >= -90 && m_Eye.z <= 40)
			{
				if (m_Eye.x >= 50)
				{
					m_Block = true;
					m_Eye.x = 49;
				}
			}
			else
			{
				if (m_Eye.x >= 70)
				{
					m_Block = true;
					m_Eye.x = 69;
				}
			}
		}
		if (m_LookAt.x > 0)
		{
			//왼쪽 벽(세면대쪽)
			if (m_Eye.z >= -100 && m_Eye.z <= 0)
			{
				if (m_Eye.x <= -70)
				{
					m_Block = true;
					m_Eye.x = -69;
				}
			}
			else
			{
				if (m_Eye.x <= -50)
				{
					m_Block = true;
					m_Eye.x = -49;
				}
			}
		}		
		
		//방향
		D3DXVECTOR3 Direction;
		//방향 다시 구하기
		D3DXVec3Normalize(&Direction, &(m_LookAt - m_Eye));
		m_Eye.x -= Direction.x * 3.5;
		m_Eye.z -= Direction.z * 3.5;
		if (!m_Block)
		{
			m_LookAt -= Direction * 3.5;		
		}
	}

	//왼쪽 이동
	if (MANAGERINPUT->isKeyDown(DIK_A))
	{
		m_Block = false;
		//앞문쪽 벽(출구)
		if (m_Eye.z >= 350)
		{
			m_Block = true;
			m_Eye.z = 349;
		}
		//뒷문쪽 벽(시작점)
		if (m_Eye.z <= -350)
		{
			m_Block = true;
			m_Eye.z = -349;			
		}

		//왼쪽 기둥
		if (m_Eye.z >= -100 && m_Eye.z <= 0)
		{
			if (m_Eye.x <= -70)
			{
				m_Block = true;
				m_Eye.x = -69;
			}
		}
		//왼쪽 벽(세면대쪽)
		else
		{
			if (m_Eye.x <= -50)
			{
				m_Block = true;
				m_Eye.x = -49;
			}
		}

		//오른쪽 기둥
		if (m_Eye.z >= -90 && m_Eye.z <= 40)
		{
			if (m_Eye.x >= 50)
			{
				m_Block = true;
				m_Eye.x = 49;
			}
		}
		//오른쪽 벽(상자쪽)
		else
		{
			if (m_Eye.x >= 70)
			{
				m_Block = true;
				m_Eye.x = 69;
			}
		}

		//y축, 어디방향, 내가 갈 방향
		D3DXVECTOR3 UpNormal, ForwardNormal, Direction;
		//두개 방향 정규화
		D3DXVec3Normalize(&UpNormal, &m_Up);
		D3DXVec3Normalize(&ForwardNormal, &(m_LookAt - m_Eye));
		//갈 방향 구하기
		D3DXVec3Cross(&Direction, &ForwardNormal, &UpNormal);
		D3DXVec3Normalize(&Direction, &Direction);
		m_Eye += Direction * 3;
		if (!m_Block)
		{
			m_LookAt += Direction * 3;
		}
	}
	//오른쪽 이동
	if (MANAGERINPUT->isKeyDown(DIK_D))
	{
		m_Block = false;
		//앞문쪽 벽(출구)
		if (m_Eye.z >= 350)
		{
			m_Block = true;
			m_Eye.z = 349;
		}
		//뒷문쪽 벽(시작점)
		if (m_Eye.z <= -350)
		{
			m_Block = true;
			m_Eye.z = -349;
		}

		//왼쪽 기둥
		if (m_Eye.z >= -100 && m_Eye.z <= 0)
		{
			if (m_Eye.x <= -70)
			{
				m_Block = true;
				m_Eye.x = -69;
			}
		}
		//왼쪽 벽(세면대쪽)
		else
		{
			if (m_Eye.x <= -50)
			{
				m_Block = true;
				m_Eye.x = -49;
			}
		}

		//오른쪽 기둥
		if (m_Eye.z >= -90 && m_Eye.z <= 40)
		{
			if (m_Eye.x >= 50)
			{
				m_Block = true;
				m_Eye.x = 49;
			}
		}
		//오른쪽 벽(상자쪽)
		else
		{
			if (m_Eye.x >= 70)
			{
				m_Block = true;
				m_Eye.x = 69;
			}
		}

		//y축, 어디방향, 내가 갈 방향
		D3DXVECTOR3 UpNormal, ForwardNormal, Direction;
		//두개 방향 정규화
		D3DXVec3Normalize(&UpNormal, &m_Up);
		D3DXVec3Normalize(&ForwardNormal, &(m_LookAt - m_Eye));
		//갈 방향 구하기
		D3DXVec3Cross(&Direction, &ForwardNormal, &UpNormal);
		D3DXVec3Normalize(&Direction, &Direction);
		m_Eye -= Direction * 3.5;
		if (!m_Block)
		{
			m_LookAt -= Direction * 3.5;
		}		
	}

	if (MANAGERINPUT->isKeyPressed(DIK_LCONTROL))
	{
		if (m_Sit)
		{
			m_Sit = false;
		}
		else if (!m_Sit)
		{
			m_Sit = true;
		}				
	}
	if (m_Sit)
	{
		if (m_Eye.y > 70)
		{
			m_Eye.y--;
			m_LookAt.y--;
		}
	}
	if (!m_Sit)
	{
		if (m_Eye.y < 201)
		{
			m_Eye.y++;
			m_LookAt.y++;
		}
	}

	{

		//현재 마우스 위치 가져오기
		POINT ptCurrMouse = MANAGERINPUT->getMousePosition();	

		

		//이동 방향 구하기
		float fDeltaX = ptCurrMouse.x - m_stPrevMousePosition.x;
		float fDeltaY = ptCurrMouse.y - m_stPrevMousePosition.y;

		if (fabs(fDeltaX) > fabs(fDeltaY))
		{
			fDeltaY = (ptCurrMouse.y - m_stPrevMousePosition.y) / 5;
		}
		else if (fabs(fDeltaX) < fabs(fDeltaY))
		{
			fDeltaX = (ptCurrMouse.x - m_stPrevMousePosition.x) / 5;
		}
		
		

		m_fCamRotY += (fDeltaX / 35.f);//100
		m_fCamRotX += (fDeltaY / 35.f);//100

		if (m_fCamRotX < -D3DX_PI / 2.0f + 0.0001f)
			m_fCamRotX = -D3DX_PI / 2.0f + 0.0001f;
		if (m_fCamRotX > D3DX_PI / 2.0f - 0.0001f)
			m_fCamRotX = D3DX_PI / 2.0f - 0.0001f;

		if (fDeltaY > 0) // mouse up look
		{
			D3DXVECTOR3 UpNormal;
			D3DXVec3Normalize(&UpNormal, &m_Up);
			m_LookAt -= UpNormal * 30.0f;
		}
		else if (fDeltaY < 0) // mouse down look
		{
			D3DXVECTOR3 UpNormal;
			D3DXVec3Normalize(&UpNormal, &m_Up);
			m_LookAt += UpNormal * 30.0f;
		}

		if (fDeltaX < 0) // mouse right look
		{
			D3DXVECTOR3 UpNormal, ForwardNormal, Right;
			D3DXVec3Normalize(&UpNormal, &m_Up);
			D3DXVec3Normalize(&ForwardNormal, &(m_LookAt - m_Eye));
			D3DXVec3Cross(&Right, &ForwardNormal, &UpNormal);
			D3DXVec3Normalize(&Right, &Right);
			m_LookAt += Right * 30.0f;
		}
		else if (fDeltaX > 0) // mouse left look
		{
			D3DXVECTOR3 UpNormal, ForwardNormal, Right;
			D3DXVec3Normalize(&UpNormal, &m_Up);
			D3DXVec3Normalize(&ForwardNormal, &(m_LookAt - m_Eye));
			D3DXVec3Cross(&Right, &ForwardNormal, &UpNormal);
			D3DXVec3Normalize(&Right, &Right);
			m_LookAt -= Right * 30.0f;
		}	

		m_stPrevMousePosition = ptCurrMouse;

		if (MANAGERINPUT->getMousePosition().x <= 0 || MANAGERINPUT->getMousePosition().x >= Setting::GetInstance().GetWindowWidth())
		{
			SetCursorPos(Setting::GetInstance().GetWindowWidth() / 2 + Winpos.x, MANAGERINPUT->getMousePosition().y + Winpos.y +30);
		}
		if (MANAGERINPUT->getMousePosition().y <= 0 || MANAGERINPUT->getMousePosition().y >= Setting::GetInstance().GetWindowHeight())
		{
			SetCursorPos(MANAGERINPUT->getMousePosition().x + Winpos.x + 7, Setting::GetInstance().GetWindowHeight() / 2 + Winpos.y);
			
		}

		
		ShowCursor(false);

	}
	D3DXMatrixLookAtLH(&m_viewMatrix, &m_Eye, &m_LookAt, &m_Up);
}
