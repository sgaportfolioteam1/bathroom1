#include "stdafx.h"
#include "cText.h"

#include "cMesh.h"
#include "cFont.h"
#include "cFontShader.h"
#include "cTexture.h"

cText::cText()
{
}


cText::~cText()
{
}

void cText::_Initialize()
{
	m_Mesh = new cMesh();

	//한 글자에 정점은 6개로 진행중이다.
	//최대 100글자 정도는 들어갈 수 있는 버퍼 생성
	std::vector<ST__Vertex_PT> vertex;
	vertex.resize(6 * 100);

	//메모리 초기화
	std::memset(vertex.data(), 0, (sizeof(ST__Vertex_PT) * (6 * 100)));

	std::vector<UINT> indices;
	indices.resize(6 * 100);
	for (int i = 0; i < 6 * 100; ++i)
		indices[i] = i;

	m_Mesh->_InitializeVertexDYNAMIC<ST__Vertex_PT>(vertex);
	m_Mesh->_InitializeIndices(indices);

	m_screenWidth = Setting::GetInstance().GetWindowWidth();
	m_screenHeight = Setting::GetInstance().GetWindowHeight();
}

void cText::_Update(float x, float y, D3DXMATRIX* v, D3DXMATRIX *p)
{

	float left, top;

	// 비트 맵 왼쪽의 화면 좌표를 계산합니다.
	left = (float)((m_screenWidth / 2) * -1) +
		(float)x;// +((float)m_Width / 2);
	// 비트 맵 상단의 화면 좌표를 계산합니다.
	top = (float)(m_screenHeight / 2) -
		(float)y;// -((float)m_Height / 2);
	//m_Mesh->_Update();
	D3DXMATRIX mat;
	D3DXMatrixIdentity(&mat);

	//크기 조절
	mat._11 = 5;
	mat._22 = 5;
	//위치 조정
	mat._41 = left;
	mat._42 = top;
	m_FontShader->_Update(mat, *v, *p);
}

void cText::_Render()
{
	m_Mesh->_Render();

	m_Font->m_Texture->_Render();

	const UINT indexCount = m_Mesh->m_indices;
	m_FontShader->_Render(indexCount);
}

void cText::_SetText(const std::string & text)
{
	//글자 길이
	int numLetters;

	ST__Vertex_PT* vertices;
	
	//어디에 그릴 것인가.
	float drawX, drawY;

	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ST__Vertex_PT* verticesPtr;

	// Store the color of the sentence.
	//sentence->red = red;
	//sentence->green = green;
	//sentence->blue = blue;

	// Get the number of letters in the sentence.
	// 글자 길이 가져오기.
	// 반복문을 돌리기 위해 . 글자 하나씩 처리.
	numLetters = (int)strlen(text.c_str());

	// Check for possible buffer overflow. //오버플로우 발생시 예외처리
	// 는 나중에

	// Create the vertex array. //6 * length
	//vertices = new ST__Vertex_PT[m_Text->m_indexCount];
	vertices = new ST__Vertex_PT[6 * numLetters];
	if (!vertices)
	{
		assert(false);
	}
	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(ST__Vertex_PT) * (6 * numLetters)));

	// Calculate the X and Y pixel position on the screen to start drawing to.
	drawX = (float)(((m_screenWidth / 2) * -1) + 0);
	drawY = (float)((m_screenHeight / 2) - 0);

	// Use the font class to build the vertex array from the sentence text and sentence draw location.
	m_Mesh->m_indices = m_Font->BuildVertexArray(
		(void*)vertices, text.data(), drawX, drawY);

	// Lock the vertex buffer so it can be written to.
	result = cGraphic::GetInstance().GetDeviceContext()->Map(
		m_Mesh->m_VertexBuffer, 
		0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	
	if (FAILED(result))
	{
		assert(false);
	}

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (ST__Vertex_PT*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)vertices, 
		(sizeof(ST__Vertex_PT) * m_Mesh->m_indices));

	// Unlock the vertex buffer.
	cGraphic::GetInstance().GetDeviceContext()->Unmap(
		m_Mesh->m_VertexBuffer, 0);

	// Release the vertex array as it is no longer needed.
	delete[] vertices;
	vertices = 0;
}
