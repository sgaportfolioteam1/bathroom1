#pragma once

#include "cTransform.h"
//카메라 클래스
class cCamera;
class cRoom;
class cAssimpObjectLoad;
class cTransform;
class cCanvas;

enum OBJECTNUM
{
	EMPTY,
	KEY,
	CASE,
	PHOTOFRAME1,
	PHOTOFRAME2,
	PHOTOFRAME3,
	PHOTOFRAME4,
	PAPER1,
	PAPER2,
	PAPER3,
	DOOR,
	TOILET
};

class cExcute
{
public:
	cExcute();
	~cExcute();

public:
	void _Initialize();
	void _Update();
	void _Render();

	bool RaySphereIntersectionTest(D3DXVECTOR3* origin, D3DXVECTOR3* direction, D3DXMATRIX* target, float objectRange, int objectNum);
	void MousePicking(cAssimpObjectLoad* object, cTransform mat);

private:
	POINT m_stPrevMousePosition;	

	bool m_GetKey = false;
	bool m_CaseOpen = false;

	int m_objectNum = EMPTY;

private:
	cCamera* m_Camera = nullptr;
	
	//UI
	cCanvas* m_Canvas = nullptr;

	//↓오브젝트↓
	cRoom* m_room = nullptr;

	cAssimpObjectLoad* m_bath = nullptr;

	cAssimpObjectLoad* m_door = nullptr;
	cAssimpObjectLoad* m_door2 = nullptr;
	cAssimpObjectLoad* m_door3 = nullptr;
	cTransform m_doorMat;

	cAssimpObjectLoad* m_table = nullptr;

	cAssimpObjectLoad* m_toilet = nullptr;

	cAssimpObjectLoad* m_radiateur = nullptr;

	cAssimpObjectLoad* m_shelf = nullptr;
	cAssimpObjectLoad* m_shelf2 = nullptr;

	cAssimpObjectLoad* m_washStand0 = nullptr;
	cAssimpObjectLoad* m_washStand1 = nullptr;
	cAssimpObjectLoad* m_washStand2 = nullptr;
	cAssimpObjectLoad* m_washStand3 = nullptr;

	cAssimpObjectLoad* m_case = nullptr;
	cTransform m_caseMat;

	cAssimpObjectLoad* m_flowerpot = nullptr;
	cAssimpObjectLoad* m_flowerpot2 = nullptr;

	cAssimpObjectLoad* 	m_photoframe = nullptr;
	cAssimpObjectLoad* 	m_photoframe2 = nullptr;
	cAssimpObjectLoad* 	m_photoframe3 = nullptr;
	cAssimpObjectLoad* 	m_photoframe4 = nullptr;
	cTransform m_photoframeMat1;
	cTransform m_photoframeMat2;
	cTransform m_photoframeMat3;
	cTransform m_photoframeMat4;

	cAssimpObjectLoad* m_paper1 = nullptr;
	cAssimpObjectLoad* m_paper2 = nullptr;
	cAssimpObjectLoad* m_paper3 = nullptr;
	cTransform m_paperMat1;
	cTransform m_paperMat2;
	cTransform m_paperMat3;

	cAssimpObjectLoad* m_mirror = nullptr;

	cAssimpObjectLoad* m_key = nullptr;
	cTransform m_KeyMat;

};