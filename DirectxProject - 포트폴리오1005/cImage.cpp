#include "stdafx.h"
#include "cImage.h"

#include "cMesh.h"
#include "cTexture.h"
#include "cTextureShader.h"

cImage::cImage()
{
}


cImage::~cImage()
{
	SAFE_RELEASE(m_Texture);
	SAFE_RELEASE(m_mesh);
//	SAFE_DELETE(m_TextureShader);
}

void cImage::_Initialize(string file, string name, float width, float height)
{
	m_Width = width;
	m_Height = height;

	m_Rect = RectMakeCenter(m_x, m_y, m_Width, m_Height);

	m_mesh = new cMesh();

	std::vector<ST__Vertex_PT> vertices;
	vertices.resize(4);
	//0 2 
	//1 3
	std::vector<UINT> indices;
	indices.resize(6);
	indices = { 0, 2, 3, 0, 3, 1 };

	m_mesh = new cMesh();
	m_mesh->_InitializeVertexDYNAMIC<ST__Vertex_PT>(vertices);
	m_mesh->_InitializeIndices(indices);

	m_TextureShader = new cTextureShader();
	m_TextureShader->_Initialize();

	m_Texture = new cTexture();
	m_Texture->_Initialize(file);
	m_Texture->m_name = name;

	MANAGERRESOURCE->m_Textures->_AddResource(m_Texture);
	m_TextureShader->m_Texture = MANAGERRESOURCE->m_Textures->_FindResource(name);
	//MANAGERRESOURCE->_GetTexture("../Resources/box.jpg");

	m_screenWidth = Setting::GetInstance().GetWindowWidth();
	m_screenHeight = Setting::GetInstance().GetWindowHeight();

	m_x = m_screenWidth / 2;
	m_y = m_screenHeight / 2;
}

void cImage::_Update(float x, float y, D3DXMATRIX *v, D3DXMATRIX *p)
{
	//if (m_x == x && m_y == y)
		//return;

	m_x = x - m_Width / 2;
	m_y = y - m_Height / 2;
	
	std::vector<ST__Vertex_PT> vertices;
	vertices.resize(4);

	//1 2
	//0 3
	RECT rect;
	// 비트 맵 왼쪽의 화면 좌표를 계산합니다.
	rect.left = (float)((m_screenWidth / 2) * -1) + (float)m_x;// +((float)m_Width / 2);
	// 비트 맵 상단의 화면 좌표를 계산합니다.
	rect.top = (float)(m_screenHeight / 2) - (float)m_y;// -((float)m_Height / 2);

	// 비트 맵 오른쪽의 화면 좌표를 계산합니다.
	rect.right = rect.left + (float)m_Width;
	// 비트 맵 아래쪽의 화면 좌표를 계산합니다.
	rect.bottom = rect.top - (float)m_Height;

	m_Rect.left = m_x;
	m_Rect.top = m_y;
	m_Rect.right = m_x + m_Width;
	m_Rect.bottom = m_y + m_Height;

	//0 2 
	//1 3
	{
		vertices[0].m_position = D3DXVECTOR3(rect.left, rect.top, 0);
		vertices[0].m_texcoord = D3DXVECTOR2(0, 0);

		vertices[1].m_position = D3DXVECTOR3(rect.left, rect.bottom, 0);
		vertices[1].m_texcoord = D3DXVECTOR2(0, 1);

		vertices[2].m_position = D3DXVECTOR3(rect.right, rect.top, 0);
		vertices[2].m_texcoord = D3DXVECTOR2(1, 0);

		vertices[3].m_position = D3DXVECTOR3(rect.right, rect.bottom, 0);
		vertices[3].m_texcoord = D3DXVECTOR2(1, 1);
	}

	m_mesh->_Update(vertices);

	D3DXMATRIX mat;
	D3DXMatrixIdentity(&mat);

	m_TextureShader->_Update(mat, *v, *p);
}

void cImage::_Render()
{
	m_mesh->_Render();
	m_TextureShader->_Render(6);
	m_Texture->_Render();
}

const RECT cImage::_IGetRect()
{
	return m_Rect;
}

RECT cImage::RectMakeCenter(int x, int y, int width, int height)
{
	RECT rc = { x - width / 2, y - height / 2, x + width / 2, y + height / 2 };
	return rc;
}

void cImage::SetMeshTextureUVUpdate(float x, float y, D3DXMATRIX* v, D3DXMATRIX* p, D3DXVECTOR4 setUV)
{	
	//if (m_x == x && m_y == y)
		//return;

	m_x = x - m_Width / 2;
	m_y = y - m_Height / 2;

	std::vector<ST__Vertex_PT> vertices;
	vertices.resize(4);

	//1 2
	//0 3
	RECT rect;
	// 비트 맵 왼쪽의 화면 좌표를 계산합니다.
	rect.left = (float)((m_screenWidth / 2) * -1) + (float)m_x;// +((float)m_Width / 2);
	// 비트 맵 상단의 화면 좌표를 계산합니다.
	rect.top = (float)(m_screenHeight / 2) - (float)m_y;// -((float)m_Height / 2);

	// 비트 맵 오른쪽의 화면 좌표를 계산합니다.
	rect.right = rect.left + (float)m_Width;
	// 비트 맵 아래쪽의 화면 좌표를 계산합니다.
	rect.bottom = rect.top - (float)m_Height;

	m_Rect.left = m_x;
	m_Rect.top = m_y;
	m_Rect.right = m_x + m_Width;
	m_Rect.bottom = m_y + m_Height;

	//0 2 
	//1 3
	{
		vertices[0].m_position = D3DXVECTOR3(rect.left, rect.top, 0);
		vertices[0].m_texcoord = D3DXVECTOR2(setUV.x, setUV.y);

		vertices[1].m_position = D3DXVECTOR3(rect.left, rect.bottom, 0);
		vertices[1].m_texcoord = D3DXVECTOR2(setUV.x, setUV.w);

		vertices[2].m_position = D3DXVECTOR3(rect.right, rect.top, 0);
		vertices[2].m_texcoord = D3DXVECTOR2(setUV.z, setUV.y);

		vertices[3].m_position = D3DXVECTOR3(rect.right, rect.bottom, 0);
		vertices[3].m_texcoord = D3DXVECTOR2(setUV.z, setUV.w);
	}

	m_mesh->_Update(vertices);

	D3DXMATRIX mat;
	D3DXMatrixIdentity(&mat);

	m_TextureShader->_Update(mat, *v, *p);
}
