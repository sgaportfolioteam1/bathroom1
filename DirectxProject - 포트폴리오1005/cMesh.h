#pragma once

#include "cRefObject.h"

/*
정점 정보를 저장시킨다. 
즉 IA 정보
*/

class cMesh : public cRefObject
{
public:
	cMesh();
	virtual ~cMesh();
public:
	//정점의 정보를 넣을 버퍼
	ID3D11Buffer* m_VertexBuffer = nullptr;
	//정점의 인덱스를 넣을 버퍼.
	ID3D11Buffer* m_IndicesBuffer = nullptr;
public:

	std::string m_name = "";

	UINT m_stride = 0;
	UINT m_indices = 0;

	template<typename T>
	void _InitializeVertex(std::vector<T> &vertex)
	{
		m_stride = sizeof(T);

		D3D11_BUFFER_DESC BufferDesc;
		//정적인가, 동적인가, 선택
		BufferDesc.Usage = D3D11_USAGE_DEFAULT;
		//사이즈 정하기
		BufferDesc.ByteWidth = sizeof(T) * vertex.size();
		//무슨 버퍼인지 정하기.
		BufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		BufferDesc.CPUAccessFlags = 0;
		BufferDesc.MiscFlags = 0;
		BufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA Date;
		Date.pSysMem = vertex.data();
		Date.SysMemPitch = 0;
		Date.SysMemSlicePitch = 0;

		//해당 하는 버퍼 만들어주기
		if (FAILED(cGraphic::GetInstance().GetDevice()->CreateBuffer(
			&BufferDesc, &Date, &m_VertexBuffer)))
			assert("VertexBuffer 에러");
	}

	template<typename T>
	void _InitializeVertexDYNAMIC(std::vector<T> &vertex)
	{
		m_stride = sizeof(T);

		D3D11_BUFFER_DESC BufferDesc;
		//정적인가, 동적인가, 선택
		BufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		//사이즈 정하기
		BufferDesc.ByteWidth = sizeof(T) * vertex.size();
		//무슨 버퍼인지 정하기.
		BufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		BufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		BufferDesc.MiscFlags = 0;
		BufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA Date;
		Date.pSysMem = vertex.data();
		Date.SysMemPitch = 0;
		Date.SysMemSlicePitch = 0;

		//해당 하는 버퍼 만들어주기
		if (FAILED(cGraphic::GetInstance().GetDevice()->CreateBuffer(
			&BufferDesc, &Date, &m_VertexBuffer)))
			assert("VertexBuffer 에러");
	}

	void _InitializeIndices(std::vector<UINT> &indices);

	template<typename T>
	void _Update(std::vector<T> &vertex)
	{
		//상수 버퍼 내용을 쓸 수 있도록 잠금
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		if (FAILED(cGraphic::GetInstance().GetDeviceContext()->Map(
			m_VertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource)))
		{
			return;
		};

		//상수 버퍼의 데이터에 대한 포인터를 가져옴.
		T* dataPtr = (T*)mappedResource.pData;

		memcpy(dataPtr, vertex.data(),(sizeof(T) * vertex.size()));

		//상수 버퍼의 잠금을 푼다.
		cGraphic::GetInstance().GetDeviceContext()->Unmap(
			m_VertexBuffer, 0
		);
	}
	void _Render();
};
