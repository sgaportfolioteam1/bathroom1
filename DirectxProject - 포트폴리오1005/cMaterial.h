#pragma once

#include "cRefObject.h";

/*
머터리얼,
쉐이더를 담아서 출력시키는 통?개념으로 접근중이다.

*/

class cLightShader;
class cLightViewShader;

class cMaterial : public cRefObject
{
public:
	cMaterial();
	virtual ~cMaterial();

public:
	std::string m_name;
	
	D3DXCOLOR m_AmbientColor;
	D3DXCOLOR m_DiffuseColor;
	D3DXCOLOR m_SpecularColor;

	//cLightShader* m_Shader = nullptr;
	cLightViewShader* m_Shader = nullptr;

public:
	void _Update(D3DXMATRIX *m, D3DXMATRIX v, D3DXMATRIX p);
	void _Render(UINT indexCount);
};

