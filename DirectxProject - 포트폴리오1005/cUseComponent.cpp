#include "stdafx.h"
#include "cUseComponent.h"
#include "cTransform.h"

cUseComponent::cUseComponent()
{
}


cUseComponent::~cUseComponent()
{
}

void cUseComponent::_Update()
{
	//스케일 함수로 크기 줄일것.
	if (MANAGERINPUT->isKeyDown(DIK_UP))
	{
		m_Target->_Translation(0, 0, 0.1f);
	}
	else if (MANAGERINPUT->isKeyDown(DIK_DOWN))
	{
		m_Target->_Translation(0, 0, -0.1f);
	}
	else if (MANAGERINPUT->isKeyDown(DIK_LEFT))
	{
		m_Target->_Translation(-0.1f, 0, 0);
	}
	else if (MANAGERINPUT->isKeyDown(DIK_RIGHT))
	{
		m_Target->_Translation(0.1f, 0, 0);
	}
}

const D3DXVECTOR3 cUseComponent::_GetPosition() const
{
	return D3DXVECTOR3(
		m_Target->_GetWorld()->_41,
		m_Target->_GetWorld()->_42,
		m_Target->_GetWorld()->_43);
}
