#pragma once

class cImage;
class cText;

class cUIPointDown;

class cCanvas
{
public:
	cCanvas();
	~cCanvas();

private:
	//카메라 용.
	D3DXMATRIX m_UIViewMatrix;
	D3DXMATRIX m_UIOrthogonalProjectionMatrix;

	cImage* m_xImage = nullptr;
	cImage* m_handImage = nullptr;
	cImage* m_Crosshair = nullptr;
	cImage* m_GameClear = nullptr;
	cImage* m_Cursor = nullptr;
	cText* m_TextA = nullptr;	//E = Get
	cText* m_TextB = nullptr;	//E = Open

	cImage* m_password[5];
	bool m_keyInput[4] = { false };
	int m_answer[4] = {0};

	std::list<class cIPointDown*> m_EventList;

	float m_ScreenWidth;
	float m_ScreenHeight;

public:
	bool m_RayOn = false;
	bool m_KeyCheck = false;	//열쇠인지 확인용
	bool m_CaseCheck = false;	//상자인지 확인용

public:
	void _Initialize();
	void _Update();
	void _Render();
	//void _UpdateToRender();
	void _PassWord();
	void _Wordpos(int i);
};

