#pragma once

#include "cComponent.h"

/*
컴포넌트 처럼 사용하도록 상속 받고.
*/
class cUseComponent : public cComponent
{
public:
	cUseComponent();
	virtual ~cUseComponent();

public:
	//이 컴포넌트는 타겟을 받아 키 입력으로 움직이게
	//할 것이다.
	class cTransform* m_Target = nullptr;
	virtual void _Update() override;

	const D3DXVECTOR3 _GetPosition() const;
};

