#include "stdafx.h"
#include "cTransform.h"

cTransform::cTransform() : 
	m_localPosition(0,0,0),
	m_localRotation(0,0,0),
	m_scale(1,1,1)
{
	D3DXMatrixIdentity(&m_matWorld);
}


cTransform::~cTransform()
{
}

void cTransform::_Translation(float x, float y, float z)
{
	//m_localPosition += D3DXVECTOR3(x, y, z);
	m_localPosition.x += x;
	m_localPosition.y += y;
	m_localPosition.z += z;

	_UpdateSRT();
}

void cTransform::_Translation(D3DXVECTOR3 position)
{
	m_localPosition += position;

	_UpdateSRT();
}

void cTransform::OverwriteTranslation(float x, float y, float z)
{
	m_localPosition.x = x;
	m_localPosition.y = y;
	m_localPosition.z = z;

	_UpdateSRT();
}

void cTransform::_Rotate(float x, float y, float z)
{
	m_localRotation.x += x;
	m_localRotation.y += y;
	m_localRotation.z += z;

	if (m_localRotation.x >= 360)
		m_localRotation.x = 0;

	if (m_localRotation.y >= 360)
		m_localRotation.y = 0;

	if (m_localRotation.z >= 360)
		m_localRotation.z = 0;

	_UpdateSRT();
}

void cTransform::OverwriteRotate(float x, float y, float z)
{
	m_localRotation.x = x;
	m_localRotation.y = y;
	m_localRotation.z = z;

	while (m_localRotation.x >= 360)
		m_localRotation.x = x - 360;

	while (m_localRotation.y >= 360)
		m_localRotation.y = y - 360;

	while (m_localRotation.z >= 360)
		m_localRotation.z = y - 360;

	while (m_localRotation.x < 0)
		m_localRotation.x = x + 360;

	while (m_localRotation.y < 0)
		m_localRotation.y = y + 360;

	while (m_localRotation.z < 0)
		m_localRotation.z = y + 360;



	_UpdateSRT();
}

void cTransform::_Scaling(float x, float y, float z)
{
	m_scale.x = x;
	m_scale.y = y;
	m_scale.z = z;

	_UpdateSRT();
}

void cTransform::_Scaling(D3DXVECTOR3 scale)
{
	m_scale = scale;

	_UpdateSRT();
}

inline void cTransform::_UpdateSRT()
{
	D3DXMATRIX S, R, T;
	D3DXMatrixScaling(&S, m_scale.x, m_scale.y, m_scale.z);
	D3DXMatrixRotationYawPitchRoll(&R, 
		D3DXToRadian(m_localRotation.y), 
		D3DXToRadian(m_localRotation.x), 
		D3DXToRadian(m_localRotation.z));
	D3DXMatrixTranslation
	(&T,
		m_localPosition.x,
		m_localPosition.y,
		m_localPosition.z);
	m_matWorld = S * R * T;
}

D3DXMATRIX cTransform::_UpdateWorld()
{
	D3DXMATRIX mat;
	if (m_Parent != nullptr)
	{
		D3DXMATRIX S, R, T;
		D3DXMatrixScaling(&S, m_scale.x, m_scale.y, m_scale.z);
		D3DXMatrixRotationYawPitchRoll(&R,
			D3DXToRadian(m_localRotation.y),
			D3DXToRadian(m_localRotation.x),
			D3DXToRadian(m_localRotation.z));
		D3DXMatrixTranslation
		(&T,
			m_localPosition.x,
			m_localPosition.y,
			m_localPosition.z);

		mat = m_matWorld = (S * R * T) * *m_Parent->_GetWorld();
		//mat = m_matWorld * *m_Parent->_GetWorld();
	}
	else
	{
		mat = m_matWorld;
	}

	return mat;
}

 D3DXMATRIX * cTransform::_GetWorld()
{
	return &m_matWorld;
}

 void cTransform::_SetWorld(D3DXMATRIX mat)
 {
	 m_matWorld = mat;
 }