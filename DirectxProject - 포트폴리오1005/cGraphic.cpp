#include "stdafx.h"
#include "cGraphic.h"

cGraphic::cGraphic()
{

}

cGraphic::~cGraphic()
{
	SAFE_RELEASE(m_depthStencilBuffer);
	SAFE_RELEASE(m_depthStencilState);
	SAFE_RELEASE(m_depthStencilView);

	SAFE_RELEASE(m_alphaEnableBlendingState);
	SAFE_RELEASE(m_alphaDisableBlendingState);

	SAFE_RELEASE(pRender_Target_View);
	SAFE_RELEASE(pDevice);
	SAFE_RELEASE(pDevice_Context);
	SAFE_RELEASE(pSwap_Chain);
	/*
	가장 먼저 생성한놈은 가장 맨 마지막에 지워주어라.
	그게 더 안전하다.
	그렇지 않을 시 Dangling Pointer현상이 일어날 수 있다.
	포인터가 해제된 메모리 영역을 가리키고 있는 경우 쓰고 있던
	친구가 내가 쓰던게 사라지니 문제가 생긴다. 이러한 문제때문에
	마지막에 지워주는게 안전하다.
	*/
}

void cGraphic::Initialize()
{
	//컴 인터페이스는 만들기전에 설명서가 필요하다.
	DXGI_SWAP_CHAIN_DESC desc;
	ZeroMemory(&desc, sizeof(DXGI_SWAP_CHAIN_DESC));
	//백버퍼의 크기를 .
	desc.BufferDesc.Width = 0;
	desc.BufferDesc.Height = 0;
	//화면 주사율 / 화면을 다시 새롭게 하는 비율, 60으로~
	desc.BufferDesc.RefreshRate.Numerator = 60; // 분자
	desc.BufferDesc.RefreshRate.Denominator = 1; // 분모
	//데이터 공간의 주머니, 어떤 형식의 데이터를 담는가.
	desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	/*
	DXGI_FORMAT_R8G8B8A8_UNORM =>
	색깔의 채널을 의미, 비트 컬러라고 부르며 하나의 채녈당 얼마만큼의 메모리를 줄것이냐.
	RGBA 8비트 32비트를 할당하는 1바이트씩 가지는 4바이트의 색깔.
	8바이트 = 255 / UNORM => U(언사인트) NORM(노멀라이즈 약자)
	*/
	//한 라인을 그리는 순서를 어떻게 할것이냐.
	//모르겠다. 아직 안쓰겠다라고 셋팅한다.
	desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	//화면이 확대되거나 축소될떄 동일하게 이것도 모르겠다라고 알려준다.
	desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	//버퍼를 어떻게 사용할 것인가. 물어보는 방법이다.
	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	//버퍼 카운트는 1로
	desc.BufferCount = 1;
	desc.SampleDesc.Count = 1;

	//계단 현상 Aliasing 현상 제거, 안티에이셜링.
	//SSAA MSAA 여러가지 방식이 있지만 주로 쓰는것들.
	desc.SampleDesc.Quality = 0;
	//핸들 넣어주고.
	desc.OutputWindow = Setting::GetInstance().GetWindowHandle();
	//Window::g_handle;
	//창모드로 할 것이다.
	desc.Windowed = TRUE;
	//교환할때 어떻게 할 것인가? 폐기하라.
	desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	std::vector<D3D_FEATURE_LEVEL> feature
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	}; //기본 버젼은 어떤걸 쓸것인가, 먼저 들어오는게 높은 버전이다.

	auto hr = D3D11CreateDeviceAndSwapChain
	(
		//어뎁터 안만들어주는데 수직 동기화 영역
		nullptr,
		//드라이브 타입, 하드웨어 지원되는 기능만 가속사켜 사용하겠다.
		D3D_DRIVER_TYPE_HARDWARE,
		//다른 소프트웨어를 넣을거냐 안넣을거냐.
		nullptr,
		//어떤 기능을 쓰는가
		0,
		//배열의 시작점. &feature[0]
		feature.data(),
		feature.size(),
		//기본적으로 값이 셋팅되어있따.
		D3D11_SDK_VERSION,
		&desc,
		&pSwap_Chain,
		&pDevice,
		nullptr,
		//ppImmediate Context => 즉시 문맥.
		&pDevice_Context
	);
	assert(SUCCEEDED(hr) && "ABD");

}

void cGraphic::CreateBackBuffer(const UINT & width, const UINT & height)
{
	auto hr = pSwap_Chain->ResizeBuffers
	(
		0,
		width,
		height,
		//모르겠다, 이미 그대로 있는거 쓰겠다.
		DXGI_FORMAT_UNKNOWN,
		0
	);

	assert(SUCCEEDED(hr));

	//2)
	ID3D11Texture2D * back_buffer = nullptr;

	hr = pSwap_Chain->GetBuffer
	(
		0,
		__uuidof(ID3D11Texture2D),
		/*
		uu id of 미리 만들어진 키워드이고,
		(Universally Unique Identifier) / GUID(globally Unique Identifier)
		두개의 자료형은 다르지만 의미는 같다, 텍스쳐 자료형으로 가보자.
		16바이트 데이터를 가지고, 16진수로 표현됨
		고유 식별자, 중복이 거의 없다,
		두개를 구분하는 이유, MS에서는 GUID / 전용적으로 UUID
		크게 상관없다.
		*/
		//3번쨰 인수, Surface 표면을 얘기한다.
		//얻어올 버퍼의 정보주소, 버퍼 형식에 맞지 않다.
		//형변환을 해주고 보이드 형변환은 이렇게, 보이드 포인터로 바뀌고
		//메모리가 할당된다.
		reinterpret_cast<void**>(&back_buffer)
	);

	assert(SUCCEEDED(hr));
	/*
	우리가 사용하는 자원들 GPU는 그래픽 카드 자원들은
	다이렉트 엑스에서 제공하는 자원들은 ID3D11Resource를 상속받고 있다.
	리소스라는 자료형을 가지고 있으며 상속을 받고있다.
	모든 자원들은 리소스를 상속 받는데 크게 2가지로 나눈다.
	ID3D11Buffer			ID3D11Texture1D, 2D, 3D가 있다.
	버퍼형 자원과				텍스쳐형으로 나눈다.
	버퍼는 구조체로 만드는것,	이미지 형태로 만드는것.

	버퍼같은 경우는 자료형이 하나이고, 데이터 자체를 묶어서 내보내는데.
	Texture친구들은 여러가지 광범위하게 사용된다.
	어떤 데이터를 그릴 수 있고 이미 만들어진 그림을 가지고 올 수 있다.
	헌데 사용용도가 명확하지 않다. 어떤걸 그린다는 것인가, 가져온다는 것인가.

	ReSource View를 통해서 텍스쳐 자원의 사용용도를 명확하게 해줄수 있다.
	RenderTargetView가 이중에 있다,
	다른것들은 ID3D11ShaderResourceView, ID3D11DepthStencilView,
	ID3D11UnorderedAccessView 순서대로 화면용, 자원을 넘기기 위한 (png, jpg)
	깊이 관련된 정보, 접근뷰 읽고 쓰기.

	백버퍼는 리소스 자원이고 이것만 가지고 GPU에다 데이터를 못보낸다.
	그래서 리소스 뷰 만들어준다.
	*/
	//1) 도화지 역할 , 바로 만들수 없다, 따로 만들수 있는 것을 만들어줘야된다.

	hr = pDevice->CreateRenderTargetView
	(
		back_buffer,
		//DESC 기본적으로 스왑체인이 데이터에 있는거 가져온거라 nullptr해줌
		nullptr,
		//타켓뷰 할당.
		&pRender_Target_View
	);

	assert(SUCCEEDED(hr));



	// 깊이 버퍼 구조체를 초기화합니다
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

	// 깊이 버퍼 구조체를 작성합니다
	depthBufferDesc.Width = width;
	depthBufferDesc.Height = height;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	// 설정된 깊이버퍼 구조체를 사용하여 깊이 버퍼 텍스쳐를 생성합니다
	hr = pDevice->CreateTexture2D(&depthBufferDesc, NULL, &m_depthStencilBuffer);

	assert(SUCCEEDED(hr));
	///////////////////////////////////////////////////////////////////////////////
	// 스텐실 상태 구조체를 초기화합니다
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	// 스텐실 상태 구조체를 작성합니다
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;

	// 픽셀 정면의 스텐실 설정입니다
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// 픽셀 뒷면의 스텐실 설정입니다
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	hr = pDevice->CreateDepthStencilState(&depthStencilDesc, &m_depthStencilState);
	assert(SUCCEEDED(hr));
	// 깊이 스텐실 상태를 설정합니다
	pDevice_Context->OMSetDepthStencilState(m_depthStencilState, 1);
	///////////////////////////////////////////////////////////////////////
	// 깊이 스텐실 뷰의 구조체를 초기화합니다
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

	// 깊이 스텐실 뷰 구조체를 설정합니다
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	// 깊이 스텐실 뷰를 생성합니다
	hr = pDevice->CreateDepthStencilView(m_depthStencilBuffer, &depthStencilViewDesc, &m_depthStencilView);
	assert(SUCCEEDED(hr));

	// 렌더링 대상 뷰와 깊이 스텐실 버퍼를 출력 렌더 파이프 라인에 바인딩합니다
	pDevice_Context->OMSetRenderTargets(1, &pRender_Target_View, m_depthStencilView);
	//////////////////////////

	// 그려지는 폴리곤과 방법을 결정할 래스터 구조체를 설정합니다
	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	// 방금 작성한 구조체에서 래스터 라이저 상태를 만듭니다
	hr = pDevice->CreateRasterizerState(&rasterDesc, &m_rasterState);
	assert(SUCCEEDED(hr));

	// 이제 래스터 라이저 상태를 설정합니다
	pDevice_Context->RSSetState(m_rasterState);

	//시작점.
	m_View_Port.TopLeftX = 0.0f;
	m_View_Port.TopLeftY = 0.0f;
	m_View_Port.Width = static_cast<float>(width);
	m_View_Port.Height = static_cast<float>(height);
	m_View_Port.MinDepth = 0.0f;
	//깊이는 1정도 들어가야 된다.
	m_View_Port.MaxDepth = 1.0f;

	// 뷰포트를 생성합니다
	pDevice_Context->RSSetViewports(1, &m_View_Port);

	// 이제 2D 렌더링을위한 Z 버퍼를 끄는 두 번째 깊이 스텐실 상태를 만듭니다. 유일한 차이점은
	// DepthEnable을 false로 설정하면 다른 모든 매개 변수는 다른 깊이 스텐실 상태와 동일합니다.
	D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;
	ZeroMemory(&depthDisabledStencilDesc, sizeof(depthDisabledStencilDesc));

	depthDisabledStencilDesc.DepthEnable = false;
	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDisabledStencilDesc.StencilEnable = true;
	depthDisabledStencilDesc.StencilReadMask = 0xFF;
	depthDisabledStencilDesc.StencilWriteMask = 0xFF;
	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// 장치를 사용하여 상태를 만듭니다.
	pDevice->CreateDepthStencilState(&depthDisabledStencilDesc, &m_depthDisabledStencilState);
	////
	{
		D3D11_BLEND_DESC blendStateDescription;
		// Clear the blend state description.
		ZeroMemory(&blendStateDescription, sizeof(D3D11_BLEND_DESC));

		// Create an alpha enabled blend state description.
		blendStateDescription.RenderTarget[0].BlendEnable = TRUE;
		blendStateDescription.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		blendStateDescription.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		blendStateDescription.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendStateDescription.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendStateDescription.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendStateDescription.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendStateDescription.RenderTarget[0].RenderTargetWriteMask = 0x0f;

		// Create the blend state using the description.
		auto result = pDevice->CreateBlendState(&blendStateDescription, &m_alphaEnableBlendingState);
		if (FAILED(result))
		{
		}
		// Modify the description to create an alpha disabled blend state description.
		blendStateDescription.RenderTarget[0].BlendEnable = FALSE;
		// Create the blend state using the description.
		if (FAILED(pDevice->CreateBlendState(&blendStateDescription,
			&m_alphaDisableBlendingState)))
		{
		}
	}
	//만들었으니 삭제시켜주기.
	SAFE_RELEASE(back_buffer);
}

void cGraphic::Begin()
{
	//더블 버퍼링을 활용하는 중이다, 앞면과 후면
	//앞면 = 보여주기 // 후면 = 그리기.
	//
	//O M = OutPut-Merger Stage
	//Device_Context = 직접적인 랜더링 관련된 친구. 얘를 쓸것.
	//도화지 같은 역할을 하는 타겟뷰. 어떤 그림, 어느곳에 랜더를 할것인지
	//알려주는 정보,
	// 백버퍼를 지웁니다

	//pDevice_Context->OMSetRenderTargets
	//(
	//	//몇개의 셋팅 총8장
	//	1, 
	//	&pRender_Target_View,
	//	nullptr	//깊이에 대한 정보들이 필요하다 , 아직은 쓸일없음.
	//);
	////보여줄 영역을 랜더 타겟 바뀔때 뷰포트 또한 바껴야 된다.
	//pDevice_Context->RSSetViewports
	//(
	//	1, 
	//	&m_View_Port
	//);

	pDevice_Context->ClearRenderTargetView(pRender_Target_View, m_Clear_Color);

	// 깊이 버퍼를 지웁니다
	pDevice_Context->ClearDepthStencilView(m_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void cGraphic::End()
{
	//Begin후 지워진 도화지에 다시 그려야 되니 전면으로 보낸다.
	auto hr = pSwap_Chain->Present //후면버퍼를 -> 전면으로
	(
		1, //수직동기화 영역, 다음에 
		0  //플래그 아무것도 안쓸꺼니 0으로
	);

	assert(SUCCEEDED(hr));
}

void cGraphic::TurnZBufferOn()
{
	pDevice_Context->OMSetDepthStencilState(m_depthStencilState, 1);
}


void cGraphic::TurnZBufferOff()
{
	pDevice_Context->OMSetDepthStencilState(m_depthDisabledStencilState, 1);
}

ID3D11DepthStencilView * cGraphic::GetDepthStencilView()
{
	return m_depthStencilView;
}

void cGraphic::SetBackBufferRenderTarget()
{
	// 렌더링 대상 뷰와 깊이 스텐실 버퍼를 출력 렌더 파이프 라인에 바인딩합니다.
	pDevice_Context->OMSetRenderTargets(1, &pRender_Target_View, m_depthStencilView);
	//m_deviceContext->OMSetRenderTargets(1, &m_renderTargetView, m_depthStencilView);
}
void cGraphic::TurnOnAlphaBlending()
{
	float blendFactor[4];


	// Setup the blend factor.
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;

	// Turn on the alpha blending.
	pDevice_Context->OMSetBlendState(m_alphaEnableBlendingState, blendFactor, 0xffffffff);

	return;
}

void cGraphic::TurnOffAlphaBlending()
{
	// Setup the blend factor.
	float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	// Turn off the alpha blending.
	pDevice_Context->OMSetBlendState(m_alphaDisableBlendingState, 
		blendFactor, 0xffffffff);
}
