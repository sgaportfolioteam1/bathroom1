#include "stdafx.h"
#include "WinMain.h" 
#include "cExcute.h"

bool g_Click = false;
bool g_Pass = false;
POINT Winpos = { 100,100 };

int APIENTRY WinMain(
	HINSTANCE hInstacne,
	HINSTANCE previnstacne,
	LPSTR ipszCmdParam,
	int nCmdShow
)
/*
APIENTRY 지정자는 윈도우 표준 호출 규약인 __stdcall을 사용하겠다는 뜻 콜백함수이다.
*/
{
	//윈도우 생성
	Window::Create(hInstacne, 1280, 720);
	//윈도우 화면보이기
	Window::Show();

	//싱글톤 셋팅.
	Setting::GetInstance().SetWindowHandle(Window::g_handle);
	Setting::GetInstance().SetWindowWidth(
		static_cast<float>(Window::GetWidth()));
	Setting::GetInstance().SetWindowHeight(
		static_cast<float>(Window::GetHeight()));

	cGraphic::GetInstance().Initialize();
	cGraphic::GetInstance().CreateBackBuffer
	(
		static_cast<UINT>(Setting::GetInstance().GetWindowWidth()),
		static_cast<UINT>(Setting::GetInstance().GetWindowHeight())
	);

	MANAGERINPUT->init(hInstacne);
	//MANAGERRESOURCE

	cExcute* excute = new cExcute();
	excute->_Initialize();

	////윈도우 매프레임마다 업데이트
	while (Window::Update())
	{
		MANAGERINPUT->update();
		//업데이트
		excute->_Update();

		if (MANAGERINPUT->isKeyPressed(DIK_ESCAPE))
		{
			Window::Destroy();
		}
		//랜더러
		cGraphic::GetInstance().Begin();
		{
			//그리고 싶은 랜더 넣기.
			excute->_Render();
		}
		cGraphic::GetInstance().End();
	}

	SAFE_DELETE(excute);

	MANAGERINPUT->ReleaseSingleton();
	MANAGERRESOURCE->ReleaseSingleton();


	//윈도우 파괴 무한루프 빠져나오면.
	Window::Destroy();
	//정상종료 했다.
	return 0;
}
