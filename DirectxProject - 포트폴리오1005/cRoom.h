#pragma once


class cMesh;
class cLightViewShader;

class cRoom
{
public:
	cRoom();
	~cRoom();
private:
	cLightViewShader* m_LightViewShader = nullptr;
	std::vector<cMesh*> m_Meshes;
public:
	void _Initialize();
	void _Update(D3DXMATRIX v, D3DXMATRIX p);
	void _Render();

public:
	D3DXVECTOR3 RectMake(int bone, int i);
	std::vector<UINT>IndexMake();
	D3DXVECTOR2 TextureUV(int bone, int i);
	D3DXVECTOR2 m_Light;




};

