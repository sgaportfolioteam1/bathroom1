#pragma once

#include "stdafx.h"
#include <functional>

__interface IPointDown
{
	void _IPointDown();
};

class cIPointDown : public IPointDown
{
public:
	cIPointDown() 
	{
		//이벤트 정보를 추가하도록 하던지 한다.
		//Event Syatem.add(this);
	}
	virtual ~cIPointDown() { }

public:
	template<typename T>
	std::function<void()> IPointDown(T* t)
	{
		POINT point = MANAGERINPUT->getMousePosition();

		if (PtInRect(&t->_IGetRect(), point))
		{
			t->_IPointDown();
		}
		return 0;
	}
protected:
	// IPointDown을(를) 통해 상속됨
	virtual void _IPointDown() = 0;
	virtual const RECT _IGetRect() = 0;
};

