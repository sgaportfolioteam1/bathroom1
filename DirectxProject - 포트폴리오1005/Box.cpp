#include "stdafx.h"
#include "cMesh.h"

class Box
{
public:
	Box() { }
	~Box() { }

public:
	cMesh* _Initialize()
	{
		std::vector<D3DXVECTOR3> vertex;
		vertex.resize(24);
		//버택스 셋팅
		{
			//1		3
				//
				//0		2
			D3DXVECTOR3 _0(D3DXVECTOR3(-0.5f, -0.5f, -0.5f));
			D3DXVECTOR3 _1(D3DXVECTOR3(-0.5f, 0.5f, -0.5f));
			D3DXVECTOR3 _2(D3DXVECTOR3(0.5f, -0.5f, -0.5f));
			D3DXVECTOR3 _3(D3DXVECTOR3(0.5f, 0.5f, -0.5f));
			//5		7
			//
			//4		6
			D3DXVECTOR3 _4(D3DXVECTOR3(-0.5f, -0.5f, 0.5f));
			D3DXVECTOR3 _5(D3DXVECTOR3(-0.5f, 0.5f, 0.5f));
			D3DXVECTOR3 _6(D3DXVECTOR3(0.5f, -0.5f, 0.5f));
			D3DXVECTOR3 _7(D3DXVECTOR3(0.5f, 0.5f, 0.5f));

			//후면(뒤)
			vertex[0] = _0;
			vertex[1] = _1;
			vertex[2] = _2;
			vertex[3] = _3;
			//전면
			vertex[4] = _4;
			vertex[5] = _5;
			vertex[6] = _6;
			vertex[7] = _7;
			//왼쪽
			vertex[8] = _0;
			vertex[9] = _1;
			vertex[10] = _4;
			vertex[11] = _5;
			//오른쪽
			vertex[12] = _2;
			vertex[13] = _3;
			vertex[14] = _6;
			vertex[15] = _7;
			//위
			vertex[16] = _1;
			vertex[17] = _5;
			vertex[18] = _3;
			vertex[19] = _7;
			//아래
			vertex[20] = _0;
			vertex[21] = _4;
			vertex[22] = _2;
			vertex[23] = _6;
		}
		std::vector< UINT> index;
		index.resize(36);
		//인덱스 셋팅
		{
			index =
			{
				//후
				0, 1, 3,
				0, 3, 2,
				//전
				4,7,5,
				4,6,7,
				//왼
				8,11,9,
				8,10,11,
				//오
				12,13,15,
				12,15,14,
				//위
				16,17,19,
				16,19,18,
				//아
				20,23,21,
				20,22,23
			};
		}

		cMesh* box = new cMesh();
		box->_InitializeVertex<D3DXVECTOR3>(vertex);
		box->_InitializeIndices(index);

		box->m_name = "Box";

		return box;
	}
};
