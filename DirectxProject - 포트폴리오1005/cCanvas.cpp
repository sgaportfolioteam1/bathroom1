#include "stdafx.h"
#include "cCanvas.h"

#include "cImage.h"
#include "cText.h"

#include "cUIPointDown.h"

cCanvas::cCanvas()
{
}


cCanvas::~cCanvas()
{
	SAFE_DELETE(m_TextA);
	SAFE_DELETE(m_xImage);
	SAFE_DELETE(m_handImage);
}

void cCanvas::_Initialize()
{
	D3DXMatrixLookAtLH(&m_UIViewMatrix, 
		&D3DXVECTOR3(0, 0, 0), 
		&D3DXVECTOR3(0, 0, 1), 
		&D3DXVECTOR3(0, 1, 0));

	D3DXMatrixOrthoLH(&m_UIOrthogonalProjectionMatrix,
		Setting::GetInstance().GetWindowWidth(),
		Setting::GetInstance().GetWindowHeight(),
		0.f, 1.f);

	m_xImage = new cImage();
	m_xImage->_Initialize("../Resources/UI/xUI.png", "xUI", 30, 30);

	m_handImage = new cImage();
	m_handImage->_Initialize("../Resources/UI/handUI.png", "handUI", 80, 80);

	m_Crosshair = new cImage();
	m_Crosshair->_Initialize("../Resources/UI/Crosshair.png", "Crosshair", 256, 256);	

	//커서 텍스처는 추후 수정예정w
	m_Cursor = new cImage();
	m_Cursor->_Initialize("../Resources/UI/cursor.png", "cursor", 40, 40);

	m_TextA = new cText();
	m_TextA->_Initialize();
	m_TextA->m_Font = MANAGERRESOURCE->m_Font;
	m_TextA->m_FontShader = MANAGERRESOURCE->m_FontShader;
	m_TextA->_SetText("E = Get");

	m_TextB = new cText();
	m_TextB->_Initialize();
	m_TextB->m_Font = MANAGERRESOURCE->m_Font;
	m_TextB->m_FontShader = MANAGERRESOURCE->m_FontShader;
	m_TextB->_SetText("E = Open");

	cUIPointDown* PointDown = new cUIPointDown();
	PointDown->m_Target = m_xImage;

	m_EventList.push_back(PointDown);

	m_ScreenWidth = Setting::GetInstance().GetWindowWidth();
	m_ScreenHeight = Setting::GetInstance().GetWindowHeight();

	for (int i = 0; i < 4; i++)
	{
		m_password[i] = new cImage();
		m_password[i]->_Initialize("../Resources/number_sprite.png", "number", 100, 100);
	}
	m_password[4] = new cImage();
	m_password[4]->_Initialize("../Resources/password.png", "passward", 400, 100);

	m_GameClear = new cImage();
	m_GameClear->_Initialize("../Resources/default2.png","gameClear", m_ScreenWidth, m_ScreenHeight);
}

void cCanvas::_Update()
{
	if (g_Click)
	{
		m_xImage->_Update(m_ScreenWidth - 30, 30, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix);
		if (m_KeyCheck)
		{
			m_TextA->_Update(30, m_ScreenHeight - 70, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix);
		}
		if (m_CaseCheck)
		{
			m_TextB->_Update(30, m_ScreenHeight - 70, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix);
		}
	}		

	_PassWord();
	m_Cursor->_Update(MANAGERINPUT->getMousePosition().x,MANAGERINPUT->getMousePosition().y+10, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix);

	if (m_RayOn)
	{
		m_handImage->_Update(m_ScreenWidth / 2, m_ScreenHeight / 2, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix);
	}
	else
	{
		m_Crosshair->_Update(m_ScreenWidth / 2, m_ScreenHeight / 2, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix);
	}

	//Event Mamanger에서 처리가 되야 된다.
	if (MANAGERINPUT->isMouseDown(0))
	{
		std::list<cIPointDown*>::iterator iter = m_EventList.begin();

		for (iter; iter != m_EventList.end(); ++iter)
		{
			(*iter)->IPointDown(*iter);
		}
	}	

	//비밀번호 1234 후에 enter누르면 끝
	//추후에 엔딩텍스처 추가예정
	if(m_answer[0] == 1 && m_answer[1] == 2 && m_answer[2] == 3&& m_answer[3] == 4)
	{
		if (MANAGERINPUT->isKeyPressed(DIK_RETURN))
		{
		//std::cout << "clear" << std::endl;
		m_GameClear->_Update(m_ScreenWidth / 2, m_ScreenHeight / 2, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix);
		}
	}
}

void cCanvas::_Render()
{
	cGraphic::GetInstance().TurnZBufferOff();
	cGraphic::GetInstance().TurnOnAlphaBlending();

	if (g_Click)
	{
		m_xImage->_Render();
		if (m_KeyCheck)	//열쇠를 눌렀을 때만 "E = Get" 그려줌
		{
			m_TextA->_Render();
		}
		if (m_CaseCheck) //열쇠를 가진 상태에서 상자를 눌렀을 때만 "E = Open" 그려줌
		{
			m_TextB->_Render();
		}
		if (g_Pass == true)
		{
			for (int i = 0; i < 5; i++)
			{
				m_password[i]->_Render();
			}
		}
		
		m_Cursor->_Render();
	}
	
	if (!g_Click)
	{
		if (m_RayOn)
		{
			m_handImage->_Render();
		}
		else
		{
			m_Crosshair->_Render();
		}
	}
	m_GameClear->_Render();
	cGraphic::GetInstance().TurnOffAlphaBlending();
	cGraphic::GetInstance().TurnZBufferOn();
}

void cCanvas::_PassWord()
{
	if (g_Pass == false)
	{
		for (int i = 0; i < 4; i++)
		{
			m_keyInput[i] = false;
			m_answer[i] = 0;
		}
	}

		m_password[4]->_Update(m_ScreenWidth / 2 - 425, m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix);
		if (m_keyInput[0] == false)
		{
			_Wordpos(0);
			m_password[1]->SetMeshTextureUVUpdate(m_ScreenWidth / 2 + 275 + (1 * 100), m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
			m_password[2]->SetMeshTextureUVUpdate(m_ScreenWidth / 2 + 275 + (2 * 100), m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
			m_password[3]->SetMeshTextureUVUpdate(m_ScreenWidth / 2 + 275 + (3 * 100), m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
		}
		else
		{
			if (MANAGERINPUT->isKeyPressed(DIK_BACKSPACE))
			{
				m_password[0]->SetMeshTextureUVUpdate(m_ScreenWidth / 2 + 275 + (0 * 100), m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
				m_answer[0] = 0;
				m_keyInput[0] = false;
			}
			if (m_keyInput[1] == false)
			{
				_Wordpos(1);
				m_password[2]->SetMeshTextureUVUpdate(m_ScreenWidth / 2 + 275 + (2 * 100), m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
				m_password[3]->SetMeshTextureUVUpdate(m_ScreenWidth / 2 + 275 + (3 * 100), m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
			}
			else
			{
				if (MANAGERINPUT->isKeyPressed(DIK_BACKSPACE))
				{
					m_password[1]->SetMeshTextureUVUpdate(m_ScreenWidth / 2 + 275 + (1 * 100), m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
					m_answer[1] = 0;
					m_keyInput[1] = false;
				}
				if (m_keyInput[2] == false)
				{
					_Wordpos(2);
					m_password[3]->SetMeshTextureUVUpdate(m_ScreenWidth / 2 + 275 + (3 * 100), m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
				}
				else
				{
					if (MANAGERINPUT->isKeyPressed(DIK_BACKSPACE))
					{
						m_password[2]->SetMeshTextureUVUpdate(m_ScreenWidth / 2 + 275 + (2 * 100), m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
						m_answer[2] = 0;
						m_keyInput[2] = false;
					}
					if (m_keyInput[3] == false)
					{
						_Wordpos(3);
					}
					else
					{
						if (MANAGERINPUT->isKeyPressed(DIK_BACKSPACE))
						{
							m_password[3]->SetMeshTextureUVUpdate(m_ScreenWidth / 2 + 275 + (3 * 100), m_ScreenHeight / 2 + 285, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
							m_answer[3] = 0;
							m_keyInput[3] = false;
						}
					}

				}
			}
		}
	
}
void cCanvas::_Wordpos(int i)
{
	float m_wordPosX = m_ScreenWidth / 2+ 275 + (i * 100);
	float m_wordPosY = m_ScreenHeight / 2 + 285;

	if (MANAGERINPUT->isKeyPressed(DIK_0))
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0.0f,0.0f,0.1f,1 });
		m_answer[i] = 0;
		m_keyInput[i] = true;
	}
	else if (MANAGERINPUT->isKeyPressed(DIK_1))
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0.1f,0.0f,0.2f,1 });
		m_answer[i] = 1;
		m_keyInput[i] = true;
	}
	else if (MANAGERINPUT->isKeyPressed(DIK_2))
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0.2f,0.0f,0.3f,1 });
		m_answer[i] = 2;
		m_keyInput[i] = true;
	}

	else if (MANAGERINPUT->isKeyPressed(DIK_3))
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0.3f,0.0f,0.4f,1 });
		m_answer[i] = 3;
		m_keyInput[i] = true;
	}
	else if (MANAGERINPUT->isKeyPressed(DIK_4))
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0.4f,0.0f,0.5f,1 });
		m_answer[i] = 4;
		m_keyInput[i] = true;
	}

	else if (MANAGERINPUT->isKeyPressed(DIK_5))
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0.5f,0.0f,0.6f,1 });
		m_answer[i] = 5;
		m_keyInput[i] = true;
	}
	else if (MANAGERINPUT->isKeyPressed(DIK_6))
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0.6f,0.0f,0.7f,1 });
		m_answer[i] = 6;
		m_keyInput[i] = true;
	}
	else if (MANAGERINPUT->isKeyPressed(DIK_7))
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0.7f,0.0f,0.8f,1 });
		m_answer[i] = 7;
		m_keyInput[i] = true;
	}

	else if (MANAGERINPUT->isKeyPressed(DIK_8))
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0.8f,0.0f,0.9f,1 });
		m_answer[i] = 8;
		m_keyInput[i] = true;
	}
	else if (MANAGERINPUT->isKeyPressed(DIK_9))
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0.9f,0.0f,1.0f,1 });
		m_answer[i] = 9;
		m_keyInput[i] = true;
	}
	else
	{
		m_password[i]->SetMeshTextureUVUpdate(m_wordPosX, m_wordPosY, &m_UIViewMatrix, &m_UIOrthogonalProjectionMatrix, { 0,0,0,0 });
	}
}
