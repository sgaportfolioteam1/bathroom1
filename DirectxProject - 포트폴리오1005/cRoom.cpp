#include "stdafx.h"
#include "cRoom.h"

#include "cMesh.h"
#include "cLightViewShader.h"
#include "cTexture.h"
cRoom::cRoom()
{
}


cRoom::~cRoom()
{
	SAFE_DELETE(m_LightViewShader);
}

void cRoom::_Initialize()
{
	m_Meshes.reserve(4);
	std::vector<ST__Vertex_PTN> vertex;
	vertex.resize(24);


		std::vector<UINT>index;
		index.resize(36);
		index =
		{
		//	//���� 
		//	0,3,2,
		//	0,2,1,
		//	//�¹� 
		//	4,7,6,
		//	4,6,5,
		//	//�Ĺ� 
		//	8,11,10,
		//	8,10,9,
		//	//���
		//	12,15,14,
		//	12,14,13,
		//	//��
		//	16,19,18,
		//	16,18,17,
		//	//��
		//	20,23,22,
		//	20,22,21

			//���� 
			2,3,0,
			1,2,0,
			//�¹� 
			6,7,4,
			5,6,4,
			//�Ĺ� 
			10,11,8,
			9,10,8,
			//���
			14,15,12,
			13,14,12,
			//��
			18,19,16,
			17,18,16,
			//��
			22,23,20,
			21,22,20

		};

		cMesh *m_mesh = new cMesh();
		m_mesh->_InitializeVertexDYNAMIC<ST__Vertex_PTN>(vertex);
		m_mesh->_InitializeIndices(index);
	
		m_Meshes.push_back(m_mesh);

		//���� ���
		{
			std::vector<ST__Vertex_PTN> wall;
			wall.resize(24);

		
			std::vector<UINT> indices;
			indices.reserve(36);
			indices = IndexMake();

			cMesh *m_mesh1 = new cMesh();
			m_mesh1->_InitializeIndices(indices);
			m_mesh1->_InitializeVertexDYNAMIC(wall);
			m_Meshes.push_back(m_mesh1);
		}
		//���� ��
		{
			std::vector<ST__Vertex_PTN> wall;
			wall.resize(24);

		
			std::vector<UINT> indices;
			indices.reserve(36);
			indices = IndexMake();

			cMesh* m_mesh2 = new cMesh();
			m_mesh2->_InitializeIndices(indices);
			m_mesh2->_InitializeVertexDYNAMIC(wall);
			m_Meshes.push_back(m_mesh2);

		}
		
		//������ ���
		{
			std::vector<ST__Vertex_PTN> wall;
			wall.resize(24);

			std::vector<UINT> indices;
			indices.reserve(36);
			indices = IndexMake();

			cMesh* m_mesh3 = new cMesh();
			m_mesh3->_InitializeIndices(indices);
			m_mesh3->_InitializeVertexDYNAMIC(wall);
			m_Meshes.push_back(m_mesh3);
		}
	
	

		cTexture* texture = new cTexture();
		texture->_Initialize("../Resources/Bathroom2.png");
		
		m_LightViewShader = new cLightViewShader();
		m_LightViewShader->_Initialize();
		m_LightViewShader->m_Texture = texture;

}

void cRoom::_Update(D3DXMATRIX v, D3DXMATRIX p)
{
	
		std::vector<ST__Vertex_PTN> vertex;
		std::vector<ST__Vertex_PTN> vertex1;
		std::vector<ST__Vertex_PTN> vertex2;
		std::vector<ST__Vertex_PTN> vertex3;

		vertex.reserve(24);
		vertex1.reserve(24);
		vertex2.reserve(24);
		vertex3.reserve(24);

		float top, bottom, left, right, front, back;

		top = 400;
		bottom = 0;
		left = -200.f;
		right = 200.f;
		front = 400.f;
		back = -400.f;

		//���� Ÿ��
		D3DXVECTOR2 texture0 = { 0,0 };
		D3DXVECTOR2 texture1 = { 0.333f,0 };
		D3DXVECTOR2 texture2 = { 0.333f,1 };
		D3DXVECTOR2 texture3 = { 0,1 };

		//��,�� �� Ÿ��
		D3DXVECTOR2 texture8 = { 0.333f * 0.5f,0 };
		D3DXVECTOR2 texture9 = { 0.333f * 0.5f,1 };

		//���� Ÿ��
		D3DXVECTOR2 texture10 = { 0.666f,0 };
		D3DXVECTOR2 texture11 = { 0.666f,1 };

		//�ٴ� Ÿ��
		D3DXVECTOR2 texture4 = { 0.666f,0 };
		D3DXVECTOR2 texture5 = { 1,0 };
		D3DXVECTOR2 texture6 = { 1,1 };
		D3DXVECTOR2 texture7 = { 0.666f,1 };

		//���
		//D3DXVECTOR3 light0 = { 0,0,-1.570796f };
		D3DXVECTOR3 light0 = { -1.2f,-1,-1.2f };
		D3DXVECTOR3 light1 = { 0,0, 1.570796f };




		//��
		D3DXVECTOR3 FrontleftTop = D3DXVECTOR3(left, top, front); 				//���� ��
		D3DXVECTOR3 FrontrightTop = D3DXVECTOR3(right, top, front); 			//������ ��
		D3DXVECTOR3 FrontrightBottom = D3DXVECTOR3(right, bottom, front);		//������ �Ʒ�
		D3DXVECTOR3 FrontleftBottom = D3DXVECTOR3(left, bottom, front); 		//���� �Ʒ�

		//��
		D3DXVECTOR3 BackleftTop = D3DXVECTOR3(left, top, back); 				//���� ��
		D3DXVECTOR3 BackrightTop = D3DXVECTOR3(right, top, back); 			//������ ��
		D3DXVECTOR3 BackrightBottom = D3DXVECTOR3(right, bottom, back); 		//������ �Ʒ�
		D3DXVECTOR3 BackleftBottom = D3DXVECTOR3(left, bottom, back); 		//���� �Ʒ�


		ST__Vertex_PTN vertices [24];
		{
			//����
			vertices[0].m_position = FrontleftTop;
			vertices[1].m_position = FrontrightTop;
			vertices[2].m_position = FrontrightBottom;
			vertices[3].m_position = FrontleftBottom;
			vertices[0].m_texcoord = texture0;
			vertices[1].m_texcoord = texture8;
			vertices[2].m_texcoord = texture9;
			vertices[3].m_texcoord = texture3;

			vertices[0].m_normalize = light0;
			vertices[1].m_normalize = light1;
			vertices[2].m_normalize = light1;
			vertices[3].m_normalize = light0;
			//�¹�   
			vertices[4].m_position = BackleftTop;
			vertices[5].m_position = FrontleftTop;
			vertices[6].m_position = FrontleftBottom;
			vertices[7].m_position = BackleftBottom;
			vertices[4].m_texcoord = texture0;
			vertices[5].m_texcoord = texture1;
			vertices[6].m_texcoord = texture2;
			vertices[7].m_texcoord = texture3;

			vertices[4].m_normalize = light0;
			vertices[5].m_normalize = light0;
			vertices[6].m_normalize = light0;
			vertices[7].m_normalize = light0;

			//�Ĺ� 
			vertices[8].m_position = BackrightTop;
			vertices[9].m_position = BackleftTop;
			vertices[10].m_position = BackleftBottom;
			vertices[11].m_position = BackrightBottom;
			vertices[8].m_texcoord = texture0;
			vertices[9].m_texcoord = texture8;
			vertices[10].m_texcoord = texture9;
			vertices[11].m_texcoord = texture3;

			vertices[8].m_normalize = light1;
			vertices[9].m_normalize = light0;
			vertices[10].m_normalize = light0;
			vertices[11].m_normalize = light1;
			//���	 
			vertices[12].m_position = FrontrightTop;
			vertices[13].m_position = BackrightTop;
			vertices[14].m_position = BackrightBottom;
			vertices[15].m_position = FrontrightBottom;
			vertices[12].m_texcoord = texture0;
			vertices[13].m_texcoord = texture1;
			vertices[14].m_texcoord = texture2;
			vertices[15].m_texcoord = texture3;

			vertices[12].m_normalize = light1;
			vertices[13].m_normalize = light1;
			vertices[14].m_normalize = light1;
			vertices[15].m_normalize = light1;
			//��		
			vertices[16].m_position = BackleftTop;
			vertices[17].m_position = BackrightTop;
			vertices[18].m_position = FrontrightTop;
			vertices[19].m_position = FrontleftTop;
			vertices[16].m_texcoord = texture10;
			vertices[17].m_texcoord = texture11;
			vertices[18].m_texcoord = texture3;
			vertices[19].m_texcoord = texture0;

			vertices[16].m_normalize = light0;
			vertices[18].m_normalize = light1;
			vertices[19].m_normalize = light0;
			vertices[17].m_normalize = light1;
			//��		
			vertices[20].m_position = FrontleftBottom;
			vertices[21].m_position = FrontrightBottom;
			vertices[22].m_position = BackrightBottom;
			vertices[23].m_position = BackleftBottom;
			vertices[20].m_texcoord = texture5;
			vertices[21].m_texcoord = texture6;
			vertices[22].m_texcoord = texture7;
			vertices[23].m_texcoord = texture4;

			vertices[20].m_normalize = light0;
			vertices[21].m_normalize = light1;
			vertices[22].m_normalize = light1;
			vertices[23].m_normalize = light0;
			
		
		}


		for (int i = 0; i < 24; i++)
		{
			vertex.push_back(vertices[i]);
		}


		//���� ���
		{
			for (int i = 0; i < 24; ++i)
			{
				ST__Vertex_PTN vertic;
				vertic.m_position = RectMake(0, i);
				vertic.m_texcoord = TextureUV(i, 0);
				vertic.m_normalize = light0;
				vertex1.push_back(vertic);
			}

		
		}
		//���� ��
		{

			for (int i = 0; i < 24; ++i)
			{
				ST__Vertex_PTN vertic;
				vertic.m_position = RectMake(2, i);
				vertic.m_texcoord = TextureUV(i, 1);
				vertic.m_normalize = light0;
				vertex2.push_back(vertic);
			}


		;

		}

		//������ ���
		{

			for (int i = 0; i < 24; ++i)
			{
				ST__Vertex_PTN vertic;
				vertic.m_position = RectMake(1, i);
				vertic.m_texcoord = TextureUV(i, 0);
				vertic.m_normalize = light0;
				vertex3.push_back(vertic);
			}
		
		}


		if (!MANAGERINPUT->isKeyDown(DIK_SPACE))
		{
			for (int i = 0; i < 24; i++)
			{

				vertex[i].m_normalize = light0;
				vertex1[i].m_normalize = light0;
				vertex2[i].m_normalize = light0;
				vertex3[i].m_normalize = light0;

			}
		
		}
		else
		{
			for (int i = 0; i < 24; i++)
			{
				vertex[i].m_normalize = light1;
				vertex1[i].m_normalize = light1;
				vertex2[i].m_normalize = light1;
				vertex3[i].m_normalize = light1;

			}
		}

	
		m_Meshes[0]->_Update(vertex);
		m_Meshes[1]->_Update(vertex1);
		m_Meshes[2]->_Update(vertex2);
		m_Meshes[3]->_Update(vertex3);

		D3DXMATRIX world;
		D3DXMatrixIdentity(&world);
		m_LightViewShader->_Update(world, v, p);

}

void cRoom::_Render()
{
	UINT count = m_Meshes.size();
	for (UINT i = 0; i < count; ++i)
	{
		m_Meshes[i]->_Render();
		const UINT counts = m_Meshes[i]->m_indices;
		m_LightViewShader->_Render(counts);
	}

}

D3DXVECTOR3 cRoom::RectMake(int bone, int i)
{

	float top, bottom, left, right, front,back;

	//���� ���
	if (bone == 0)
	{
		top = 400;
		bottom = 0;
		left = 100.f;
		right = 200.f;
		front = 30.f;
		back = -60.f;
	
	}
	//������ ���
	else if (bone == 1)
	{
		top = 400.f;
		bottom = 0.f;
		left = -200.f;
		right = -130.f;
		front = -0.f;
		back = -100.f;
	}
	//���� ��
	else if (bone == 2)
	{
		top = 400;
		bottom = 0;
		left = 200.f;
		right = 150.f;
		front = 30.f;
		back = 400.f;

	}


	D3DXVECTOR3 FrontleftTop = D3DXVECTOR3(left, top, front); 			//���� ��
	D3DXVECTOR3 FrontrightTop = D3DXVECTOR3(right, top, front); 			//������ ��
	D3DXVECTOR3 FrontrightBottom = D3DXVECTOR3(right, bottom, front);		//������ �Ʒ�
	D3DXVECTOR3 FrontleftBottom = D3DXVECTOR3(left, bottom, front); 		//���� �Ʒ�

	D3DXVECTOR3 BackleftTop = D3DXVECTOR3(left, top, back); 			//���� ��
	D3DXVECTOR3 BackrightTop = D3DXVECTOR3(right, top, back); 			//������ ��
	D3DXVECTOR3 BackrightBottom = D3DXVECTOR3(right, bottom, back); 		//������ �Ʒ�
	D3DXVECTOR3 BackleftBottom = D3DXVECTOR3(left, bottom, back); 		//���� �Ʒ�

	std::vector<D3DXVECTOR3> m_skin;
	m_skin.resize(24 * 6);


		//����
		m_skin[0] = FrontleftTop;
		m_skin[1] = FrontrightTop;
		m_skin[2] = FrontrightBottom;
		m_skin[3] = FrontleftBottom;



		//�¹�  
		m_skin[4] = BackleftTop;
		m_skin[5] = FrontleftTop;
		m_skin[6] = FrontleftBottom;
		m_skin[7] = BackleftBottom;

		//�Ĺ� 
		m_skin[8] = BackrightTop;
		m_skin[9] = BackleftTop;
		m_skin[10] = BackleftBottom;
		m_skin[11] = BackrightBottom;

		//���	
		m_skin[12] = FrontrightTop;
		m_skin[13] = BackrightTop;
		m_skin[14] = BackrightBottom;
		m_skin[15] = FrontrightBottom;

		//��		
		m_skin[16] = BackleftTop;
		m_skin[17] = BackrightTop;
		m_skin[18] = FrontrightTop;
		m_skin[19] = FrontleftTop;

		//��		
		m_skin[20] = FrontleftBottom;
		m_skin[21] = FrontrightBottom;
		m_skin[22] = BackrightBottom;
		m_skin[23] = BackleftBottom;
	
	return m_skin[i];
}


std::vector<UINT> cRoom::IndexMake()
{
	std::vector<UINT>index;
	index.resize(36);
	index =
{
	//���� 
	0,3,2,
	0,2,1,
	//�¹� 
	4,7,6,
	4,6,5,
	//�Ĺ� 
	8,11,10,
	8,10,9,
	//���
	12,15,14,
	12,14,13,
	//��
	16,19,18,
	16,18,17,
	//��
	20,23,22,
	20,22,21
};
	


	return index;
}

D3DXVECTOR2 cRoom::TextureUV(int bone,int i)
{
	D3DXVECTOR2 texture0 = { 0,0 };
	D3DXVECTOR2 texture1 = { 0.333f * 0.125f,0 };
	D3DXVECTOR2 texture2 = { 0.333f * 0.125f,1 };
	D3DXVECTOR2 texture3 = { 0,1 };

	D3DXVECTOR2 texture4 = { 0.333f * 0.5f,0 };
	D3DXVECTOR2 texture5 = { 0.333f * 0.5f,1 };

	if (i == 0)
	{


		if (bone % 4 == 0)
		{
			return texture0;
		}
		else if (bone % 4 == 1)
		{
			return texture1;
		}
		else if (bone % 4 == 2)
		{
			return texture2;
		}
		else if (bone % 4 == 3)
		{
			return texture3;
		}
		else
		{
			//	return texture0;
		}
	}
	if (i == 1)
	{


		if (bone % 4 == 0)
		{
			return texture0;
		}
		else if (bone % 4 == 1)
		{
			return texture4;
		}
		else if (bone % 4 == 2)
		{
			return texture5;
		}
		else if (bone % 4 == 3)
		{
			return texture3;
		}
		
	}

}
