#include "stdafx.h"
#include "cManagerResource.h"

#include "cMaterial.h"
#include "cTexture.h"
#include "cMesh.h"

#include "cLightShader.h"
#include "cLightViewShader.h"

#include "cFont.h"
#include "cFontShader.h"

cManagerResource::cManagerResource()
{
	//라이트 쉐이더 생성 후 초기화
	m_LightShader = new cLightShader();
	m_LightShader->_Initialize();

	m_LightViewShader = new cLightViewShader();
	m_LightViewShader->_Initialize();

	//
	m_Textures = new cGenericResource<cTexture>();
	m_Materials = new cGenericResource<cMaterial>();
	m_Meshs = new cGenericResource<cMesh>();
	//
	cTexture* text = new cTexture();
	text->_Initialize("../Resources/font.dds");

	m_Font = new cFont();
	m_Font->_LoadFontData("../Resources/fontdata.txt");
	m_Font->m_Texture = text;

	m_FontShader = new cFontShader();
	m_FontShader->_Initialize();
}

cManagerResource::~cManagerResource()
{
	SAFE_DELETE(m_LightViewShader);
	SAFE_DELETE(m_LightShader);

	SAFE_DELETE(m_Materials);
//	SAFE_DELETE(m_Textures);
	SAFE_DELETE(m_Meshs);

	//_MaterialRelease();
	//_TextureRelease();
	//_MeshRelease();

	SAFE_DELETE(m_Font);
}

/*
void cManagerResource::_MaterialRelease()
{
	std::list<cMaterial*>::iterator iterMaterial;

	//리소스 매니져가 맨 마지막에 지워진다 가정하에 지우는 것.
	for (iterMaterial = m_Materials.begin(); iterMaterial != m_Materials.end(); )
	{
		cMaterial* ref = (*iterMaterial);
		iterMaterial = m_Materials.erase(iterMaterial);
		SAFE_RELEASE(ref);
	}
}
void cManagerResource::_SetMaterial(cMaterial * mtl)
{
	//리스트 추가하기
	m_Materials.push_back(mtl);

	std::cout << mtl->m_name.data() << std::endl;
}
cMaterial * cManagerResource::_GetMaterial(const std::string & key)
{
	//string key 비교 반환
	std::list<cMaterial*>::iterator iter = m_Materials.begin();

	//꼭 iterator을 쓸 필요는 없다.
	for (iter; iter != m_Materials.end(); ++iter)
	{
		//키 비교 반환
		if ((*iter)->m_name._Equal(key))
		{
			return *iter;
		}
	}

	return nullptr;
}

void cManagerResource::_TextureRelease()
{
	std::list<cTexture*>::iterator iter;

	//리소스 매니져가 맨 마지막에 지워진다 가정하에 지우는 것.
	for (iter = m_Textures.begin(); iter != m_Textures.end(); )
	{
		cTexture* ref = (*iter);
		iter = m_Textures.erase(iter);
		SAFE_RELEASE(ref);
	}
}

void cManagerResource::_SetTexture(cTexture * tex)
{
	//리스트 추가하기
	m_Textures.push_back(tex);

	std::cout << tex->m_name.data() << std::endl;
}

cTexture * cManagerResource::_GetTexture(const std::string & key)
{
	//string key 비교 반환
	std::list<cTexture*>::iterator iter = m_Textures.begin();

	//꼭 iterator을 쓸 필요는 없다.
	for (iter; iter != m_Textures.end(); ++iter)
	{
		//키 비교 반환
		if ((*iter)->m_name._Equal(key))
		{
			return *iter;
		}
	}

	return nullptr;
}

void cManagerResource::_MeshRelease()
{
	std::list<cMesh*>::iterator iter;

	//리소스 매니져가 맨 마지막에 지워진다 가정하에 지우는 것.
	for (iter = m_Meshs.begin(); iter != m_Meshs.end(); )
	{
		cMesh* ref = (*iter);
		iter = m_Meshs.erase(iter);
		SAFE_RELEASE(ref);
	}
}

void cManagerResource::_SetMesh(cMesh * mesh)
{
	//리스트 추가하기
	m_Meshs.push_back(mesh);

	std::cout << mesh->m_name.data() << std::endl;
}

cMesh * cManagerResource::_GetMesh(const std::string & key)
{
	//string key 비교 반환
	std::list<cMesh*>::iterator iter = m_Meshs.begin();

	//꼭 iterator을 쓸 필요는 없다.
	for (iter; iter != m_Meshs.end(); ++iter)
	{
		//키 비교 반환
		if ((*iter)->m_name._Equal(key))
		{
			return *iter;
		}
	}

	return nullptr;
}

*/

void cManagerResource::_InitializeModel()
{
	const std::string path =

		
		
			"../Resources/Crusader knight/Base mesh/crusader base mesh.fbx";
	//	"../Resources/box_2.obj";
	//	"../Crusader knight/animation/crusader@walk.fbx";

//aiProcess_Triangulate 레퍼런스 검색
	scene = importer.ReadFile(
		path,
		aiProcess_Triangulate |
		aiProcess_FlipUVs |
		aiProcess_MakeLeftHanded |
		aiProcess_JoinIdenticalVertices);

	int index = 0;

	for (UINT i = 0; i < scene->mNumMeshes; i++)
	{
		const aiMesh* paiMesh = scene->mMeshes[i];

		UINT NumVertices = paiMesh->mNumVertices;
		UINT NumIndices = paiMesh->mNumFaces * 3;

		//버택스 정보 셋팅하기
		std::vector<ST__Vertex_SKIN> vertices;
		vertices.reserve(NumVertices);
		std::vector<UINT> Indices;
		Indices.reserve(NumIndices);

		//매쉬 초기화
		_InitializeMesh(i, paiMesh, vertices, Indices);

		//본, 가중치 초기화
		_InitializeBone(i, paiMesh, vertices, m_BoneActor);

		const aiNode* groupnode = scene->mRootNode->mChildren[0];
		const aiNode* meshnode;
		std::string name = "";

		//검
		if(index >= groupnode->mNumChildren)
		{
	//		meshnode = groupnode->mChildren[12];
	//		name = "Sword";
		}
		//다른 메쉬정보들
		else
		{
			meshnode = groupnode->mChildren[index];
			name = meshnode->mName.data;
		}

		//매쉬 생성
		cMesh* meshref = new cMesh();

		meshref->m_name = name;

		std::cout << i << "  " << name << std::endl;
		meshref->m_stride = sizeof(ST__Vertex_SKIN);

		meshref->_InitializeVertex<ST__Vertex_SKIN>(vertices);

		meshref->_InitializeIndices(Indices);

		//MANAGERRESOURCE->_SetMesh(meshref);
		MANAGERRESOURCE->m_Meshs->_AddResource(meshref);

		index++;
	}
}

void cManagerResource::_InitializeMesh(
	UINT MeshIndex,
	const aiMesh * paiMesh,
	std::vector<struct ST__Vertex_SKIN>& vertices,
	std::vector<UINT>& Indices)
{
	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

	ST__Vertex_SKIN vertices_data;
	// Populate the vertex attribute vectors
	for (UINT i = 0; i < paiMesh->mNumVertices; i++) {
		const aiVector3D* pPos = &(paiMesh->mVertices[i]);
		const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;
		const aiVector3D* pNormal = &(paiMesh->mNormals[i]);

		vertices_data.m_position = D3DXVECTOR3(pPos->x, pPos->y, pPos->z);
		vertices_data.m_texcoord = D3DXVECTOR2(pTexCoord->x, pTexCoord->y);
		vertices_data.m_normal = D3DXVECTOR3(pNormal->x, pNormal->y, pNormal->z);

		//공간예약이므로 push
		//아직 가중치는 안됨
		vertices.push_back(vertices_data);
	}

	// Populate the index buffer
	for (UINT i = 0; i < paiMesh->mNumFaces; i++) {
		const aiFace& Face = paiMesh->mFaces[i];
		assert(Face.mNumIndices == 3);
		Indices.push_back(Face.mIndices[0]);
		Indices.push_back(Face.mIndices[1]);
		Indices.push_back(Face.mIndices[2]);
	}
}

void cManagerResource::_InitializeBone(
	UINT MeshIndex, const aiMesh * paiMesh,
	std::vector<struct ST__Vertex_SKIN>& vertices, struct ST__Acter& acter)
{
	//본 정보를 가져온다.
	for (UINT i = 0; i < paiMesh->mNumBones; i++) {

		UINT BoneIndex = 0;
		std::string BoneName(paiMesh->mBones[i]->mName.data);

		if (acter.m_BoneMapping.find(BoneName) == acter.m_BoneMapping.end()) {
			// Allocate an index for a new bone
			BoneIndex = acter.m_NumBones;
			acter.m_NumBones++;

			//해당 본의 BoneOffset 지정한다.
			ST__BindPose bindpose;
			acter.m_BindPose.push_back(bindpose);
			//m_BindPose.push_back(bindpose);

			acter.m_BindPose[BoneIndex].BoneOffset = paiMesh->mBones[i]->mOffsetMatrix;
			acter.m_BoneMapping[BoneName] = BoneIndex;
		}
		else {
			BoneIndex = acter.m_BoneMapping[BoneName];
		}

		//가중치 인덱스, 벨류 셋팅
		for (UINT j = 0; j < paiMesh->mBones[i]->mNumWeights; j++) {

			UINT VertexID = paiMesh->mBones[i]->mWeights[j].mVertexId;
			float Weight = paiMesh->mBones[i]->mWeights[j].mWeight;

			vertices[VertexID].AddBoneData(BoneIndex, Weight);
		}
	}
}

