
//MatrixBuffer은 어디단계에 넣는 버퍼인가.?
//VSSetConstantBuffers(0, 1, &m_matrixBuffer);
cbuffer MatrixBuffer : register(b0)
{
    //cbuffer Constant Buffer 약자이다.
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

//VSSetConstantBuffers(1, 1, &m_cameraBuffer);
cbuffer CameraBuffer : register(b1)
{
    float3 cameraPosition;
    float padding;
};

struct VertexInputType
{
    float4 position : POSITION;
	float2 tex : TEXCOORD0;
    float3 normalize : NORMALIZE;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
    float3 normalize : NORMALIZE;
	float3 viewDirection : TEXCOORD1;
};


PixelInputType VS(VertexInputType input)
{
	PixelInputType output;
	
	input.position.w = 1.0f;
	
	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	output.tex = input.tex;
    
    //월드 행렬에 대해서만 법선 벡터를 계산한다.
    output.normalize = mul(input.normalize, (float3x3) worldMatrix);
	
    //빛의 세기를 균등하게
    output.normalize = normalize(output.normalize);
    
    // 세계의 정점 위치를 계산합니다.
    float4 worldPosition = mul(input.position, worldMatrix);

    // 카메라의 위치와 세계의 정점 위치를 기준으로 보기 방향을 결정합니다.
    output.viewDirection = cameraPosition.xyz - worldPosition.xyz;
	
    // 뷰 방향 벡터를 표준화합니다.
    output.viewDirection = normalize(output.viewDirection);
    
    
	return output;
}

//PSSetShaderResources(0, 1, &m_ShaderResource);
Texture2D shaderTexture : register(t0);

//PSSetSamplers(0, 1, &m_SampleState);
SamplerState SampleType : register(s0);

//PSSetConstantBuffers(0, 1, &m_lightBuffer);
cbuffer LightBuffer : register(b0)
{
    float4 ambientColor;
    float4 diffuseColor;
    float3 lightDirection;
    float specularPower;
    float4 specularColor;
};

float4 PS(PixelInputType input) : SV_TARGET
{
    // 이 텍스처 좌표 위치에서 샘플러를 사용하여 텍스처에서 픽셀 색상을 샘플링합니다.
    float4 textureColor = shaderTexture.Sample(SampleType, input.tex);

	// 모든 픽셀의 기본 출력 색상을 주변 광원 값으로 설정합니다.
    float4 color = ambientColor;

	// specular color를 초기화한다.
    float4 specular = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// 계산을 위해 빛 방향을 반전시킵니다.
    float3 lightDir = -lightDirection;

	// 이 픽셀의 빛의 양을 계산합니다.
    float lightIntensity = saturate(dot(input.normalize, lightDir));

    if (lightIntensity > 0.0f)
    {
        // 확산 색과 광 강도의 양에 따라 최종 확산 색을 결정합니다.
        color += (diffuseColor * lightIntensity);
    
    	// 최종 빛의 색상을 채 웁니다.
        color = saturate(color);

		// 빛의 강도, 법선 벡터 및 빛의 방향에 따라 반사 벡터를 계산합니다.
        float3 reflection = normalize(2 * lightIntensity * input.normalize - lightDir);

		// 반사 벡터, 시선 방향 및 반사 출력을 기준으로 반사 조명의 양을 결정합니다.
        specular = pow(saturate(dot(reflection, input.viewDirection)), specularPower);
    }

	// 텍스처 픽셀과 최종 확산 색을 곱하여 최종 픽셀 색상 결과를 얻습니다.
    color = color * textureColor;

	// 출력 색상의 마지막에 반사 컴포넌트를 추가합니다.
    color = saturate(color + specular);

    return color;
}