﻿#include "stdafx.h"
#include "cExcute.h"

#include "cCamera.h"
#include "cCanvas.h"

#include "cAssimpObjectLoad.h"
#include "cTransform.h"
#include "cRoom.h"

D3DXVECTOR3 g_CameraPosition;

//해야 되는 것
//열쇠를 획득하지 않은 상태에서 상자에 포인트레이를 갖다대면 손모양은 뜨지만 클릭시 아무런 반응이 없다.
//열쇠를 획득한 상태에서 상자를 클릭할 경우(상자가 열리면서(열리는 애니메이션이 있다면)) 쪽지가 나오면서 쪽지에 힌트가 적혀있다.
//상자 안에 있는 쪽지에 적힌 것 "그녀가 아는 것은 패스워드 그 자체가 아니다. 패스워드의 위치를 가르키는 것이다."

//추가하면 좋은 것
//상자 있는 쪽 벽에 거울 또는 액자 같은 거 걸어두고 상호작용으로 돌려볼 수 있게 해줌
//돌려서 보면 뒷쪽에 쪽지가 붙어있음(쪽지에 적힌 게 힌트는 아니기 때문에 꼭 필요한 건 아님)

cExcute::cExcute()
{
}

cExcute::~cExcute()
{
	SAFE_DELETE(m_Canvas);

	//오브젝트
	{
		SAFE_DELETE(m_Camera);
		SAFE_DELETE(m_room);
		SAFE_DELETE(m_bath);
		SAFE_DELETE(m_door);
		SAFE_DELETE(m_door2);
		SAFE_DELETE(m_door3);
		SAFE_DELETE(m_table);
		SAFE_DELETE(m_toilet);
		SAFE_DELETE(m_radiateur);
		SAFE_DELETE(m_shelf);
		SAFE_DELETE(m_shelf2);
		SAFE_DELETE(m_washStand0);
		SAFE_DELETE(m_washStand1);
		SAFE_DELETE(m_washStand2);
		SAFE_DELETE(m_washStand3);
		SAFE_DELETE(m_case);
		SAFE_DELETE(m_flowerpot);
		SAFE_DELETE(m_flowerpot2);
		SAFE_DELETE(m_photoframe);
		SAFE_DELETE(m_photoframe2);
		SAFE_DELETE(m_photoframe3);
		SAFE_DELETE(m_photoframe4);
		SAFE_DELETE(m_paper1);
		SAFE_DELETE(m_paper2);
		SAFE_DELETE(m_paper3);
		SAFE_DELETE(m_mirror);
	}
}

void cExcute::_Initialize()
{
	//Camera
	{
		m_Camera = new cCamera();
		m_Camera->_Initialize();
	}

	//Canvas
	{
		m_Canvas = new cCanvas();
		m_Canvas->_Initialize();
	}

	//방
	{
		{
			m_room = new cRoom();
			m_room->_Initialize();
		}
		//욕조
		{
			m_bath = new cAssimpObjectLoad();
			m_bath->_Initialize("../Resources/wanna/wanna1.obj");
			m_bath->m_Transform->_Rotate(-90, 90, 0);
			m_bath->m_Transform->_Translation(-328, 59, -267);
		}
		//문
		{
			m_door = new cAssimpObjectLoad();
			m_door->_Initialize("../Resources/door/10057_wooden_door_v3_iterations-2.obj");
			m_door->_TextureSet("../Resources/door/10057_wooden_door_v1_diffuse.jpg");
			m_door->m_Transform->_Scaling(1.6f, 1.5f, 1.5f);
			m_door->m_Transform->_Translation(149, 0, 270);
			m_door->m_Transform->_Rotate(-90, -90, 0);
			m_doorMat = cTransform();
			m_doorMat = *m_door->m_Transform;


			m_door2 = new cAssimpObjectLoad();
			m_door2->_Initialize("../Resources/door2/16696_20_Pane_Casement_Window-Pine_V1.obj");
			m_door2->_TextureSet("../Resources/default2.png");
			m_door2->m_Transform->_Scaling(1.6f, 1.1f, 3.8f);
			m_door2->m_Transform->_Translation(0, 96, -399);
			m_door2->m_Transform->_Rotate(90, 180, 0);

			m_door3 = new cAssimpObjectLoad();
			m_door3->_Initialize("../Resources/papper/papper.obj");
			m_door3->m_Transform->_Scaling(3.7, 5.45, 0.01f);
			m_door3->m_Transform->_Translation(0, -22, -400);
			m_door3->m_Transform->_Rotate(0, 180, 0);

		}
		//테이블
		{
			m_table = new cAssimpObjectLoad();
			m_table->_Initialize("../Resources/table/obj/console_table.obj");
			m_table->_TextureSet("../Resources/table/console_table_diffuse.png");
			m_table->m_Transform->_Rotate(0, 90, 0);
			m_table->m_Transform->_Scaling(6, 3, 5);
			m_table->m_Transform->_Translation(160, 2, -212);
		}
		//변기
		{
			m_toilet = new cAssimpObjectLoad();
			m_toilet->_Initialize("../Resources/toilet/Toilet.3ds");
			m_toilet->m_Transform->_Scaling(0.7f, 0.7f, 0.7f);
			m_toilet->m_Transform->_Rotate(-90, 180, 0);
			m_toilet->m_Transform->_Translation(115, 1, 133);
		}
		//라디에이터
		{
			m_radiateur = new cAssimpObjectLoad();
			m_radiateur->_Initialize("../Resources/radiateur/Radiateur.obj");
			m_radiateur->m_Transform->_Rotate(0, 90, 0);
			m_radiateur->m_Transform->_Translation(-133, 1, -3);
		}
		//선반
		{
			m_shelf = new cAssimpObjectLoad();
			m_shelf->_Initialize("../Resources/shelf/shelves.obj");
			m_shelf->_TextureSet("../Resources/shelf/Wood Texture.jpg");
			m_shelf->m_Transform->_Rotate(0, 90, 0);
			m_shelf->m_Transform->_Scaling(100, 50, 100);
			m_shelf->m_Transform->_Translation(157, 28, -300);

			m_shelf2 = new cAssimpObjectLoad();
			m_shelf2->_Initialize("../Resources/shelf/shelves.obj");
			m_shelf2->_TextureSet("../Resources/shelf/Wood Texture.jpg");
			m_shelf2->m_Transform->_Rotate(0, 90, 0);
			m_shelf2->m_Transform->_Scaling(100, 50, 100);
			m_shelf2->m_Transform->_Translation(-146, 28, 251);

		}
		//세면대
		{
			m_washStand0 = new cAssimpObjectLoad();
			m_washStand0->_Initialize("../Resources/washstand/Lavabo.obj");
			m_washStand0->m_Transform->_Rotate(0, 90, 0);
			m_washStand0->m_Transform->_Scaling(1, 1, 1);
			m_washStand0->m_Transform->_Translation(-150, 89, 100);

			m_washStand1 = new cAssimpObjectLoad();
			m_washStand1->_Initialize("../Resources/washstand/Lavabo.obj");
			m_washStand1->m_Transform->_Rotate(0, 90, 0);
			m_washStand1->m_Transform->_Scaling(1, 1, 1);
			m_washStand1->m_Transform->_Translation(-150, 89, 300);


			m_washStand2 = new cAssimpObjectLoad();
			m_washStand2->_Initialize("../Resources/box.obj");
			m_washStand2->_TextureSet("../Resources/marble.jpg");
			m_washStand2->m_Transform->_Rotate(0, 0, 0);
			m_washStand2->m_Transform->_Scaling(10, 2, 40);
			m_washStand2->m_Transform->_Translation(-156, 113, 202);


			m_washStand3 = new cAssimpObjectLoad();
			m_washStand3->_Initialize("../Resources/box.obj");
			m_washStand3->_TextureSet("../Resources/marble.jpg");
			m_washStand3->m_Transform->_Rotate(0, 0, 0);
			m_washStand3->m_Transform->_Scaling(7, 12, 2);
			m_washStand3->m_Transform->_Translation(-140, 47, 202);
		}
		//케이스
		{
			m_case = new cAssimpObjectLoad();
			m_case->_Initialize("../Resources/case/13449_Treasure_Chest_v1_l1.obj");
			m_case->_TextureSet("../Resources/case/13449_Treasure_Chest_Diffuse.jpg");
			m_case->m_Transform->_Rotate(-90, -90, 0);
			m_case->m_Transform->_Scaling(1.7f, 1, 1);
			m_case->m_Transform->_Translation(146, 100, -148);
			m_caseMat = cTransform();
			m_caseMat = *m_case->m_Transform;
		}
		//화분
		{
			m_flowerpot = new cAssimpObjectLoad();
			m_flowerpot->_Initialize("../Resources/flowerpot/10435_Begonia_v3_L3.obj");
			m_flowerpot->_TextureSet("../Resources/flowerpot/10435_Begonia_v002DiffuseMap.jpg");
			m_flowerpot->m_Transform->_Rotate(-90,0,0);
			m_flowerpot->m_Transform->_Scaling(2.f, 2.f, 2.f);
			m_flowerpot->m_Transform->_Translation(150, 100, -307);

			m_flowerpot2 = new cAssimpObjectLoad();
			m_flowerpot2->_Initialize("../Resources/flowerpot2/11.obj");
			m_flowerpot2->_TextureSet("../Resources/flowerpot2/arch75_011_diffuse.jpg");
			m_flowerpot2->m_Transform->_Scaling(0.8f, 1.5f, 0.8f);
			m_flowerpot2->m_Transform->_Translation(-136, 124, 203);
		}
		//액자
		{
			m_photoframe = new cAssimpObjectLoad();
			m_photoframe->_Initialize("../Resources/photoframe/album.obj");
			m_photoframe->_TextureSet("../Resources/photoframe/1.jpg");
			m_photoframe->m_Transform->_Rotate(0, 40, 0);
			m_photoframe->m_Transform->_Scaling(0.2f, 0.2f, 0.2f);
			m_photoframe->m_Transform->_Translation(-130, 123, 325);
			m_photoframeMat1 =  cTransform();
			m_photoframeMat1 = *m_photoframe->m_Transform;
	
			m_photoframe2 = new cAssimpObjectLoad();
			m_photoframe2->_Initialize("../Resources/photoframe/album.obj");
			m_photoframe2->_TextureSet("../Resources/photoframe/2.jpg");
			m_photoframe2->m_Transform->_Rotate(0, 96, 0);
			m_photoframe2->m_Transform->_Scaling(0.2f, 0.2f, 0.2f);
			m_photoframe2->m_Transform->_Translation(-130, 123, 375);
			m_photoframeMat2 =  cTransform();
			m_photoframeMat2 = *m_photoframe2->m_Transform;
			
			m_photoframe3 = new cAssimpObjectLoad();
			m_photoframe3->_Initialize("../Resources/photoframe/album.obj");
			m_photoframe3->_TextureSet("../Resources/photoframe/3.jpg");
			m_photoframe3->m_Transform->_Rotate(0, 103, 0);
			m_photoframe3->m_Transform->_Scaling(0.2f, 0.2f, 0.2f);
			m_photoframe3->m_Transform->_Translation(-150, 123, 350);
			m_photoframeMat3 = cTransform();
			m_photoframeMat3 = *m_photoframe3->m_Transform;

			m_photoframe4 = new cAssimpObjectLoad();
			m_photoframe4->_Initialize("../Resources/photoframe/album.obj");
			m_photoframe4->_TextureSet("../Resources/photoframe/4.jpg");
			m_photoframe4->m_Transform->_Rotate(0, 90, 0);
			m_photoframe4->m_Transform->_Scaling(0.2f, 0.2f, 0.2f);
			m_photoframe4->m_Transform->_Translation(-184, 0, 187);
			m_photoframeMat4 =  cTransform();
			m_photoframeMat4 = *m_photoframe4->m_Transform;

		}
		//종이
		{
			m_paper1 = new cAssimpObjectLoad();
			m_paper1->_Initialize("../Resources/papper/papper.obj");
			m_paper1->_TextureSet("../Resources/papper/papper1.jpg");
			m_paper1->m_Transform->_Rotate(90, 90, 0);
			m_paper1->m_Transform->_Scaling(0.5f, 0.5f, 0.01f);
			m_paper1->m_Transform->_Translation(-170, 32, 253);
			m_paperMat1 = cTransform();
			m_paperMat1 = *m_paper1->m_Transform;

			m_paper2 = new cAssimpObjectLoad();
			m_paper2->_Initialize("../Resources/papper/papper.obj");
			m_paper2->_TextureSet("../Resources/papper/papper1.jpg");
			m_paper2->m_Transform->_Rotate(90, -90, 0);
			m_paper2->m_Transform->_Scaling(0.5f, 0.5f, 0.01f);
			m_paper2->m_Transform->_Translation(184, 31, -302);
			m_paperMat2 = cTransform();
			m_paperMat2 = *m_paper2->m_Transform;

			m_paper3= new cAssimpObjectLoad();
			m_paper3->_Initialize("../Resources/papper/papper1.obj");
			m_paper3->_TextureSet("../Resources/papper/papper1.jpg");
			m_paper3->m_Transform->_Rotate(90, -163, 0);
			m_paper3->m_Transform->_Scaling(0.5f, 0.5f, 0.01f);
			m_paper3->m_Transform->_Translation(150, 100, -213);
			m_paperMat3 = cTransform();
			m_paperMat3 = *m_paper3->m_Transform;
		}
		//거울?
		{
			m_mirror = new cAssimpObjectLoad();
			m_mirror->_Initialize("../Resources/papper/papper.obj");
			m_mirror->m_Transform->_Rotate(0, 90, 0);
			m_mirror->m_Transform->_Scaling(8, 3, 0.01f);
			m_mirror->m_Transform->_Translation(-200, 133, 197);
		}
		//열쇠
		{
			m_key = new cAssimpObjectLoad();
			m_key->_Initialize("../Resources/Key/10927_Paper_clip_v2_LOD3.obj");
			//m_key->m_Transform->_Rotate(0, 90, 0);
			m_key->m_Transform->_Scaling(7, 7, 7);
			m_key->m_Transform->_Translation(140, 132, -317);

			m_KeyMat =  cTransform();
			m_KeyMat = *m_key->m_Transform;
		}
	}
}

void cExcute::_Update()
{
	//카메라 업데이트	
	if(!g_Click)
	{
		m_Camera->_Update();
	}

	//오브젝트 업데이트
	{
		m_room->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_door->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_door2->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_door3->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_bath->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_table->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_toilet->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_radiateur->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_shelf->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_shelf2->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_washStand0->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_washStand1->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_washStand2->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_washStand3->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_case->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_flowerpot->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_flowerpot2->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_photoframe->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_photoframe2->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_photoframe3->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_photoframe4->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);

		m_paper1->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		m_paper2->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		if (m_CaseOpen)
		{
			m_paper3->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		}

		m_mirror->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		
		if (!m_GetKey)
		{
			m_key->_Update(m_Camera->m_viewMatrix, m_Camera->m_projectionMatrix);
		}
	}

	
	//마우스 상호작용( 오브젝트, 비교할 매트[ex.기본좌표] )
	{	
		m_objectNum = EMPTY;	//포인트 레이에 아무것도 안 걸릴 경우 EMPTY
		if (!g_Click)
		{
			m_Canvas->m_RayOn = false;						
		}

		POINT point;
		point.x = Setting::GetInstance().GetWindowWidth() / 2;
		point.y = Setting::GetInstance().GetWindowHeight() / 2;

		float pointX;
		float pointY;
		D3DXMATRIX proj = m_Camera->m_projectionMatrix;

		// 마우스 커서 좌표를 -1에서 +1 범위로 이동합니다
		// 뷰포트의 종횡비를 고려하여 투영 행렬을 사용하여 점을 조정합니다
		pointX = ((2.0f * point.x) / Setting::GetInstance().GetWindowWidth()) - 1.0f;
		pointY = (((-2.0f * point.y) / Setting::GetInstance().GetWindowHeight()) + 1.0f);

		pointX = pointX / proj(0, 0);
		pointY = pointY / proj(1, 1);

		D3DXVECTOR3 origin = D3DXVECTOR3(0, 0, 0);
		D3DXVECTOR3 direction = D3DXVECTOR3(pointX, pointY, 1);

		// 뷰 행렬의 역함수를 구합니다.
		D3DXMATRIX view = m_Camera->m_viewMatrix;
		D3DXMATRIX viewInverse;
		D3DXMatrixInverse(&viewInverse, 0, &view);

		// 이제 광선 원점과 광선 방향을 뷰 공간에서 월드 공간으로 변환합니다.
		D3DXVec3TransformCoord(&origin, &origin, &viewInverse);
		D3DXVec3TransformNormal(&direction, &direction, &viewInverse);
		D3DXVec3Normalize(&direction, &direction);	

		RaySphereIntersectionTest(&origin, &direction, m_key->m_Transform->_GetWorld(), 10, KEY);		
		RaySphereIntersectionTest(&origin, &direction, m_case->m_Transform->_GetWorld(), 25, CASE);
		RaySphereIntersectionTest(&origin, &direction, m_paper1->m_Transform->_GetWorld(), 11, PAPER1);
		RaySphereIntersectionTest(&origin, &direction, m_paper2->m_Transform->_GetWorld(), 11, PAPER2);
		RaySphereIntersectionTest(&origin, &direction, m_paper3->m_Transform->_GetWorld(), 11, PAPER3);
		RaySphereIntersectionTest(&origin, &direction, m_photoframe->m_Transform->_GetWorld(), 12, PHOTOFRAME1);
		RaySphereIntersectionTest(&origin, &direction, m_photoframe2->m_Transform->_GetWorld(), 12, PHOTOFRAME2);
		RaySphereIntersectionTest(&origin, &direction, m_photoframe3->m_Transform->_GetWorld(), 12, PHOTOFRAME3);
		RaySphereIntersectionTest(&origin, &direction, m_photoframe4->m_Transform->_GetWorld(), 12, PHOTOFRAME4);
		//문도 이런 식으로 해주면 될 거 같다.
		D3DXMATRIX door = *m_door->m_Transform->_GetWorld();
		door. _42 = 180;
		RaySphereIntersectionTest(&origin, &direction,&door, 40, DOOR);
		
		if (m_objectNum == KEY)
		{
			MousePicking(m_key, m_KeyMat);
		}
		else if (m_objectNum == CASE)//상자를 눌렀을 때
		{
			if (!m_CaseOpen)
			{
				MousePicking(m_case, m_caseMat);
			}			
		}
		else if (m_objectNum == PAPER1)
		{
			MousePicking(m_paper1, m_paperMat1);
		}
		else if (m_objectNum == PAPER2)
		{
			MousePicking(m_paper2, m_paperMat2);
		}
		else if (m_objectNum == PAPER3)
		{
			if (m_CaseOpen)
			{
				MousePicking(m_paper3, m_paperMat3);
			}
		}
		else if (m_objectNum == PHOTOFRAME1)
		{
			MousePicking(m_photoframe, m_photoframeMat1);
		}
		else if (m_objectNum == PHOTOFRAME2)
		{
			MousePicking(m_photoframe2, m_photoframeMat2);
		}
		else if (m_objectNum == PHOTOFRAME3)
		{
			MousePicking(m_photoframe3, m_photoframeMat3);
		}
		else if (m_objectNum == PHOTOFRAME4)
		{
			MousePicking(m_photoframe4, m_photoframeMat4);
		}
		else if (m_objectNum == DOOR)
		{
			MousePicking(m_door, m_doorMat);
		}
	}

	//Canvas
	{
		m_Canvas->_Update();
	}

	if (MANAGERINPUT->isKeyPressed(DIK_SPACE))
	{
		cout << MANAGERINPUT->getMousePosition().x << endl;
		cout << MANAGERINPUT->getMousePosition().y << endl;	
	}

}

void cExcute::_Render()
{
	//오브젝트 렌더
	{
		m_bath->_Render();

		m_door->_Render();
		m_door2->_Render();
		m_door3->_Render();

		m_room->_Render();

		m_table->_Render();

		m_toilet->_Render();

		m_radiateur->_Render();

		m_shelf->_Render();
		m_shelf2->_Render();

		m_washStand0->_Render();
		m_washStand1->_Render();
		m_washStand2->_Render();
		m_washStand3->_Render();

		m_case->_Render();

		m_flowerpot->_Render();
		m_flowerpot2->_Render();

		m_photoframe->_Render();
		m_photoframe2->_Render();
		m_photoframe3->_Render();
		m_photoframe4->_Render();

		m_paper1->_Render();
		m_paper2->_Render();
		if (m_CaseOpen)
		{
			m_paper3->_Render();
		}

		m_mirror->_Render();
		
		if (!m_GetKey)//열쇠를 획득하지 않은 상태일 경우
		{
			m_key->_Render();//그려준다.
		}
	}
	
	//Canvas
	{
		m_Canvas->_Render();
	}
}

bool cExcute::RaySphereIntersectionTest(D3DXVECTOR3* origin, D3DXVECTOR3* direction, D3DXMATRIX* target, float objectRange, int objectNum)
{	
	D3DXVECTOR3 targetpos = D3DXVECTOR3(target->_41, target->_42, target->_43);
	
	D3DXVECTOR3 v = *origin - targetpos;	

	float b = 2.0f * D3DXVec3Dot(direction, &v);
	float c = D3DXVec3Dot(&v, &v) - (objectRange * objectRange); //1 -> radius 변수로 줄것.

	float discriminant = (b * b) - (4.0f * c);

	if (discriminant < 0.0f)
		return false;

	discriminant = sqrt(discriminant);

	float s0 = (-b + discriminant) / 2.0f;
	float s1 = (-b - discriminant) / 2.0f;
	
	//130 미만이면 true를 반환
	float distance = sqrt(pow(origin->x - target->_41,2) + pow(origin->y - target->_42, 2)+ pow(origin->z - target->_43, 2));
	
	if (distance <= 160)
	{		
		m_objectNum = objectNum;//범위 안에 들어오면 무슨 오브젝트인지 값이 들어감
		if (s0 >= 0.0f || s1 >= 0.0f)
		{
			return true;
		}
	}
	
	return false;
}

//여기서 예외처리가 필요한 오브젝트 상호작용을 처리한다.
void cExcute::MousePicking(cAssimpObjectLoad* object, cTransform mat)
{	
	if (m_objectNum == KEY)//피킹 레이가 가리키는 게 열쇠일 경우
	{
		if (!m_GetKey)//열쇠를 획득한 상태가 아니라면
		{
			m_Canvas->m_RayOn = true;//손 모양이 뜬다.
		}
	}
	else
	{
		m_Canvas->m_RayOn = true;
	}	

	if (MANAGERINPUT->isMousePressed(0))
	{								
		if (m_objectNum == KEY)//클릭한 게 열쇠일 경우
		{
			if (!m_GetKey)//열쇠를 얻지 않았다면
			{
				g_Click = true;//클릭이 됨
				m_Canvas->m_KeyCheck = true;
			}
		}
		else if (m_objectNum == CASE)//클릭한 게 상자일 경우
		{
			if (m_GetKey)//열쇠를 얻었다면
			{
				g_Click = true;
				m_Canvas->m_CaseCheck = true;
			}
			else//열쇠를 얻지 않았다면
			{
				g_Click = false;//클릭이 안 된다.
			}
		}
		else if (m_objectNum == DOOR)
		{			
			g_Click = true;
			g_Pass = true;
		}
		else
		{
			g_Click = true;
		}		
	}

	if (g_Click)//클릭을 한 상태에서
	{
		if (MANAGERINPUT->isKeyPressed(DIK_E))//E를 누르면
		{
			if (m_objectNum == KEY)
			{
				m_Canvas->m_KeyCheck = false;
				m_GetKey = true;//열쇠를 얻는다.
				g_Click = false;//클릭 상태에서 빠져나온다.				
			}
			if (m_objectNum == CASE)
			{
				m_Canvas->m_CaseCheck = false;
				m_CaseOpen = true;//상자가 열린다.
				g_Click = false;//클릭 상태에서 빠져나온다.
			}
		}		
	}		

	if (!g_Click)
	{
		D3DXVECTOR3 temp;
		
		temp = mat.m_localRotation;		

		object->m_Transform->OverwriteRotate(temp.x,temp.y, temp.z);
		object->m_Transform->_SetWorld(*mat._GetWorld());
	}

	if (MANAGERINPUT->isMouseDown(0))
	{
		if (MANAGERINPUT->isMousePressed(0))
		{
			m_stPrevMousePosition = MANAGERINPUT->getMousePosition();
		}

		POINT ptCurrMouse = MANAGERINPUT->getMousePosition();

		if (g_Click)
		{		
			//회전 시켜서 봐야하는 오브젝트만
			if (m_objectNum == KEY ||
				m_objectNum == PHOTOFRAME1 ||
				m_objectNum == PHOTOFRAME2 ||
				m_objectNum == PHOTOFRAME3 ||
				m_objectNum == PHOTOFRAME4 ||
				m_objectNum == PAPER1 ||
				m_objectNum == PAPER2 ||
				m_objectNum == PAPER3)
			{

				D3DXVECTOR3 temp2 = mat.m_localPosition;

				float fDeltaX = (ptCurrMouse.x - m_stPrevMousePosition.x) * 0.3f;
				float fDeltaY = (ptCurrMouse.y - m_stPrevMousePosition.y) * 0.3f;
				float setX = (temp2.x + m_Camera->GetEye().x) / 2;
				float setY = (temp2.y + m_Camera->GetEye().y) / 2;
				float setZ = (temp2.z + m_Camera->GetEye().z) / 2;

				object->m_Transform->_Rotate(fDeltaY, -fDeltaX, 0);
				object->m_Transform->OverwriteTranslation(setX, setY, setZ);
				m_stPrevMousePosition = ptCurrMouse;
			}
		}		
	}
}