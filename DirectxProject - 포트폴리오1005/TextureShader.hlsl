
cbuffer MatrixBuffer : register(b0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

struct VertexInputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD;
};

struct PixelInputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD;
};


PixelInputType VS(VertexInputType input)
{
	PixelInputType output;

	input.position.w = 1.0f;

	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	output.tex = input.tex;

	return output;
}

Texture2D shaderTexture;
//추후 설명
SamplerState SampleType;

float4 PS(PixelInputType input) : SV_TARGET
{
	//텍스처에서 픽셀 색상을 샘플링한다.
	float4 textureColor = shaderTexture.Sample(SampleType, input.tex);

	if (textureColor.r == 1.0f && textureColor.g == 0.0f && textureColor.b == 1.0f)
	{
		textureColor = float4(0,0,0,0);
	}
	// If the color is other than black on the texture then this is a pixel in the font so draw it using the font pixel color.
	else
	{
	//	return textureColor;
	}


	return textureColor;
}