#pragma once

class cFontShader;

class cFont
{
public:
	cFont();
	~cFont();

private:
	struct FontType
	{
		float left, right;
		int size;
	};

public:
	bool _LoadFontData(const std::string& file);
	std::vector<FontType> m_fontDate;
	class cTexture* m_Texture = nullptr;

	UINT BuildVertexArray(
		void* vertices, const char* sentence, float drawX, float drawY);

};

