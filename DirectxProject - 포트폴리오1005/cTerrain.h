#pragma once

/*
굳이 상속은 안하겠다..
*/
const int TEXTURE_REPEAT = 8;

//버택스 버퍼, 인덱스 버퍼 저장용
class cMesh;
class cLightShader;

class cTerrain
{
public:
	cTerrain();
	~cTerrain();
private:
	struct HeightMapType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};

	int m_terrainWidth = 0;
	int m_terrainHeight = 0;
	int m_vertexCount = 0;
	int m_indexCount = 0;

	HeightMapType* m_heightMap = nullptr;

	cMesh* m_mesh = nullptr;
	cLightShader* m_LightShader = nullptr;

private:
	//BMP 파일 불러오기
	void LoadHeightMap(const std::string& file);
	//Y축 표준화
	void NormalizeHeightMap();
	//법선 계산
	bool CalculateNormals();
	//UV 계산
	void CalculateTextureCoordinates();

	//선형보간
	inline float Lerp(float a, float b, float t);
public:
	void _Initialize();
	void _Update(D3DXMATRIX *v, D3DXMATRIX *);
	void _Render();

	//y축 구하는 용도
	const float _GetHight(float x, float y, float z);
};

