#include "stdafx.h"
#include "cAssimpObjectLoad.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "cMesh.h"
#include "cObjectShader.h"
#include "cTexture.h"
#include "cTransform.h"
cAssimpObjectLoad::cAssimpObjectLoad()
{
	m_Transform = new cTransform();
	//m_Transform->_Scaling(0.5f, 0.5f, 0.5f);
}


cAssimpObjectLoad::~cAssimpObjectLoad()
{
	SAFE_DELETE(m_ObjectShader);
	SAFE_DELETE(m_Transform);

	//메쉬 지워야함;;
}

void cAssimpObjectLoad::_Initialize(const std::string & fileName)
{
	Assimp::Importer import;
	const aiScene *scene = import.ReadFile(
		fileName,
		//"../Resources/Crusader knight/Base mesh/crusader base mesh.fbx",
		aiProcess_Triangulate | aiProcess_JoinIdenticalVertices
		| aiProcess_LimitBoneWeights);

	m_vMesh.reserve(scene->mNumMeshes);

	//scene->mTextures = aiTexture *a;






	for (int i = 0; i < scene->mNumMeshes; ++i)
	{
		aiMesh *mesh = scene->mMeshes[i];
		std::vector<ST__Vertex_Obj> vertex;
		vertex.reserve(mesh->mNumVertices);

		//P 본인덱스4개까지 가중치4개까지

		//Position 정보 채우기 T N 나중에
		for (int j = 0; j < mesh->mNumVertices; ++j)
		{
			ST__Vertex_Obj skin;

			skin.m_position.x = mesh->mVertices[j].x;
			skin.m_position.y = mesh->mVertices[j].y;
			skin.m_position.z = mesh->mVertices[j].z;
			
			if (mesh->mTextureCoords[0])
			{
				skin.m_texcoord.x = mesh->mTextureCoords[0][j].x;
				skin.m_texcoord.y = mesh->mTextureCoords[0][j].y;
			}
			else
			{
				skin.m_texcoord = D3DXVECTOR2(0.0f, 0.0f);
			}

			if (mesh->mNormals)
			{
				skin.m_normalize.x = mesh->mNormals[j].x;
				skin.m_normalize.y = mesh->mNormals[j].y;
				skin.m_normalize.z = mesh->mNormals[j].z;

			//	std::cout << skin.m_normalize.x << std::endl;
			//	std::cout << skin.m_normalize.y << std::endl;
			//	std::cout << skin.m_normalize.z << std::endl;
			}
			vertex.push_back(skin);
		}

		std::vector<UINT> indices;
		indices.reserve(mesh->mNumFaces * 3);

		for (unsigned int i = 0; i < mesh->mNumFaces; i++)
		{
			aiFace face = mesh->mFaces[i];
			for (unsigned int j = 0; j < face.mNumIndices; j++)
				indices.push_back(face.mIndices[j]);
		}

		cMesh* m_mesh = new cMesh();
		m_mesh->_InitializeVertex<ST__Vertex_Obj>(vertex);
		m_mesh->_InitializeIndices(indices);

		m_vMesh.push_back(m_mesh);
	}


	m_ObjectShader = new cObjectShader();
	m_ObjectShader->_Initialize();
}

void cAssimpObjectLoad::_Update(D3DXMATRIX v, D3DXMATRIX p)
{
	m_ObjectShader->_Update(*m_Transform->_GetWorld(),
		v, p);
}

void cAssimpObjectLoad::_Render()
{
	UINT count = m_vMesh.size();
	for (UINT i = 0; i < count; ++i)
	{
		m_vMesh[i]->_Render();
		const UINT count = m_vMesh[i]->m_indices;
		m_ObjectShader->_Render(count);
	}
}

void cAssimpObjectLoad::_TextureSet(const std::string & fileName)
{
	cTexture* texture = new cTexture();
	texture->_Initialize(fileName);
	m_ObjectShader->m_Texture = texture;
}
