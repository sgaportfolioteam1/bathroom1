#pragma once

/*
텍스쳐 쉐이더

텍스쳐를 받아서 출력해준다.
*/

//텍스쳐 오브젝트 사용
class cTexture;

class cTextureShader
{
public:
	cTextureShader();
	~cTextureShader();
private:
	//버텍스 쉐이더, 
	ID3D11VertexShader* m_VSShader = nullptr;
	//화면상 쉐이더,
	ID3D11PixelShader* m_PSShader = nullptr;
	//Byte 맞추기
	ID3D11InputLayout* m_InputLayout = nullptr;
	//월드 버퍼
	ID3D11Buffer* m_WVPBuffer = nullptr;
public:
	//텍스쳐 
	cTexture* m_Texture = nullptr;

	void _Initialize();
	void _Update(D3DXMATRIX m, D3DXMATRIX v, D3DXMATRIX p);
	void _Render(UINT renderCount);
};

