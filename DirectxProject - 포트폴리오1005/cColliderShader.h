#pragma once
/*
그냥 콜라이더 출력용이다..
*/
class cColliderShader
{
public:
	cColliderShader();
	~cColliderShader();
private:
	//버텍스 쉐이더, 
	ID3D11VertexShader* m_VSShader = nullptr;
	//화면상 쉐이더,
	ID3D11PixelShader* m_PSShader = nullptr;
	//Byte 맞추기
	ID3D11InputLayout* m_InputLayout = nullptr;
	//와이어 프레임
	ID3D11RasterizerState * m_RSState = nullptr;
	//월드 버퍼
	ID3D11Buffer* m_WVPBuffer = nullptr;
	//컬러 버퍼
	ID3D11Buffer* m_ColorBuffer = nullptr;
public:
	D3DXCOLOR m_Color = D3DXCOLOR(0, 1, 0, 0);


	void _Initialize();
	void _Update(D3DXMATRIX m, D3DXMATRIX v, D3DXMATRIX p);
	void _Render(UINT renderCount);
};

