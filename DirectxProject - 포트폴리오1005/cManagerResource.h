#pragma once

#include "cSingleton.h"
#include "cGenericResource.h"

#include <assimp/Importer.hpp>  // C++ importer interface
#include <assimp/scene.h>       // Output data structure
#include <assimp/postprocess.h> // Post processing flags

//머터리얼 사용
class cMaterial;
//텍스쳐 사용
class cTexture;
//메쉬 사용
class cMesh;

class cLightShader;
class cLightViewShader;

class cFont;
class cFontShader;

//본, 매핑 정보
struct ST__Acter
{
	UINT m_NumBones = 0;
	std::vector<struct ST__BindPose> m_BindPose;
	std::map<std::string, UINT> m_BoneMapping; // maps a bone name to its index
};



class cManagerResource : public cSingleton<cManagerResource>
{
public:
	cManagerResource();
	virtual ~cManagerResource();

public:
	//템플릿으로 변환은 추후 생각해보겠다.
	//std::list<cMaterial*> m_Materials;
	//std::list<cTexture*> m_Textures;
	//std::list<cMesh*> m_Meshs;

	cGenericResource<cMaterial> * m_Materials = nullptr;
	cGenericResource<cTexture> * m_Textures = nullptr;
	cGenericResource<cMesh> * m_Meshs = nullptr;

public:
	//void _MaterialRelease();
	//void _SetMaterial(cMaterial* mtl);
	//cMaterial* _GetMaterial(const std::string& key);
	//
	//void _TextureRelease();
	//void _SetTexture(cTexture* tex);
	//cTexture* _GetTexture(const std::string& key);
	//
	//void _MeshRelease();
	//void _SetMesh(cMesh* tex);
	//cMesh* _GetMesh(const std::string& key);


	cLightShader* m_LightShader = nullptr;
	cLightViewShader* m_LightViewShader = nullptr;

	cFont* m_Font = nullptr;
	cFontShader* m_FontShader = nullptr;

	//////////////////////////
	Assimp::Importer importer;
	const aiScene *scene = nullptr;

	Assimp::Importer importerAniA;
	const aiScene *sceneAniA = nullptr;

	Assimp::Importer importerAniB;
	const aiScene *sceneAniB = nullptr;

	//기사 본
	ST__Acter m_BoneActor;

	aiMatrix4x4 m_GlobalInverseTransform;

	void _InitializeModel();
	void _InitializeMesh(UINT MeshIndex,
		const aiMesh* paiMesh,
		std::vector<struct ST__Vertex_SKIN>& vertices,
		std::vector<UINT>& Indices);
	void _InitializeBone(UINT MeshIndex,
		const aiMesh * paiMesh,
		std::vector<struct ST__Vertex_SKIN>& vertices, struct ST__Acter& acter);
};

