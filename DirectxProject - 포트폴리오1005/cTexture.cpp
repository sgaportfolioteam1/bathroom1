#include "stdafx.h"
#include "cTexture.h"


cTexture::cTexture()
{
}


cTexture::~cTexture()
{
	SAFE_RELEASE(m_SampleState);
	SAFE_RELEASE(m_ShaderResource);
}

void cTexture::_Initialize(const std::string & image)
{
	//ID3D11ShaderResourceView, ID3D11SamplerState
	{
		auto hr = D3DX11CreateShaderResourceViewFromFileA
		(
			cGraphic::GetInstance().GetDevice(),
			image.c_str(), //"Image.png",
			nullptr,
			nullptr,
			&m_ShaderResource,
			nullptr
		);

		assert(SUCCEEDED(hr));
		//////////////////////////////////////////////////////////////
		D3D11_SAMPLER_DESC desc;
		ZeroMemory(&desc, sizeof(D3D11_SAMPLER_DESC));
		desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		//U 좌표가 늘어났을때 어떤 작업을 할건가.
		desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		//V 좌표가 늘어났을때 어떤 작업을 할건가.
		desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		//나머지 좌표에 대한것, 몰라도된다.
		desc.BorderColor[0] = 1; //R
		desc.BorderColor[1] = 0; //G
		desc.BorderColor[2] = 0; //B
		desc.BorderColor[3] = 1; //A
		//외각선 / 빈공간에 대한 색깔을 칠할때
		desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		//이전과 현재 데이터를 비교하는 방법을 정하는 플래그
		desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		//desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		//이미지가 축소, 확대, 밉맵이 일어날때에 보정법.
		desc.MaxAnisotropy = 16;
		//비등방성 필터링, 몰라도 된다, 
		//3D할때 이미지를 발랐을때 회전된경우나,
		//후면에 대해 이미지를 깔끔하게 할것인가에 대한 방법
		desc.MaxLOD = std::numeric_limits<float>::max();
		desc.MinLOD = std::numeric_limits<float>::min();
		//LOD = Level of Detail 거리에 따라서 어떻게
		//조밀하거나 먼것들은 잘 안보이거나 조정 가능하다.
		desc.MipLODBias = 0.0f;
		//밉맵에 대해 배울때 설명, 
		//인덱스 번들있다 추가되는 옵셋 정보들.

		hr = cGraphic::GetInstance().GetDevice()->
			CreateSamplerState
			(
				&desc, &m_SampleState
			);

		assert(SUCCEEDED(hr));
	}
}

void cTexture::_Render()
{
	cGraphic::GetInstance().GetDeviceContext()->
		PSSetShaderResources(0, 1, &m_ShaderResource);
	cGraphic::GetInstance().GetDeviceContext()->
		PSSetSamplers(0, 1, &m_SampleState);
}
